
=head1 NAME

=head1 SYNOPSIS

      use base MusicFlow::bin::Common;
	  
	  # aSub references the Common sub 'trim'
	  sub aSub {
		my $self = shift;
		$string = $self->trim($string);

=head1 DESCRIPTION

  This is an object-oriented shared library that supports the MusicFlow app.

=head2 Methods

=cut

package MusicFlow::bin::Common;
###
use strict;
use warnings;
#use utf8;
#use utf8::all;
use feature "switch";
use Encode qw(is_utf8 encode decode);
use File::Spec::Functions;
use Win32API::File 0.08 qw( :ALL );
use Win32::FindFile qw(wchar);
use Text::Unidecode;
use File::Find;
use File::Spec::Functions;
use File::Spec;
use Win32::Unicode;
use MusicFlow::bin::Select qw<@selectedDirs @selectedTracks>;
 
require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw();

sub getTasks {
	my @self = shift;
	my $type = shift;
	my @dirs;
	my @Tracks;
	my @Albums;
			
	if (open(my $MusicFilefh, '<', $ENV{"Selection"})) {
		#print $hLogFile "Reading $ENV{'Selection'}.\n";
		while (defined (my $dir = readline($MusicFilefh))) {
			my $dir = decode('UTF-8', $dir);
			if (defined $dir) {
				chomp $dir;
				push(@dirs, $dir);
			}
		}
		close($MusicFilefh);
	}

	my $outfh;	
	if (scalar @dirs > 0) {
		findW(makeExtClosure($outfh,
		sub {
			#print "$_ $Win32::Unicode::Dir::name\n";
			if (/.(wav|flac|mp3)$/) {
				my ($volume, $directories, $file) =
					File::Spec->splitpath( $Win32::Unicode::Dir::name, 0 );
				my @dirs = File::Spec->splitdir( $directories );
				pop @dirs; # toss empty element (due to trailing /)
				my $album = pop @dirs;
				my $artist = pop @dirs;
				my $dir = File::Spec->catdir( @dirs );
				my $track =
					$volume . $dir . "::" . $artist . "::" . $album . "::" . $_;
				my $albumdir =
					$volume . $dir . "::" . $artist . "::" . $album;
				push(@Tracks, $track);
				push(@Albums, $albumdir);
				#print $selection . "\n";
			}
		}), @dirs);
	}

	@Tracks = sort @Tracks;
	@Albums = keys %{{ map { $_ => 1 } sort @Albums }};

	if ($type =~ /song/i) {
		return(\@Tracks);
	}
	else {
		return(\@Albums);
	}
}

sub makeExtClosure
{
    my ($ext, $action) = @_;
#    my ($action) = @_;
	return sub {
       &$action;
    };
}

sub wantedaction
{
	my $self = shift;	
}
sub wanted
{
	my $self = shift;
	#my $fh = shift;
	my $args = shift;
	
	if (/.(wav|flac|mp3)$/) {
		my ($volume, $directories, $file) =
			File::Spec->splitpath( $args->{path}, 0 );
		my @dirs = File::Spec->splitdir( $directories );
		pop @dirs; # toss empty element (due to trailing /)
		my $album = pop @dirs;
		my $artist = pop @dirs;
		my $dir = File::Spec->catdir( @dirs );
		my $selection =
			encode('UTF-8', $volume . $dir . "::" . $artist . \
				   "::" . $album . "::" . $args->{file});
		#print $fh $selection . "\n";
		#print $selection . "\n";
	}
	
}

# Perl trim function to remove whitespace from the start and end of the string
sub trim
{
	my $self = shift;
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

sub nameToWin {
	my $self = shift;
	my $winName = shift;
	$winName = $self->trim($winName);
	$winName =~ s/:/;/g;
	$winName =~ s/\?/=/g;
	$winName =~ s/\//_/g;
	$winName =~ s/\.$/_/g;
	$winName =~ s/\s+/ /;
	return $winName;
}
sub trackNameToWin {
	my $self = shift;
	my $winName = shift;
	$winName = $self->trim($winName);
	$winName =~ s/:/;/g;
	$winName =~ s/\?/=/g;
	$winName =~ s/\//_/g;
	#$winName =~ s/\.$/_/g;
	$winName =~ s/\s+/ /;
	return $winName;
}
sub WinToName {
	my $self = shift;
	my $name = shift;
	$name = $self->trim($name);
	$name =~ s/;/:/g;
	$name =~ s/=/\?/g;
	$name =~ s/_$/\./g;
	$name =~ s/_/\//g;
	$name =~ s/\s+/ /;
	return $name;
}

sub runSystemCommand
{
	my $self = shift;
	my $exe = shift;
	my $infile = shift;
	my $outfile = shift;
	my $modifiesInput = shift;
	my @options = @_;
	my $asciiinfile;
	my $asciioutfile;
	my $cmd;
	my $output;
	my $rval = 0;
	
	#sleep(1);
	if ($self->containsUnicode($infile)) {
		$asciiinfile = unidecode($infile);
		$self->pathToAscii($infile);
		$asciioutfile = unidecode($outfile);
	}
	else {
		$asciiinfile = $infile;
		$asciioutfile = $outfile;
	}
	
	if ($asciioutfile eq '') {
		$cmd = "\"$exe\" " . join(" ", @options) . " \"$asciiinfile\"";
	}
	else {
		$cmd = "\"$exe\" " . join(" ", @options) . " \"$asciiinfile\"" . " \"$asciioutfile\"";
	}
	# can i use  :encoding(utf8) , a :std here instead of containsUnicode?
	# or
	# use open ':encoding(utf8)';
    # use open ':locale';
    # use open ':encoding(iso-8859-7)';
	#print "runSystemCommand\n";
	$output = `$cmd 2>&1`;
	#print "output = " . $output . "\n";
#	$output = $cmd;
	#open(my $cmdin, "-|:raw", $cmd);
	#$output = <$cmdin>;
	#close($cmdin);
	$rval = $?;
	
	if ($self->containsUnicode($infile)) {
		$self->pathToUnicode($infile, $modifiesInput);
	}
								
	#if (defined $output) {
	#	print $output . "\n";
	#}
	
	if (!defined $output) {
		$output = "";
	}
	my @ret = ($output, $rval);
	return (\@ret);
}

sub containsUnicode
{
	my $self = shift;
	my $path = shift @_;
	return($path ne unidecode($path));
}

sub splitFilePath
{
	my $self = shift;
	my $path = shift @_;
	my $volume;
	my $directories;
	my $artist;
	my $album;
	my $track;
	my @dirs;
	
	($volume,$directories,$track) = File::Spec->splitpath( $path );
	$directories =~ s/^\\//g;
	$directories =~ s/\\$//g;
	@dirs = File::Spec->splitdir( $directories );
	$album = pop @dirs;
	$artist = pop @dirs;
	unshift(@dirs, $volume);
	
	return(File::Spec->catdir( @dirs ), $artist, $album, $track);
}

sub dirToAscii
{
	my $self = shift;
	my $path;
	my $artist;
	my $album;
	my $track;
	my $asciidirpath;
	
	if (@_ == 1) {
		($path, $artist, $album, $track) = $self->splitFilePath(shift @_);
	}
	else {
		$path = shift @_;
		$artist = shift @_;
		$album = shift @_;
		$track = shift @_;
	}
	
	my $trackpath = catfile($path, $artist, $album, $track);
	
	$asciidirpath = unidecode($trackpath);
	if (containsUnicode($trackpath)) {
		my $asciiartist = unidecode($artist);
		my $asciialbum = unidecode($album);
		my $asciitrack = unidecode($track);
		if ($asciiartist ne $artist) {
			my $artistpath = catfile($path, $asciiartist);
			Win32::CreateDirectory( $artistpath );
		}
		if ($asciialbum ne $album) {
			my $albumpath = catfile($path, $asciiartist, $asciialbum);
			Win32::CreateDirectory( $albumpath );
		}
		CopyFileW(wchar($trackpath), wchar($asciidirpath), 1);
	}
	
	return($asciidirpath);
}

sub pathToAscii
{
	my $self = shift;
	my $path;
	my $artist;
	my $album;
	my $track;
	my $asciitrackpath;
	
	if (@_ == 1) {
		($path, $artist, $album, $track) = $self->splitFilePath(shift @_);
	}
	else {
		$path = shift @_;
		$artist = shift @_;
		$album = shift @_;
		$track = shift @_;
	}
	
	my $trackpath = catfile($path, $artist, $album, $track);
	
	$asciitrackpath = unidecode($trackpath);
	if (containsUnicode($trackpath)) {
		my $asciiartist = unidecode($artist);
		my $asciialbum = unidecode($album);
		my $asciitrack = unidecode($track);
		if ($asciiartist ne $artist) {
			my $artistpath = catfile($path, $asciiartist);
			Win32::CreateDirectory( $artistpath );
		}
		if ($asciialbum ne $album) {
			my $albumpath = catfile($path, $asciiartist, $asciialbum);
			Win32::CreateDirectory( $albumpath );
		}
		CopyFileW(wchar($trackpath), wchar($asciitrackpath), 1);
	}
	
	return($asciitrackpath);
}

sub pathToUnicode {
	my $self = shift;
	my $path;
	my $artist;
	my $album;
	my $track;
	
	if (@_ == 2) {
		($path, $artist, $album, $track) = $self->splitFilePath(shift @_);
	}
	else {
		$path = shift @_;
		$artist = shift @_;
		$album = shift @_;
		$track = shift @_;
	}
	my $modifiesInput = shift @_;
	
	my $trackpath = catfile($path, $artist, $album, $track);
	
	if (containsUnicode($trackpath)) {
		my $asciitrackpath = unidecode($trackpath);
		my $asciiartist = unidecode($artist);
		my $asciialbum = unidecode($album);
		my $asciitrack = unidecode($track);
		
		if ($modifiesInput) {
			MoveFileExW(wchar($asciitrackpath), wchar($trackpath), MOVEFILE_REPLACE_EXISTING);
		}
		else {
			DeleteFileW(wchar($asciitrackpath));
		}
		
		# Check for output files just created
		my $albumdir = catfile($path, $asciiartist, $asciialbum);
		opendir ( DIR, $albumdir ) || die "Error in opening dir $albumdir\n";
		my @filenames =  grep !/^\.\.?$/, readdir DIR;
		foreach (@filenames) {
			 my $filename = $_;
		     my $asciifile = catfile($path, $asciiartist, $asciialbum, $filename);
			 my $utffile = $asciifile;
			 my $tracknoext;
			 $tracknoext = $track;
			 $tracknoext =~ s/\.(flac|mp3|wav|log|cue|jpg|html|text|txt)$//;
			 my $asciitracknoext;
			 $asciitracknoext = $asciitrack;
			 $asciitracknoext =~ s/\.(flac|mp3|wav|log|cue|jpg|html|text|txt)$//;
			 $utffile =~ s/$asciitracknoext/$tracknoext/;
			 $utffile =~ s/$asciialbum/$album/;
			 $utffile =~ s/$asciiartist/$artist/;
			 MoveFileW(wchar($asciifile), wchar($utffile));
		}
		closedir(DIR);

		if ($asciialbum ne $album) {
			my $albumpath = catfile($path, $asciiartist, $asciialbum);
			rmdir( $albumpath );
		}
		if ($asciiartist ne $artist) {
			my $artistpath = catfile($path, $asciiartist);
			rmdir( $artistpath );
		}
	}
}
sub FormatTrackNumber
{
	my $self = shift;
	my $formatTrack;
	my $track = shift @_;
	if ($track =~ /(^|\/)(\d+)-(\d) /) {
		$formatTrack = $track;
		$formatTrack =~ s/(^|\/)(\d+)-(\d) /$1$2-0$3 /;
	}
	elsif (length($track) eq 1) {
		$formatTrack = "0" . $track;
	}
	else {
		$formatTrack = $track;
	}
		
	return($formatTrack);
}

sub tagFile
{
	my $self = shift;
	my $musicfile = shift;
	my $artfile = shift;
	my $hrTrack = shift;
	my $retval = 0;
	my $tagpath = $ENV{"Tag_Exe"};
	my $metaflacpath = $ENV{"MetaFlac_Exe"};
	my $addMp3Artpath = $ENV{"AddMP3Art_Exe"};
	my $log;
					
	if ($musicfile =~ /.mp3/) {
		my $wf = $self->WindowizePath($musicfile);
		#my $shortmusicfile = Win32::GetShortPathName($wf);
		my $mp3 = MP3::Tag->new($musicfile);
		$log .= "Applying tags to " . $wf . "\n";
		if (!defined $mp3) {
			$log .= "mp3 undefined - Skipping " . $wf . "\n";
			return(1);
		}
		$mp3->get_tags();
		my $id3v2 = $mp3->{ID3v2};
		if (!defined $id3v2) {
			$id3v2 = $mp3->new_tag("ID3v2");
		}
		else {
			$id3v2->remove_tag();
			$mp3 = MP3::Tag->new($musicfile);
			#$mp3->get_tags();
			$id3v2 = $mp3->new_tag("ID3v2");
		}
		
		#my $f;
		#my $data = do {
		#	open my $f, '<', "$picfile" or die;
		#    undef $/;
		#	<$f>
		#};
		
		#my $new_frame = $id3v2->frame_select('APIC', '', undef, $data);
		#my $data = $id3v2->frame_select('APIC', '', '')
		#		or die "no expected APIC frame found";

		if (defined $hrTrack->{title}) {
			$id3v2->frame_select_by_descr("TIT2", "$hrTrack->{title}");
		}
		if (defined $hrTrack->{artist}) {
			$id3v2->frame_select_by_descr("TPE1", "$hrTrack->{artist}");
		}
		if (defined $hrTrack->{album}) {
			my ($info, $name, @rest) = $id3v2->get_frame("TALB");
			if (!defined $info) {
				$id3v2->add_frame("TALB","$hrTrack->{album}");
			}
			$id3v2->change_frame("TALB","$hrTrack->{album}");
		}
		if (defined $hrTrack->{track}) {
			$id3v2->frame_select_by_descr("TRCK", "$hrTrack->{track}");
		}
		if (defined $hrTrack->{quality}) {
			$id3v2->frame_select_by_descr("TXXX[RIP_QUALITY]", $hrTrack->{quality});
		}
		$id3v2->write_tag();
		if (defined $hrTrack->{source}) {
			$id3v2->frame_select_by_descr("TXXX[SOURCE]", $hrTrack->{source});
		}
		$id3v2->write_tag();
		if (defined $hrTrack->{UPC}) {
			$id3v2->frame_select_by_descr("TXXX[UPC]", $hrTrack->{UPC});
		}
		if (defined $hrTrack->{producer}) {
			$id3v2->frame_select_by_descr("TXXX[PRODUCER]", $hrTrack->{producer});
		}
		if (defined $hrTrack->{RELEASE_VERSION}) {
			$id3v2->frame_select_by_descr("TXXX[RELEASE VERSION]", $hrTrack->{RELEASE_VERSION});
		}
		if (defined $hrTrack->{quality}) {
			$id3v2->frame_select_by_descr("TXXX[RIP_QUALITY]", $hrTrack->{quality});
		}
		if (defined $hrTrack->{releasedate}) {
			$id3v2->frame_select_by_descr("TXXX[YEAR]", $hrTrack->{releasedate});
		}
		if (defined $hrTrack->{genre}) {
			$id3v2->frame_select_by_descr("TCON", "$hrTrack->{genre}");
		}
		if ((defined $hrTrack->{comment}) && (defined $hrTrack->{location})) {
			$id3v2->frame_select_by_descr("COMM(eng,#0)[]", "$hrTrack->{comment} $hrTrack->{location}");
		}
		elsif (defined $hrTrack->{location}) {
			$id3v2->frame_select_by_descr("COMM(eng,#0)[]", "$hrTrack->{location}");
		}
		elsif (defined $hrTrack->{comment}) {
			$id3v2->frame_select_by_descr("COMM(eng,#0)[]", "$hrTrack->{comment}");
		}
		if (defined $hrTrack->{recordingdate}) {
			$id3v2->frame_select_by_descr("TXXX[RECORDINGDATES]", "$hrTrack->{recordingdate}");
		}
		if (defined $hrTrack->{bandlisting}) {
			$hrTrack->{bandlisting} =~ s/\\\\/\\/g;
			$id3v2->frame_select_by_descr("TPE2", "$hrTrack->{bandlisting}");
		}
		if (defined $hrTrack->{totaltracks}) {
			$id3v2->frame_select_by_descr("TXXX[TOTALTRACKS]", "$hrTrack->{totaltracks}");
		}
		if (defined $hrTrack->{label}) {
			$id3v2->frame_select_by_descr("TPUB", "$hrTrack->{label}");
		}
		if (defined $hrTrack->{disc}) {
			$id3v2->frame_select_by_descr("TPOS", "$hrTrack->{disc}" . "/" . "$hrTrack->{totaldiscs}");
		}
		if (defined $hrTrack->{composer}) {
			$id3v2->frame_select_by_descr("TCOM", "$hrTrack->{composer}");
		}
		
		$id3v2->write_tag();
		$mp3->close();
		
		# then artwork
		# there are issues with applying large images
		if (0 && ($musicfile =~ m/.mp3$/)) {
			my $outfile = $wf;
			$outfile =~ s/.mp3$/art.mp3/;
			my $cmd = "\"$addMp3Artpath\" -p \"$artfile\" -i \"$wf\" -o \"$outfile\"";
			$log .= $cmd . "\n";
			my $output = `$cmd`;
			#$output = `$cmd 2>&1`;
			my $rval = $?;
			$log .= $output . "\n";
			if ($rval ne 0) {
				$log .= "ERROR: $wf art tagging failed (exit code: $rval)\n";
				$retval += $rval;
				DeleteFileW(wchar($outfile));
			}
			elsif (-s $outfile > 0) {
				if (MoveFileW(wchar($outfile), wchar($wf)) == 0) {
					$log .= "ERROR: Move unsuccessful:\n\t$outfile =>\n\t$wf\n$!\n\t";
					return(1);
				}
			}
			else {
				DeleteFileW(wchar($outfile));
			}
			$log .= "finished adding mp3 art\n";
		}
		
		#printAll($musicfile);
	}
	else {
		# flac format
		#$cmd = "\"$metaflacpath\" --remove --block-type=PICTURE \"$musicfile\"";
		my $wf = $self->WindowizePath($musicfile);
		$wf =~ s/\$/\\\$/g;
		#$wf = decode( 'utf8', $wf );
		$DB::single = 1;
		my $rval = $self->runSystemCommand($metaflacpath, $wf, "", 1, "--remove-all");
		#my $cmd = "\"$metaflacpath\" --remove-all \"$wf\"";
		#my $output = `$cmd 2>&1`;
		#my $rval = $?;
		#$log .= $output . "\n";
		if ($rval ne 0) {
			$log .= "ERROR: clearing tags from:\n\t$wf\n";
			print "ERROR: clearing tags from:\n\t$wf\n";
			$retval += $rval;
		}
		#my $cmd = "\"$tagpath\" --remove ";
		#my $cmd = "--remove ";
		my $cmd = "--stdout ";
			
		if (defined $hrTrack->{title}) {
			my $t = $hrTrack->{title};
			#my $t = decode('UTF-8', $hrTrack->{title});
			#my $t = encode('UTF-8', $hrTrack->{title});
			#my $t = "Überjam";
			#my $t = "\xc3\x9cberjam";
			$t =~ s/\$/\\\$/g;
			
			#$t = decode('UTF-8', $t);
			#$cmd = $cmd . "--title \"$t\" ";
			#$cmd = $cmd . "-u title=\"$t\" ";
			$cmd = $cmd . "--title \"$t\" ";
		}
		if (defined $hrTrack->{artist}) {
			$cmd = $cmd . "--artist \"$hrTrack->{artist}\" ";
		}
		if (defined $hrTrack->{album}) {
			#my $t = decode('UTF-8', $hrTrack->{album});
			#$cmd = $cmd . "-u album=\"$t\" ";
			$cmd = $cmd . "--album \"$hrTrack->{album}\" ";
		}
		if (defined $hrTrack->{track}) {
			$cmd = $cmd . "--track $hrTrack->{track} ";
		}
		if (defined $hrTrack->{releasedate}) {
			$cmd = $cmd . "--year \"$hrTrack->{releasedate}\" ";
		}
		if (defined $hrTrack->{genre}) {
			$cmd = $cmd . "--genre \"$hrTrack->{genre}\" ";
		}
		if ((defined $hrTrack->{comment}) && (defined $hrTrack->{location})) {
			$cmd = $cmd . "--comment \"$hrTrack->{comment} $hrTrack->{location}\" ";
		}
		elsif (defined $hrTrack->{location}) {
			$cmd = $cmd . "--comment \"$hrTrack->{location}\" ";
		}
		elsif (defined $hrTrack->{comment}) {
			$cmd = $cmd . "--comment \"$hrTrack->{comment}\" ";
		}
		if (defined $hrTrack->{recordingdate}) {
			$cmd = $cmd . "-t recordingdates=\"$hrTrack->{recordingdate}\" ";
		}
		if (defined $hrTrack->{bandlisting}) {
			$cmd = $cmd . "-t Band=\"$hrTrack->{bandlisting}\" ";
		}
		if (defined $hrTrack->{totaltracks}) {
			$cmd = $cmd . "-t totaltracks=\"$hrTrack->{totaltracks}\" ";
		}
		if (defined $hrTrack->{producer}) {
			$cmd = $cmd . "-t producer=\"$hrTrack->{producer}\" ";
		}
		if (defined $hrTrack->{label}) {
			$cmd = $cmd . "-t publisher=\"$hrTrack->{label}\" ";
		}
		if (defined $hrTrack->{disc}) {
			$cmd = $cmd . "-t Discnumber=\"$hrTrack->{disc}\"/\"$hrTrack->{totaldiscs}\" ";
		}
		if (defined $hrTrack->{composer}) {
			$cmd = $cmd . "-u Composer=\"$hrTrack->{composer}\" ";
		}
		if (defined $hrTrack->{source}) {
			$cmd = $cmd . "-t source=\"$hrTrack->{source}\" ";
		}
		if (defined $hrTrack->{quality}) {
			$cmd = $cmd . "-t rip_quality=\"$hrTrack->{quality}\" ";
		}
		
		# first tags
		# make bat file and run it
		my $battagfile = $musicfile;
		$battagfile =~ s/flac$/bat/;
		open (BAT, ">:encoding(UTF-8)", $battagfile) or die $!;
		print BAT "\@echo off\n";
		print BAT "chcp 65001\n";
		print BAT "\"$tagpath\" $cmd \"$musicfile\"";
		close BAT;
		#$rval = $self->runSystemCommand($tagpath, $wf, "", 1, $cmd);
		$rval = $self->runSystemCommand($battagfile, "", "", 1, "");
		DeleteFile($battagfile);
		
		#$output = `$cmd 2>&1`;
		#$rval = $?;
		#$log .= $output . "\n";
		if ($rval ne 0) {
			$log .= "ERROR: $musicfile tagging failed (exit code: $rval)\n";
			$retval += $rval;
		}
		
		# then artwork
#		if (($musicfile =~ m/.flac$/)) {
#			#$cmd = "\"$metaflacpath\" --import-picture-from=\"$artfile\" \"$musicfile\"";
#			$rval = $self->runSystemCommand($metaflacpath, $musicfile, "", 1, "--import-picture-from=\"$artfile\"");
#			#$output = `$cmd 2>&1`;
#			#$rval = $?;
#			#$log .= $output . "\n";
#			if ($rval ne 0) {
#				$log .= "ERROR: $musicfile art tagging failed (exit code: $rval)\n";
#				$retval += $rval;
#			}
#		}
	}
	
	$log .= "\n";
	
	return($retval);
}


sub WindowizePath
{
	my $self = shift;
	my $path = shift;
	$path =~ s/\/cygdrive\/k/K:/i;
	$path =~ s/\/cygdrive\/j/J:/i;
	$path =~ s/\//\\/g;
	#$DB::single = 1;
	$self->{Log} .= "New path: $path\n";
	return $path;
}


sub CalculateTrack
{
	my $self = shift;
	my $TrackNum = shift;
	my $DiscNum = shift;
	my $refTotalTracks = shift;
	my $totalTrackNum;
	my $disc;
	my %TotalTracks = %$refTotalTracks;

	$totalTrackNum = 0;
	
	foreach $disc(1 ... $DiscNum-1) {
		$totalTrackNum += $TotalTracks{$disc};
	}
	
	$totalTrackNum += $TrackNum;
	
	return($totalTrackNum);
}

sub Capitalize($)
{
	my $self = shift;
	my $string = shift;
		
	$string =~ s/(\s|^)(\w)/$1\U$2/g;
	$string =~ s/(Mc)([a-z])/$1\U$2/g;
	return($string);
}
sub capitalize
{
	my $self = shift;
	my $string = shift;
	
	$string =~ s/ (
        (^\w)    #at the beginning of the line
          |      # or
        (\s\w)   #preceded by whitespace
	 ) /\U$1/xg;
    $string =~ s/([\w']+)/\u\L$1/g;
	
	return($string);

}

sub digitize
{
	my $self = shift;
	my $num = shift;
	
	if ($num eq "one") {return 1 }
	elsif ($num eq "two") {return 2 }
	elsif ($num eq "three") {return 3 }
	elsif ($num eq "four") {return 4 }
	elsif ($num eq "five") {return 5 }
	elsif ($num eq "six") {return 6 }
	elsif ($num eq "seven") {return 7 }
	elsif ($num eq "eight") {return 8 }
	elsif ($num eq "nine") {return 9 }
	elsif ($num eq "ten") {return 10 }
	elsif ($num eq "eleven") {return 11 }
	elsif ($num eq "twelve") {return 12 }
	else { return 0 }
}

sub readTags
{
	my $self = shift;
	my $tagfile = shift @_;
	my $ripresults = shift @_;
	my $artists;
	my @tagrecord;
	my $UPC;
	my $catalog;
	my $artist;
	my $album;
	my $source;
	my $producer;
	my $releasedate;
	my $recordingdate;
	my $trackrecordingdate;
	my $location;
	my $genre;
	my $discnumber;
	my $tracknum;
	my $totaltracks;
	my $totaldiscs;
	my $label;
	my $inband = 0;
	my $intrack = 0;
	my $uniqtrack;
	my $filetrack;
	my @tracklist;
	my %totalTracks;
	my $log;
	$DB::single = 1;
	
	#my $regExWeirdDash = "ΓÇô";
	#my $regExTrackName = "[\\w\\s\\'\\-\\.:,#&\\?!\\/\\(\\)°øÈùèìáéíóúÁÉÍÓÚÑñäëïöüÄËÏÖÜçãôêåàÀõÅøæ\+è]+";
	my $regExLocation = "[\\w\\s,\.\'øáéíóúÁÉÍÓÚÑñäëïöüÄËÏÖÜçãôêåàÀõÅ&\-]+";
	#my $regExComposerName = "\\D[\\w\\s,\.\\-'\\/áéíóúÁÉÍÓÚÑñäëïöüÄËÏÖÜçãôênÅåø!]+";
	#my $regExAlbumName = "[\\w\\s\\'!\\(\\),\-]+";
	#my $regExInstrument = "[\\w\\s\\d]+";
	my $regExDate = "(([jJ]an|[fF]eb|[mM]ar|[aA]pr|[mM]ay|[jJ]un|[jJ]an|[jJ]ul|[aA]ug|[sS]ep|[oO]ct|[nN]ov|[dD]ec)\\w*\\s+\\d\\d?(st|nd|rd|th|),\\s*\\d\\d\\d\\d)|(\\d\\d?\[\\-\\/]\\d\\d?[\\-\\/]\\d\\d\\d\\d)";	
	my ($filepath, $fileartist, $filealbum, $tagfilename) = $self->splitFilePath($tagfile);
	
	#my $shorttagfile = Win32::GetShortPathName($tagfile);
	open (TAG, "<:encoding(UTF-8)", "$tagfile") or return($artists);
	$/ = undef;
	my $line = <TAG>;
	close TAG;
	#$line = encode('utf8', $line);
	@tagrecord = split(/\n/, $line);
	
	for my $tagline (@tagrecord) {
		#print $tagline . "\n";
		if ($tagline =~ /^\s*$/) {
			next;
		}
		if ($tagline =~ /^#/) {
			next;
		}
		if ($tagline =~ /^artist (.*)/i) {
			$artist = $self->trim($1);
			if ($artist ne $fileartist) {
				$log .= "WARNING: $artist ne $fileartist\n";
				#print "WARNING: $artist ne $fileartist\n";
			}
			if (!defined $artists->{$artist}) {
				my $hrAlbums = {};
				$artists->{$fileartist} = $hrAlbums;
			}
		}
		elsif ($tagline =~ /^album (.*)/i) {
			$album = $self->trim($1);
			if ($album ne $filealbum) {
				$log .= "WARNING: $album ne $filealbum\n";
				#print "WARNING: $album ne $filealbum\n";
			}
			if (!defined $artists->{$fileartist}{$filealbum}) {
				my $hrTracks = {};
				$artists->{$fileartist}{$filealbum}{tracks} = $hrTracks;
				$artists->{$fileartist}{$filealbum}{album} = $album;
				$artists->{$fileartist}{$filealbum}{artist} = $artist;
			}
		}
		elsif ($tagline =~ /^source ([\d\s\w,\.\-]+)/i) {
			$source = $1;
		}
		elsif ($tagline =~ /^producer (.*)/i) {
			$producer = $1;
		}
		elsif ($tagline =~ /^released ([\d\s\w,\.\-]+)/i) {
			$releasedate = $1;
		}
		elsif ($tagline =~ /^recorded (.*)/i) {
			# Recorded 12-21-1962 at Rudy Van Gelder Studio, Englewood Cliffs, NJ
			$recordingdate = $1;
		}
		elsif ($tagline =~ /^UPC (.*)/i) {
			$UPC = $1;
		}
		elsif ($tagline =~ /^Catalog (.*)/i) {
			$catalog = $1;
		}
		elsif ($tagline =~ /^location (.*)/i) {
			$location = $1;
		}
		elsif ($tagline =~ /^genre (.*)/i) {
			$genre = $1;
		}
		elsif ($tagline =~ /^tracks (.*)/i) {
			$totaltracks = $1;
		}
		elsif ($tagline =~ /^discs (.*)/i) {
			$totaldiscs = $1;
		}
		elsif ($tagline =~ /^label /i) {
			$label = $tagline;
			$label =~ s/Label //i;
			$label = $self->trim($label);
		}
		elsif ($tagline =~ /track listing/i) {
			$inband = 0;
			$intrack = 1;
		}
		elsif (($intrack eq 1) && ($tagline =~ /^recorded /i)) {
			$tagline=~ s/\.$//;
			if ($tagline =~ /\s+in\s+(\d\d\d\d)/i) {
				$trackrecordingdate = $1;
				$tagline =~ s/$trackrecordingdate//;
				#$trackrecordingdate = FormatDate($trackrecordingdate);
			}
			elsif ($tagline =~ /(\s+on\s+)(${regExDate})/) {
				$trackrecordingdate = $2;
				$tagline =~ s/$1$2//;
				#$trackrecordingdate = FormatDate($trackrecordingdate);
			}
			my $tracklocation;
			if ($tagline =~ /\s+(at|in)\s+($regExLocation)/i) {
				# location
				$tracklocation = $2;
			}
			if (defined $trackrecordingdate) {
				$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{recordingdate} = $trackrecordingdate;
			}
			if (defined $tracklocation) {
				$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{location} = "Recorded at " . $tracklocation;
			}
		}
		elsif (($intrack eq 1) && ($tagline =~ /^\"(.*)\"/i)) {
			$log .= $1 . "\n";
			my $regextrack = $1;
			$regextrack = $self->FormatTrackNumber($regextrack);
			my $subTitle;
			$subTitle = $1;
			$subTitle =~ s/\(/\\\(/g;
			$subTitle =~ s/\)/\\\)/g;
			$subTitle =~ s/\?/\\\?/g;
			$subTitle =~ s/\-/\\\-/g;
			$tagline =~ s/$subTitle//;
			$tagline =~ s/\(song sample[\s\w,]*\)//;
			$regextrack = $self->Capitalize($regextrack);
			push(@tracklist, $regextrack);

			#$log .= $regextrack; 
			my $formatTrackNum;
			if ($regextrack =~ /^(\d\d?)-(\d+) /) {
				$discnumber = $self->getDiscNum($regextrack);
				$formatTrackNum = $self->getTrackNum($regextrack);
				$formatTrackNum = $self->FormatTrackNumber($formatTrackNum);
			}
			
			$uniqtrack = "$regextrack";
			$filetrack = $uniqtrack;
			$filetrack = unidecode($filetrack);
			if ($uniqtrack ne $filetrack) {
				$log .= "WARNING: $uniqtrack ne $filetrack\n";
				#print "WARNING: $uniqtrack ne $filetrack\n";
			}
		
			if ($tagline =~ /\((\d+[-|\/]\d+[-|\/]\d+)\)/) {
				$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{recordingdate} = $1;
				$tagline =~ s/\($1\)//;
			}
			elsif ($tagline =~ /\((\d\d\d\d)\)/) {
				$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{recordingdate} = $1;
				$tagline =~ s/\($1\)//;
			}
			elsif (!defined $artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{recordingdate}) {
				$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{recordingdate} = $recordingdate;
			}
			if ((!defined $artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{location}) && (defined $location)) {
				$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{location} = "Recorded at " . $location;
			}

			$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{totaldiscs} = $totaldiscs;
			$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{totaltracks} = $totaltracks;
			$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{title}	= $regextrack;
			$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{genre}	= $genre;
			$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{disc}	= $discnumber;
			$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{UPC}	= $UPC if defined $UPC;
			$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{RELEASE_VERSION}	= $catalog if defined $catalog;
			$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{track}	= $formatTrackNum if defined $formatTrackNum;
			$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{releasedate}	= $releasedate;
			$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{label} = $label;
			$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{source} = $source;
			$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{producer} = $producer if defined $producer;
			if ($filetrack) {
				my $riptrackorig = $filetrack;
				my $riptrack = $filetrack;
				my $riptrackalt;
				$riptrackorig = $self->trackNameToWin($riptrackorig);
				foreach my $d (1 ... $totaldiscs) {
					$riptrackorig =~ s/^\d+/$d/;
					$riptrackorig = $self->simplifyString($riptrackorig, 1, 0);
					$riptrackalt = $self->simplifyString($riptrackorig, 1, 1);
					$riptrack = $self->simplifyString($riptrackorig, 0, 1);
					$riptrackorig =~ s/^(\d)(\d\d)/$1-$2 /;
					$riptrackalt =~ s/^(\d)(\d\d)/$1-$2 /;
					$riptrack    =~ s/^(\d)(\d\d)/$1-$2 /;
					if (defined $ripresults->{$riptrackorig}) {
						$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{quality} = $ripresults->{$riptrackorig};
					}
					elsif (defined $ripresults->{$riptrack}) {
						$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{quality} = $ripresults->{$riptrack};
					}
					elsif (defined $ripresults->{$riptrackalt}) {
						$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{quality} = $ripresults->{$riptrackalt};
					}
				}
			}
				
			#if ($tagline =~ /\((\s*${regExComposerName})\)/)
			if ($tagline =~ /\"\s*[<.*>]?\s*\((.*)\)/)
			{
				my $composer = $1;
				$composer = $self->Capitalize($composer);
				$composer =~ s/\s*\/\s*/, /g;
				$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{composer} = $composer;							 
			}
		
			if ($tagline =~ /<(.*)>/) {
				my $comment = $1;
				$comment = $self->trim($comment);
				$artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{comment} = $comment;							 
			}
			$totalTracks{$totaldiscs} = $tracknum;
			$tracknum++;
		}
		elsif (($intrack eq 1) && ($tagline =~ /:/i)) {
			# track specific band info
			my $instrument;
			my $instruments;
			my $member;
			my $members;
			my @members;
			my @instruments;
			$tagline =~ s/vocal(\s|$)/vocals/i;
			$tagline =~ s/lead guitars(\s|$)/lead guitar/i;
			$tagline =~ s/\(uncredited\)//i;
			#if (($tagline =~ /:/i) && ($tagline =~ /,/i)) {
				($member, $instruments) = split(/:/, $tagline);
				@instruments = split(/,/, $instruments);
				if (@instruments == 0) {
					push @{ $artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{band}{$member} }, "";
				}
				else {
					foreach $instrument(@instruments) {
						#$artists{$fileartist}{$filealbum}{tracks}{$track}{band}{$member} = $instrument;
						push @{ $artists->{$fileartist}{$filealbum}{tracks}{$filetrack}{band}{$member} }, $self->trim($instrument);
					}
				}
			#}
			#else {
			#		$artists{$fileartist}{$filealbum}{tracks}{$track}{additionalband} = $tagline;
			#}
		}
		elsif ($tagline =~ /personnel/i) {
			$intrack = 0;
			$inband = 1;
		}
		elsif (($inband eq 1) && ($tagline =~ /:|-/i)) {
			while ($tagline =~ /(\d\d?)-(\d\d?)/) {
				my $expnum;
				my $l = $1;
				my $u = $2;
				foreach my $num ($l ... $u) {
					$expnum .= "$num ";
				}
				$expnum =~ s/.$//;
				$tagline =~ s/$l-$u/$expnum/;
			}
			$tagline =~ s/vocal(\s|$)/vocals /i;
			$tagline =~ s/lead guitars(\s|$)/lead guitar /i;
			$tagline =~ s/\(uncredited\)//i;
			my $member;
			my $instruments;
			my $instrument;
			my @instruments;
			($member, $instruments) = split(/:/, $tagline);
			@instruments = split(/,/, $instruments);
			my $albumtrack = $filetrack;
			$albumtrack =~ s/\.(flac|mp3)$//;
			$albumtrack =~ s/128_q0//;
			$albumtrack =~ s/320_q0//;
			$albumtrack = $self->WinToName($albumtrack);
			for $instrument(@instruments) {
				my $thisinstrument;
				my $tracknum;
				my $tracks;
				my @tracks;
				$thisinstrument = $self->trim($instrument);
				if ($thisinstrument !~ /\(tracks\s+/i) {
					# apply to all tracks
					foreach $albumtrack ( keys %{$artists->{$fileartist}{$filealbum}{tracks}} ) {
						#if ( !grep( /^$thisinstrument$/, @{ $artists{$fileartist}{$filealbum}{tracks}{$albumtrack}{band}{$member} } ) ) {
							push @{ $artists->{$fileartist}{$filealbum}{tracks}{$albumtrack}{band}{$member} }, $thisinstrument;
						#}
					}
				}
				else {
					$thisinstrument =~ /(\(tracks [\d\s]*\))/i;
					$tracks = $1;
					$thisinstrument =~ s/\($tracks\)//i;
					$tracks =~ s/\(tracks\s*//i;
					$tracks =~ s/\s*\)//;
					@tracks = split(/\s+/, $tracks);
					for $tracknum(@tracks) {
						my $trackcnt = 1;
						foreach $albumtrack ( sort keys %{$artists->{$fileartist}{$filealbum}{tracks}} ) {
							#if ($tracknum eq CalculateTrack($artists{$fileartist}{$filealbum}{tracks}{$albumtrack}{track}, $artists{$fileartist}{$filealbum}{tracks}{$albumtrack}{disc}, \%totalTracks)) {
							if ($tracknum == $trackcnt) {
								push @{ $artists->{$fileartist}{$filealbum}{tracks}{$albumtrack}{band}{$member} }, $self->trim($thisinstrument);											
							}
							$trackcnt++;
						}
					}											
				}
			}
		}
	}
	
	for my $Artist (sort keys %$artists) {
		for my $Album (sort keys %{$artists->{$Artist}}) {
			my @tracks = sort {$artists->{$fileartist}{$filealbum}{tracks}{$a}{disc}*100+$artists->{$fileartist}{$filealbum}{tracks}{$a}{track}<=>$artists->{$fileartist}{$filealbum}{tracks}{$b}{disc}*100+$artists->{$fileartist}{$filealbum}{tracks}{$b}{track}} keys %{$artists->{$fileartist}{$filealbum}{tracks}};
			
			foreach my $Track (@tracks) {
				my $hrTrack = $artists->{$fileartist}{$filealbum}{tracks}{$Track};
				$hrTrack->{artist} = $artist;
				$hrTrack->{album} = $artists->{$fileartist}{$filealbum}{album};
	
				my $bandlisting = "";
				for my $member (sort keys %{$artists->{$fileartist}{$filealbum}{tracks}{$Track}{band}}) {
					my %seen = ();
					my $instrument;
					my @uniqinstruments;
			
					# eliminate duplicate instruments
					foreach $instrument (@{$artists->{$fileartist}{$filealbum}{tracks}{$Track}{band}{$member}}) {
						push(@uniqinstruments, $instrument) unless $seen{$instrument}++;
					}
				
					if ((@uniqinstruments == 0) || (length($uniqinstruments[0]) == 0)) {
						$bandlisting = $bandlisting . "${member}";
					}
					else {
						$bandlisting = $bandlisting . "${member}:";
					}

					# print instrument list for this member
					foreach $instrument (@uniqinstruments) {
						$bandlisting = $bandlisting . "${instrument}, ";
					}
				
					$bandlisting =~ s/..$/\\\\\\\\/;
				}
				if ($bandlisting =~ /, /) {
					$bandlisting =~ s/, $//;
				}
				if ($bandlisting =~ /\\$/) {
					$bandlisting =~ s/\\+$//;
				}
				$hrTrack->{bandlisting} = $bandlisting;
			}
		}
	}
				
	return($artists);
}

sub writeTagsFile
{
	my $self = shift;
	my $TAG;
	
	my $tagFile = shift @_;
	my $albuminfo = shift @_;
	
	open($TAG, ">:encoding(UTF-8)", "$tagFile") or die $!;
	
	print $TAG "Artist ";
	if (defined $albuminfo->{hdr}{artist}) {
		#my $tagArtist = encode( 'utf8', $albuminfo->{hdr}{artist} );
		my $tagArtist = $albuminfo->{hdr}{artist};
		print $TAG "$tagArtist" ;
	}
	print $TAG "\nAlbum ";
	if (defined $albuminfo->{hdr}{album}) {
		#my $tagAlbum = encode( 'utf8', $albuminfo->{hdr}{album} );
		my $tagAlbum = $albuminfo->{hdr}{album};
		print $TAG "$tagAlbum" ;
	}
	print $TAG "\nSource ";
	print $TAG "$albuminfo->{hdr}{source}" if defined $albuminfo->{hdr}{source};
	print $TAG "\nReleased ";
	print $TAG "$albuminfo->{hdr}{releasedate}" if defined $albuminfo->{hdr}{releasedate};
	print $TAG "\nRecorded ";
	print $TAG "$albuminfo->{hdr}{recordingdate}" if defined $albuminfo->{hdr}{recordingdate};
	print $TAG "\nProducer ";
	if (defined $albuminfo->{hdr}{producer}) {
		#my $tagProducer = encode( 'utf8', $albuminfo->{hdr}{producer} );
		my $tagProducer = $albuminfo->{hdr}{producer};
		print $TAG "$tagProducer" ;
	}
	print $TAG "\nLabel ";
	if (defined $albuminfo->{hdr}{publisher}) {
		#my $tagLabel = encode( 'utf8', $albuminfo->{hdr}{publisher} );
		my $tagLabel = $albuminfo->{hdr}{publisher};
		print $TAG "$tagLabel" ;
	}
	print $TAG "\nGenre ";
	print $TAG "$albuminfo->{hdr}{genre}" if defined $albuminfo->{hdr}{genre};
	print $TAG "\nDiscs ";
	print $TAG "$albuminfo->{hdr}{totaldiscs}" if defined $albuminfo->{hdr}{totaldiscs};
	print $TAG "\nTracks ";
	print $TAG "$albuminfo->{hdr}{totaltracks}" if defined $albuminfo->{hdr}{totaltracks};
	print $TAG "\n\nTrack Listing\n";
	
	for my $tracknum (sort {$a <=> $b} keys %{$albuminfo->{tracks}}) {
		#my $tagTrack = encode( 'utf8', $albuminfo->{tracks}{$tracknum} );
		my $tagTrack = $albuminfo->{tracks}{$tracknum};
		print $TAG "$tagTrack\n";
	}
	
	close($TAG);
}

sub checkFileExistence {
	my $self = shift;
	my $tracks = shift @_;
	my $albumPath = shift @_;
	my $log;
		
	if (keys %$tracks) {
		foreach my $albumtrack1 ( keys %$tracks ) {
			my $albumtrack = $self->trackNameToWin($albumtrack1);
			#$albumtrack = decode('utf8', $albumtrack);
			my $musicfile1 = catfile($albumPath, $albumtrack) . ".flac";
			my $musicfile2 = catfile($albumPath, $albumtrack) . ".mp3";
			my $musicfile3 = catfile($albumPath, $albumtrack) . "320_q0.mp3";
			my $musicfile4 = catfile($albumPath, $albumtrack) . "128_q0.mp3";
			if ((!-e $musicfile1) && (!-e $musicfile2) && (!-e $musicfile3) && (!-e $musicfile4)) {
				$log .= "WARNING: track name mismatch: $musicfile1\n";
			}
		}
	}
}

sub parseTrackNum
{
	my $self = shift;
	my $track = shift;
	my $title = $track;
	my $disc = 1;
	my $tracknum;
	
	if ($track =~ /^(\d-\d\d )/) {
		$title =~ s/$1//;
		my @parts = split(/[- ]/, $track);
		$disc = shift @parts;
		$tracknum = shift @parts;				
	}
	elsif ($track =~ /(^\d \d\d )/) {
		$title =~ s/$1//;
		my @parts = split(/ /, $track);
		$disc = shift @parts;
		$tracknum = shift @parts;				
	}
	elsif ($track =~ /(^\d\d? )/) {
		$title =~ s/$1//;
		my @parts = split(/ /, $track);
		$tracknum = shift @parts;				
	}
	else{
		# expecting track to begin with a digit
		$tracknum = 1;
	}
	my $dt = $self->FormatTrackNumber($disc . "-" . $tracknum) . " " . $title;
	return($dt);
}

sub getTrackNum
{
	my $self = shift;
	my @f;
	my $track = shift @_;
	@f = split(/\//, $track);
	if (scalar(@f) < 2) {
		@f = split(/\\/, $track);
	}
	my $f = pop(@f);
	$f =~ /^(\d+)-0?(\d+)/;
	my $trackNum = $2;
	return($trackNum);
}
sub getDiscNum
{
	my $self = shift;
	my @f;
	my $disc = shift @_;
	@f = split(/\//, $disc);
	if (scalar(@f) < 2) {
		@f = split(/\\/, $disc);
	}
	my $f = pop(@f);
	$f =~ /^(\d+)-0?(\d+)/;
	my $discNum = $1;
	return($discNum);
}

sub simplifyString
{
	my $self = shift;
	my $string = shift @_;
	my $bRemoveParens = shift @_;
	my $bRemoveNonChars = shift @_;
	
	$string = lc($string);
	$string =~ s/\s*[\{\(\[].*[\}\)\]]\s*// if $bRemoveParens;
	$string =~ s/ and //g;
	$string =~ s/[,\s\;\=\!\:\-\'\?\&\/_]//g;
	$string =~ s/\.wav$//;
	$string =~ s/\.flac$//;
	$string =~ s/\.mp3$//;
	$string =  unidecode($string);
	$string =~ s/[^a-zA-Z0-9]*//g if $bRemoveNonChars;
	
	return($string);
}


sub readLogs
{
	my $self = shift;
	my %ripresults;
	my $EACLogDir = shift @_;
	
	#my $asciitrackpath = pathToAscii($EACLogDir);
	
	print "processing: $EACLogDir\n";
	opendir(DIR, "$EACLogDir");
	my @logfiles = grep(/\.log$/,readdir(DIR));
	closedir(DIR);
	#$DB::single = 1;
	#pathToUnicode($EACLogDir);
		
	# consider all the logfiles in the album dir
	my $bFoundRip;
	foreach my $file (@logfiles) {
		my $riptrack;
		my $riptrackalt;
		my $riptrackorig;
		#$file = decode( 'utf8', $file );
		#my $thislogfile = Win32::GetShortPathName("$EACLogDir/$file");
		my $thislogfile = "$EACLogDir/$file";
		my $thisutf8logfile = $thislogfile;
		$thisutf8logfile =~ s/\.log$/utf8.log/i; 
		#$DB::single = 1;
		system("piconv -f windows-1252 -t UTF-8 < \"$thislogfile\" > \"$thisutf8logfile\"");
		open (my $eaclogfile, "<:encoding(UTF-8)", $thisutf8logfile) or die $!;
		$/ = undef;
		my $line = <$eaclogfile>;
		close $eaclogfile;
		DeleteFile($thisutf8logfile);
		#DeleteFileW(wchar($thisutf8logfile));
		my @lines = split(/\n/, $line);
				
		my $intrack = 0;
		my $verified = "unverified";
		foreach my $line (@lines) {
			#print $line . "\n";
			if (($intrack == 0) && ($line =~ /filename/i)) {
				$intrack = 1;
				my @a = split(/\\/, $line);
				$riptrackorig = pop @a;
				$riptrackorig = "1-" . $riptrackorig;
				$riptrackorig = $self->trackNameToWin($riptrackorig);
				#$riptrack = encode('utf8', $riptrack);
				$riptrackorig = $self->simplifyString($riptrackorig, 1, 0);
				$riptrackalt = $self->simplifyString($riptrackorig, 1, 1);
				$riptrack = $self->simplifyString($riptrackorig, 0, 1);
				$riptrackorig =~ s/^(\d)(\d\d)/$1-$2 /;
				$riptrackalt =~ s/^(\d)(\d\d)/$1-$2 /;
				$riptrack    =~ s/^(\d)(\d\d)/$1-$2 /;
			}
			elsif ($intrack && ($line =~ /^Track/i)) {
				$intrack = 0;
			}
			elsif ($intrack && ($line =~ /Suspicious position/i)) {
				$verified = "EAC Error";
				$intrack = 0;
				$ripresults{$riptrack} = $verified;
				$ripresults{$riptrackalt} = $verified;
				$ripresults{$riptrackorig} = $verified;
			}
			elsif ($intrack && ($line =~ /Cannot be verified/i)) {
				$verified = "Accurate rip unverified";
				$intrack = 0;
				$ripresults{$riptrack} = $verified;
				$ripresults{$riptrackalt} = $verified;
				$ripresults{$riptrackorig} = $verified;
			}
			elsif ($intrack && ($line =~ /Accurately ripped/i)) {
				$verified = "Accurate rip verified";
				$intrack = 0;
				$ripresults{$riptrack} = $verified;
				$ripresults{$riptrackalt} = $verified;
				$ripresults{$riptrackorig} = $verified;
			}
			elsif ($intrack && ($line =~ /Copy OK/i)) {
				$verified = "Accurate rip unverified";
				$intrack = 0;
				$ripresults{$riptrack} = $verified;
				$ripresults{$riptrackalt} = $verified;
				$ripresults{$riptrackorig} = $verified;
			}
			elsif ($intrack && ($line =~ /Copy aborted/i)) {
				$verified = "EAC Error";
				$intrack = 0;
				$ripresults{$riptrack} = $verified;
				$ripresults{$riptrackalt} = $verified;
				$ripresults{$riptrackorig} = $verified;
			}
			elsif ($intrack && ($line =~ /Track not present/i)) {
				$verified = "Accurate rip unverified";
				$intrack = 0;
				$ripresults{$riptrack} = $verified;
				$ripresults{$riptrackalt} = $verified;
				$ripresults{$riptrackorig} = $verified;
			}
		}
	}
	
	return(\%ripresults);
}

=head1 AUTHOR

  © 2014 Steve Coward <steve-coward@comcast.net>
 *All rights reserved. No warranty, explicit or implicit, provided.
 In no event shall the author be liable for any claim or damages.

=cut

1;

