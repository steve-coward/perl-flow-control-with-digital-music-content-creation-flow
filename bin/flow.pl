
=head1 NAME

=head1 SYNOPSIS

      perl flow.pl -f MusicFlow

=head1 DESCRIPTION

  This is an application that performs Flow Control.

=head2 MOTIVATION

  There are several motivations for developing and using a methodology flow
  control platform.  In engineering environments, adherence to design
  methodologies is critical to the success of the project.
  Yet tool flows and design methodologies can be quite complex.  MFC can reduce
  the apparent complexity of a flow by hiding flow details from the user and
  providing easy access to information pertinent to the user.  Such information
  includes command line option settings, log file contents and help information.
  Furthermore, MFC can simplify these flows by ensuring proper step invocation
  and ordering and providing basic results/error interpretation.

  By providing essential services to the underlying flow, MFC reduces the
  development time of that flow. MFC provides the following services: log file
  maintenance, stepwise help information display, flow state restoration upon
  restart, a standard GUI to control and monitor the flow and parallel processing
  capabilities.  
  
  Usage of MFC will reduce the need for user training.  Furthermore, training
  benefits will multiply as additional flows are built on the MFC platform since
  users will already be familiar with it.  Since MFC provides a standard GUI,
  provides one click access to step options, results and help information and
  hides unnecessary flow detail from the user, the training process is vastly
  quicker.  Most basic user support questions will no longer be posed to the
  project software support team. Questions such as “When do I run this step?”,
  “Did this step pass?”, and “Am I done?” will be answered automatically by MFC.
  
  Tool versioning and environment variable setting issues will also be greatly
  reduced.  At initialization, MFC automatically reads an init file which is
  maintained by the tool owner and defines paths (and hence versions) to all
  components used by the flow.

=head2 OPPORTUNITY

  There are several properties that a tool flow should manifest that will
  determine the applicability of using MFC to run the flow.  A tool flow should
  be comprised of multiple, discrete components since the sequencing and stitching
  of multiple steps is a core function that MFC provides.  The tool flow should
  not necessitate or benefit from a flow specific GUI since MFC provides only a
  generic GUI that would not be suitable.  Finally the tool should be targeted at
  the in-house design team since the generic solution provided by MFC is probably
  not suitable for external productization. Any tool flow meeting these three
  criteria will make a good candidate for support by MFC.  In the authors opinion,
  most design environments will have several opportunities to employ an
  MFC-like tool.

=head2 IMPLEMENTATION

  MFC is written in Perl and uses the Tk tool kit to implement the GUI.
  Two tenets were used in designing MFC.  First MFC should absorb a maximum amount
  of functionality from the underlying flows to be supported.  This maximizes
  software reuse and simplifies creation of the underlying flows. Secondly MFC
  should never incorporate functionality specific to any proper subset of flows.
  With this in mind, MFC is able to provide the following functionality for all
  underlying flows: GUI, save/restore flow state, log file maintenance, help file
  display and creation, step sequencing and parallel processing capabilities.
  
  For step inclusion in a flow supported by MFC, the following requirements must
  be satisfied. First the name of the flow is defined by creating a bat file
  called <flowname>.bat which defines settings for all environment variables
  used by the flow.  Note that the only environment variables used by MFC are
  ‘Bin_Dir’, which should point to the flow bin directory where the Perl step
  packages reside and ‘Log_Dir’, which should point to the directory where log
  files are maintained. Then an xml file called <flowname>.xml is created.
  Each step must be defined within this xml file.  Step properties such as step
  number (1-based), step package name, step title (to appear on the step
  invocation button in the GUI), threadability and error handling flags are
  defined here. See Appendix A for an example xml file.  Finally a Perl package
  must be created for each step. Within each step package the following
  subroutines must be provided: New(), CheckDependencies(), RunStep(),
  GetOptions(), SetOptions() and QueryOptions().
  New() is the step constructor. It will be invoked by MFC during initialization
  to create the step object. CheckDependencies() is called by MFC prior to
  invoking the step by calling RunStep().  CheckDependencies() must return an
  integer signaling whether the step should be run at this time.  RunStep()
  implements the step functionality.  The *Option() routines provide a way to
  save, restore and set command line options for the step.  Appendix B includes
  a starting step template. 
  
  It is worth mentioning here that the implementation of MFC borrows from the
  concepts of both Aspect Oriented Programming and Feature Oriented Programming.
  Since MFC is in complete control of the tool flow, program execution is
  returned back to MFC at the completion of each step of the flow.
  This allows MFC to provide cross-cutting, Aspect oriented services to the flow
  steps.  Secondly the <flowname>.xml file is a type of configuration file whereby
  new tool flows can be easily created by adding or removing steps
  (a.k.a. features)  from the flow in a Feature Oriented manner.

=head2 Methods

=cut

#!/usr/bin/perl
package flow;
  
# Unicode issues
# When running perl in windows command window do
# use Lucida Console font
# chcp 65001

#use diagnostics;
use strict;
#no strict "refs"; # seems to be required for InvokeStep() command invocation
use warnings 'all';
use XML::Simple;
use XML::Parser;
use Win32::FileOp; # for ShellExecute
#use Win32::FileOp qw(ShellExecute);
use Getopt::Std;
use Cwd;
use base 'Exporter';
use Module::Load;
use Cwd 'abs_path';
use File::Basename;
use File::stat;
use File::Spec::Functions;
use utf8;
#use threads;
use threads 1.39;
use Thread::Queue;
use threads::shared;
use Time::HiRes qw(usleep);

# Step object creation
my $flowLibPath;
BEGIN {
	my $ExecPath = dirname(abs_path($0));
	$flowLibPath = abs_path(catfile($ExecPath, "..", "flows"));
	
	# flush to stdout
	$| = 1;
}
use lib "$flowLibPath";

our @EXPORT_OK = qw($MainWindow);

my $flowname;
my @enables;
my $stephash;
my %images;	
my $statehash;
my $TERM : shared = 0;
my $bVerbose;
my $fhLogFile;

my $schedulerThread;
my $schedulerQ; # start flow step signal to scheduler
my $schedulerStepDoneQ; # scheduler step done signal to MainLoop
# $workerQs key: thread ID value: thread workerQ
my %workerQs; # start task signal to worker, per worker
my $workerTaskDoneQ; # worker task done signal to scheduler

&main();

sub main {
	my $numThreads = 3;
	my $nogui = 0;
	my @retvals;
	my @runSteps;
	
	print STDOUT "Running perl version $]\n" if $bVerbose;
			
	($flowname, $nogui, $bVerbose, @runSteps) = parseCommandLine();
	if (sourceFlowBat($flowname)) { die "Error ($flowname): sourcing bat file\n"; }
	if (sourceFlowCfg($flowname)) { die "Error ($flowname): reading cfg file\n"; }
	if (sourceFlowState("flowstate.xml")) { die "Error: reading state file\n"; }
	if (makeFlowSteps($nogui, \@runSteps)) { die "Error ($flowname): making flow steps\n"; }
	if (makeThreadPool($numThreads, \@retvals)) { die "Error ($flowname): making thread pool\n"; }
		$schedulerThread = shift @retvals;
		$schedulerQ = shift @retvals;
		$schedulerStepDoneQ = shift @retvals;
		$workerTaskDoneQ = shift @retvals;
		
	# create Tk gui if necessary in runFlowLoop
	if (runFlowLoop($nogui, @runSteps)) { die "Error ($flowname): running flow loop\n"; };
}

###############################################################################


sub parseCommandLine {
	my %Options;
	my $flowname;
	my $nogui = 0;
	my $verbose = 0;
	my @runSteps;
	my $ok = getopts('s:f:nhv', \%Options);
	if ($ok == 0) {
		return(1);
	}
	
	if (defined $Options{'h'}) {
		PrintHelp();
		exit(0);
	}
	
	if (defined $Options{'v'}) {
		$verbose = 1;
	}
	
	if (!defined $Options{'f'}) {
		print STDOUT "Please specify flowname.\n";
		PrintHelp();
		exit(1);
	}
	$flowname = $Options{'f'};
	
	if (defined $Options{'n'}) {
		$nogui = 1;
		
		if (defined $Options{'s'}) {
			my $optsteps = $Options{'s'};
			my @indsteps = split(',',$optsteps);
			foreach my $step (@indsteps) {
				my @mulsteps = split(/-/, $step);
				if ($#mulsteps == 0) {
					push(@runSteps, $mulsteps[0]);
				}
				else {
					foreach my $mstep ($mulsteps[0] .. $mulsteps[1]) {
						push(@runSteps, $mstep);
					}
				}
			}
		}
	}
	
	return($flowname, $nogui, $verbose, sort @runSteps);
}

sub getLogPath {
	return(catfile($ENV{"${flowname}_Dir"}, "log"));
}
sub getDocPath {
	return(catfile($ENV{"${flowname}_Dir"}, "doc"));
}
sub makeDirs {
	mkdir(getLogPath());
	mkdir(getDocPath());
}

sub runFlowLoop {
	my $nogui = shift;
	my $runSteps = @_;
	
	if ($nogui) {
		my $steps = $#enables + 1;
		@enables = ();
		push(@enables, 0);
		my $stepcount = 1;
		
		foreach $stepcount (1 .. $steps) {
			$enables[$stepcount] = 0;
		}
		foreach my $stepcount (sort @$runSteps) {
			my $stepname = GetStepName($stepcount);
			if ($stephash->{$stepname}->{requiresTk} == 0) {
				$enables[$stepcount] = 1;
			}
		}
		# Never run first
		#$enables[1] = 0;
		
		foreach my $step (sort hashValueAscendingNum (keys(%$stephash))) {
			InvokeStep($step, $nogui);
			last;
		}
	}
	else {
		# Tk initialization							
		# Tk not thread safe
		use Tk;
		use Tk::Dialog;
	
		$flow::MainWindow = MainWindow->new;
		$flow::MainWindow->title("Flow Control: $flowname");
		my $winh = keys(%$stephash) * 40 + 80;
		$flow::MainWindow->geometry("620x$winh");
		$flow::MainWindow->resizable( 0, 0 ); # not resizable in any direction
										
		
		my $mf = $flow::MainWindow->Frame->grid(-pady => 5, -padx => 10);
		foreach my $image qw(help red green yellow left right) {
			my $file = catfile("..", "rsrc", "${image}.gif");
			$images{$image} = $flow::MainWindow->Photo(-file => $file);
		}
		
		my $w;
		my $row;
		foreach my $stepname (sort hashValueAscendingNum (keys(%$stephash))) {
			my $attributehash = $stephash->{$stepname};
			$row = $attributehash->{number};
			
			# Do not mix pack() and grid()
			$w = $mf->Button(
				-image => $images{"yellow"},
				-relief => 'flat');
			$w->grid(-row=>$row,
				-column=>1, -pady=>5, -padx=>4);
			$attributehash->{Indicator} = $w;
			
			$w = $mf->Checkbutton(
				-variable => \$enables[$attributehash->{number}],
				-relief => 'flat');
			$w->grid(
				-row=>$row,
				-column=>2, -pady=>5, -padx=>4);
			$attributehash->{Enable} = $w;
			
			$w = $mf->Button(
				-text => $attributehash->{title},
				-command => [\&InvokeStep, $stepname, $nogui],
				-width => 35);
			$w->grid(
				-row=>$row,
				-column=>3, -pady=>5, -padx=>4);
			$attributehash->{Activate} = $w;
			
			$w = $mf->Button(
				-text => "Options",
				-command => [\&InvokeOptions, $stephash->{$stepname}],
				-width => 10);
			$w->grid(
				-row=>$row,
				-column=>4, -pady=>5, -padx=>4);
			$attributehash->{Options} = $w;
			
			$w = $mf->Button(
				-text => "Log",
				-command => [\&ShowLog, $stepname],
				-width => 10);
			$w->grid(-row=>$row,
				-column=>5, -pady=>5, -padx=>4);
			$attributehash->{Log} = $w;
			
			$w = $mf->Button(
				-image => $images{"help"},
				-command => [\&ShowHelp, $stepname],
				-relief => 'flat');
			$w->grid(
				-row=>$row,
				-column=>6, -pady=>5, -padx=>4);
			$attributehash->{Help} = $w;
		}
		
		$row++;
		$w = $mf->Checkbutton(
			-variable => \$enables[0], -relief => 'flat',
			-command => [\&ToggleEnables, $nogui, $stephash]);
		$w->grid(-row=>$row, -column=>2);
		
		$w = $mf->Button(
			-image => $images{"help"},
			-command => [\&ShowHelp, "FlowControl"],
			-relief => 'flat');
		$w->grid(-row=>$row, -column=>6);

		my $statefile = catfile($ENV{"Cfg_Dir"}, "flowstate.xml");
		$w = $flow::MainWindow->Button(
			-text => "Exit", -command => [\&ExitApplication, $statefile] );
		$w->grid(-ipadx=>12);
		
		# Allow for some post processing upon exit
		$flow::MainWindow->protocol(
			'WM_DELETE_WINDOW', [\&ExitApplication, $statefile] );
		 # intercept Alt-F4, too
		 $flow::MainWindow->bind("<Alt-F4>", [\&ExitApplication, $statefile] );
				
		MainLoop;
	}
}

sub GetStepName {
	my $stepnum = shift;
	
	foreach my $step (keys(%$stephash)) {
		if ($stepnum == $stephash->{$step}->{number}) {
			return($step);
		}
	}
	return("NoSuchStep");
}

sub makeThreadPool {
	my $numThreads = shift;
	my $retVals = shift;
	my $aschedulerThread;
	my $aschedulerQ;
	my $aworkerTaskDoneQ;
	my $aschedulerStepDoneQ;
	
	### Signal Handling ###
	
	# Gracefully terminate application on ^C or command line 'kill'
	$SIG{'INT'} = $SIG{'TERM'} =
		sub {
			print(">>> Terminating <<<\n");
			$TERM = 1;
		};
	
	# Create the worker thread pool
	$aworkerTaskDoneQ = Thread::Queue->new();
	for (1..$numThreads) {
		# Create a work queue for a thread
		my $workerQ = Thread::Queue->new();
	
		# Create the thread, and give it the work queue
		my $thr = threads->create('worker', $workerQ, $aworkerTaskDoneQ, $stephash);
	
		# Remember the thread's work queue
		$workerQs{$thr->tid()} = $workerQ;
	}
	
	# Create the scheduler thread
	$aschedulerQ = Thread::Queue->new();
	$aschedulerStepDoneQ = Thread::Queue->new();
	$aschedulerThread = threads->create('schedulerNew', $aschedulerQ, $aschedulerStepDoneQ, $aworkerTaskDoneQ, \%workerQs, $stephash);

	push(@$retVals, $aschedulerThread, $aschedulerQ, $aschedulerStepDoneQ, $aworkerTaskDoneQ);
	return(0);
}

sub makeFlowSteps {
	my $noGui = shift;
	my $runSteps = shift;
	my $count = 0;
	my $package;
	my $step;

	# steps are numbered starting with 1
	# arrays are indexed starting with 0
	# so push a 0 here to sync
	@enables = ();
	push(@enables, 0);
	
	if ($noGui) {
		@$runSteps = (0..keys(%$stephash))
	}
	
	foreach my $stepname (sort hashValueAscendingNum (keys(%$stephash))) {
		$count++;
		my $attributehash = $stephash->{$stepname};
		if ($bVerbose) {
			print STDOUT "Step $stepname:\n";
			while (my ($key, $value) = each %$attributehash) {
				print STDOUT "	$key $value\n";
			}
		}
	
		$package = $attributehash->{package};
		$package =~ s/\"//g;
		
		my $mod;
		$mod = "${flowname}::bin::$package";
		eval "
			use $mod;
		";
		if ($@) {
			print "Error loading flow package $mod :\n$@\n";
			return(1);
		}
	
		# create new step object
		# These generally use Tk
		$step = $mod->new(name => $stepname);
		#$step->SetOptions();
		$stephash->{$stepname}{Module} = $step;
						
		# steps are disabled by default
		#print STDOUT "$stepname\n";
		if (defined $statehash->{$stepname}->{enabled}) {
			push(@enables, $statehash->{$stepname}->{enabled});
		}
		else {
			push(@enables, 0);
		}
		
		#print STDOUT "$stepname\n";
		if (defined $statehash->{$stepname}->{options}) {
			my $opthash = $statehash->{$stepname}->{options};
			$stephash->{$stepname}{Module}->SetOptions($opthash);
		}
		if ($count > 100) {
			last;
		}
	}
	return(0);
}

sub sourceFlowState {
	my $filename = shift;
	
	my $statefile = catfile($ENV{"Cfg_Dir"}, $filename);
	if (-e $statefile && -r $statefile) {
	
		# initialize parser object and parse the string
		my $parser = XML::Simple->new( );
		my $parsetree = $parser->XMLin( $statefile );
	 
		# ISSUE: Should sanity check xml step definitions here
		#print STDOUT "$parsetree->{step}->{validate}->{title}\n";

		# report any error that stopped parsing, or announce success
		if( $@ ) {
			$@ =~ s/at \/.*?$//s; # remove module line number
			print STDERR "\nERROR in '$statefile':\n$@\n\n";
			return(1);
		}
		else {
			print STDOUT "'$statefile' is well-formed\n\n";
		}
		
		if ($bVerbose) {
			# serialize the structure
			use Data::Dumper;
			print Dumper( $parsetree );
		}
		
		my $stepvalue;
		($statehash, $stepvalue) =  $parsetree->{step};
	}
	else {
		# initialize data structure
		foreach my $stepname (sort hashValueAscendingNum (keys(%$stephash))) {
			$statehash->$stepname->{enabled} = 1;
		}
	}
	return(0);
}

sub sourceFlowCfg {
	my $flowname = shift;
	
	my $xmlfile = catfile($ENV{"Cfg_Dir"}, $flowname . ".xml");
	if (-e $xmlfile && -r $xmlfile) {
		# initialize parser object and parse the string
		my $parser = XML::Simple->new( );
		my $parsetree = $parser->XMLin( $xmlfile );
	 
		# report any error that stopped parsing, or announce success
		if( $@ ) {
			$@ =~ s/at \/.*?$//s; # remove module line number
			print STDERR "\nERROR in '$xmlfile':\n$@\n\n";
			return(1);
		}
		else {
			print STDOUT "'$xmlfile' is well-formed\n\n";
		}
	
		if ($bVerbose) {
			# serialize the structure
			use Data::Dumper;
			print Dumper( $parsetree );
		}
		
		my $stepvalue;
		($stephash, $stepvalue) =  $parsetree->{step};
	}
	else {
		print STDERR "\nmissing flow definition file $xmlfile.\n";
		return(1);
	}
	return(0);
}

sub sourceFlowBat {
	my $flowname = shift;

	my $ExecPath = dirname(abs_path($0));
	my $batFile = catfile(
		$ExecPath, "..", "flows", $flowname, "cfg", $flowname . "Win.bat");
	my $fileerror = 0;
	if (-e $batFile && -f $batFile) {
		#system("$batFile");
		open(BAT, "$batFile") or die $!;
		while (my $line = <BAT>) {
			chomp $line;
			$line =~ s/#.*//;		
			$line =~ s/^set //;
			my @params = split(/=/, $line);
			if (@params == 2) {
				if ($params[1] =~ /\$ENV{(.*)}/) {
					my $envarg = $1;
					$envarg =~ s/\'//g;
					$envarg =~ s/\"//g;
					$params[1] =~ s,\$ENV{.*},$ENV{$envarg},;
				}
				if (!-e $params[1]) {
					print STDOUT "WARNING! $batFile: $params[1] does not exist.\n";
					#$fileerror = 1;
				}
				$ENV{$params[0]} = $params[1];
			}
		}
		close(BAT);
		#print STDOUT "$ENV{Music_Dir}\n";
		if ($fileerror) {
			return(1);
		}
	}
	else {
		print STDERR "\nmissing flow batch file $batFile.\n";
		return(1);
	}
	
	makeDirs();
	
	return(0);
}

sub ExitApplication {
	my $statefile = shift;
	my $stepopts;
	my $stepopt;
	
	# Signal all threads that there is no more work
	print STDOUT "Signal threads to close.\n";
	$schedulerQ->enqueue(undef);
	$workerQs{$_}->enqueue(undef) foreach keys(%workerQs);

	# Save flow options
	if (0) {
		print STDOUT "Saving flow options in $statefile.\n";
		open(SAVE, ">$statefile") or die $!;
		print SAVE "<state>\n";
	
		foreach my $stepname (sort hashValueAscendingNum (keys(%$stephash))) {
			print SAVE "  <step name=\"$stepname\">\n";
			if (defined $enables[$stephash->{$stepname}->{number}]) {
				my $num = $enables[$stephash->{$stepname}->{number}];
				print SAVE "    <enabled>$num</enabled>\n";
			}
			print SAVE "    <options>\n";
			if (defined $stephash->{$stepname}) {
				$stepopts = $stephash->{$stepname}{Module}->GetOptions();
				foreach $stepopt (keys (%$stepopts)) {
					my $opt = $stepopts->{$stepopt};
					print "      <$stepopt>$opt</$stepopt>\n";
					print SAVE "      <$stepopt>$opt</$stepopt>\n";
				}
			}
			print SAVE "    </options>\n";
			print SAVE "  </step>\n\n";
		}
		print SAVE "</state>\n";
		close(SAVE);
	}
	
	for my $image (keys %images) {
		$images{$image}->delete;
	}

    # Wait for all the threads to finish
    print "Wait\n";
	$schedulerThread->join();
	$_->join() foreach threads->list();
	
	# Could prompt user to confirm here
	print "Exit\n";
	exit;
}

sub hashValueAscendingNum {
   $stephash->{$a}->{number} <=> $stephash->{$b}->{number};
}

sub InvokeStep {
	my $startstepname = shift;
	my $nogui = shift;
	my $step;
	my $starthere = 0;
	my @threads;
	my $steprunning = 0;
	
	foreach $step (sort hashValueAscendingNum (keys(%$stephash))) {
		if ($starthere == 0) {
			if ($step eq $startstepname) {
				$starthere = 1;
			}
			else {
				next;
			}
		}
		
		if ($starthere) {
			my $log_time;
			my $logFile = catfile(getLogPath(), $step . ".log");
			if (-e $logFile) {
				$log_time = stat($logFile)->mtime;
			}
			else {
				open(F, ">$logFile") && close F;
				$log_time = stat($logFile)->mtime;
			}

			if ($enables[$stephash->{$step}->{number}] != 0) {
				my $depCheck = $stephash->{$step}{Module}->CheckDependencies($log_time);
				#$DB::single = 1;
				if ($depCheck > 0) {
					# create log file for step
					open ($fhLogFile, ">:encoding(UTF-8)", $logFile);
					select((select($fhLogFile), $| = 1)[0]);
					print $fhLogFile "Starting $step at ";
			
					my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)
						= localtime(time);
					# $year contains no. of years since 1900,
					# add 1900 to make Y2K compliant
					$year += 1900;
					my @month = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
					print $fhLogFile "$month[$mon] $mday $year $hour:$min:$sec\n";

					my $command = $stephash->{$step}->{command};
					my $retval;
					# Dispatch command
					print STDOUT "Dispatch step $step.\n";
					$steprunning = 1;
					if (defined $stephash->{$step}->{threads}) {
						if ($stephash->{$step}->{threads} > 0) {
							# Run in separate thread
	
							# Signal all threads that there is no more work
							# Passing unshared values between threads is
							# accomplished by serializing the specified values
							# using Storable when enqueuing and de-serializing
							# the queued value on dequeuing. This allows for great
							# flexibility at the expense of more CPU usage.
							# It also limits what can be passed, as e.g.
							# code references can not be serialized.
							# code references can not be passed.
							my $numThreadsToUse = $stephash->{$step}->{threads};
							my @stepinfo = ($step, $numThreadsToUse);
							$schedulerQ->enqueue(\@stepinfo);
						}
						else {
							print $fhLogFile "Step $step starts\n";
							$retval = $stephash->{$step}{Module}->RunStep($logFile);
							print $fhLogFile "Step $step exits with status $retval\n";
							$schedulerStepDoneQ->enqueue($step);
						}
					}
			
					#$timer->cancel;
					my $donestep = $schedulerStepDoneQ->dequeue(1);
					printf("(%2d) Finished step $step.\n", threads->tid());
					if (defined $retval) {
						if ($retval == 0) {
							updateImage($stephash->{$step}, $images{"green"}, $nogui);
						}
						else {
							updateImage($stephash->{$step}, $images{"red"}, $nogui);
							close $fhLogFile;
							last;
						}
					}
					close $fhLogFile;

				}
				elsif ($depCheck < 0) {
					print STDOUT "A previous step must run before this step can run.\n";
					updateImage($stephash->{$step}, $images{"red"}, $nogui);
					last;
				}
				else {
					print STDOUT "Skipping $step.\n";
					updateImage($stephash->{$step}, $images{"green"}, $nogui);
				}
			}
		}
		
		#last;
	}
}

sub updateImage {
	my $stepref = shift;
	my $image = shift;
	my $nogui = shift;
	$stepref->{Indicator}->configure(-image => $image) if !$nogui;
	$stepref->{Indicator}->update() if !$nogui;
}

sub InvokeOptions {
	my($stepref) = shift @_;
	
	if ($enables[$stepref->{number}] != 0) {
		$stepref->{Module}->QueryOptions();
	}
}
	
sub ShowLog {
	my($stepname) = shift @_;
	my $logfile;
	my $htmlfile;
	my $exitval = 0;
	my @line;
	
	$logfile = getLogPath() . "\\" . $stepname . ".log";
	$htmlfile = getLogPath() . "\\" . $stepname . ".html";
	print STDOUT "Displaying $htmlfile\n";
	if (-e $logfile && -r $logfile) {
		open (LOG, "$logfile") or die $!;
		open (OUT, ">$htmlfile") or die $!;
		
		print OUT "<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\">\n\n";
		print OUT "<html>\n";
		print OUT "<head><title></title></head>\n";
		print OUT "<body>\n";
	
		$/ = "\n";
		while(my $line = <LOG>){
			chomp $line;
			print OUT "<br>$line</br>";
		}
		print OUT "</body>\n";
		print OUT "</html>\n";
		close(LOG);
		close(OUT);
		
		# ShellExecute not available on later Perl versions
		# use system("start ...) instead
		if ($] < 5.011) {
			#print STDOUT "openexplorer\n";
			#$exitval= ShellExecute('open', "$htmlfile", '', '', 1);
			$exitval = system("\"$ENV{Open_Exe}\" \"$htmlfile\"");
		}
		else {
			#print STDOUT "start explorer\n";
			$exitval = system("start iexplore.exe \"$htmlfile\"");
		}
		
		#DeleteFileW(wchar("$htmlfile")); 
	}
	
	return($exitval);
}

sub ToggleEnables {
	my $nogui = shift;
	my $stephash = shift;
	my $val = $enables[0];
	
	 if (!$nogui) {
		if ($val) {
			foreach my $stepname (keys(%$stephash)) {
				$stephash->{$stepname}->{Enable}->select();
				$stephash->{$stepname}->{Enable}->configure();
			}
		}
		else {
			foreach my $stepname (keys(%$stephash)) {
				$stephash->{$stepname}->{Enable}->deselect();
				$stephash->{$stepname}->{Enable}->configure();
			}
		}
	}
}

sub ShowHelp {
	my($step) = shift @_;
	my $htmldir = catfile($ENV{"${flowname}_Dir"}, "doc");
	my $outfile = catfile($htmldir, $step . "-help.html");
	my $infile;
	if ($step eq "FlowControl") {
		$infile = abs_path($0);
	}
	else {
		$infile = catfile($ENV{"${flowname}_Dir"}, "bin", "$step.pm");		
	}
	my $exitval;
	
	my $cmd;
	$cmd = "pod2html --title=${flowname}::$step \"$infile\" --htmldir=\"$htmldir\" --outfile=\"$outfile\"";
	system($cmd);
	
	# ShellExecute not available on later Perl versions
	# use system("start ...) instead
	if ($] < 5.011) {
		$exitval= ShellExecute('open', "$outfile", '', '', 1);
	}
	else {
		$exitval = system("start iexplore.exe \"$outfile\"");
	}
	return($exitval);
}

sub PrintHelp {
	# my $ok = getopts('s:f:nh', \%Options);
	print STDOUT "\nperl flow.pl -h|-f <flowname> [-n -s [[\\d|\\d-\\d][,\\d|,\\d-\\d]*]\n";
	print STDOUT "-h            : print this help info.\n";
	print STDOUT "-f <flowname> : flowname is name of xml file defining flow.\n";
	print STDOUT "-n            : run in batch mode - no gui displayed.\n";
	print STDOUT "-s [[\\d|\\d-\\d][,\\d|,\\d-\\d]*]: list of steps to run.\n";
	print STDOUT "\nExample:\n  perl flow.pl -f MusicFlow -n -s 2,3,4,7-9\n";
}

# A worker thread - one or more of these
sub worker
{
    my ($taskQ, $taskDoneQ, $astepref) = @_;
	my $astep;
	my $task;
	my $threads;
	my $taskInfo;

    # This thread's ID
    my $tid = threads->tid();

	# flush to stdout
	$| = 1;
	
    # Work loop
    do {
        # Wait for work from the queue
		$taskInfo = $taskQ->dequeue(1);
        my $log = sprintf("(%2d) got one\n", $tid);
		
		# Do some work while monitoring $TERM
        if (! $TERM && (defined $taskInfo)) {
	        ($astep, $task) = @$taskInfo;
	        #printf("Thread %d step %s beginning task %s \n",
			#	$tid, $astep, $task);
	        $log .= sprintf("(%2d) running %s\n", $tid, $task);
			# $stepval = threadID, log, return value
			my $stepval = $astepref->{$astep}{Module}->RunStep($task);
			$log .= $stepval->[1];
			$log .= sprintf("(%2d) enqueueing %s\n", $tid, $task);
			my @ret = ($task, $stepval->[0], $stepval->[2], $log);
			$taskDoneQ->enqueue(\@ret);
        }

        # Loop back to idle state if not told to terminate
    } while (! $TERM && (defined $taskInfo));

    # All done
    printf("(%2d) Worker exiting!\n", $tid);
}

# A scheduler thread - only on of these
sub schedulerNew
{
    my ($schedulerQ, $schedulerStepDoneQ, $workerTaskDoneQ, $workerQs, $astepref) = @_;
	my $astep;
	my $logfile;
	# the number of threads this step is requesting
	my $numThreadsReq;
	my $tid;
	my $stepInfo;
	my $taskInfo;
	my $fh;
	
	# flush to stdout
	$| = 1;

    #$DB::single = 1;
	$tid = threads->tid();	
	
	# worker thread task queue size ceiling
	# if zero then there is no ceiling
	my $threadEnqueueLimit = 1;
	
	# in seconds
	my $taskDequeueTimeout = 2;
	
	# the number of worker threads running
	my $numThreads = scalar keys %workerQs;

    # Work loop
    do {
        # Wait for work from the queue
		# This will specify step to run
        $stepInfo = $schedulerQ->dequeue(1);
		
		# Do the work while monitoring $TERM
        if (! $TERM && (defined $stepInfo)) {
			($astep, $numThreadsReq) = @$stepInfo;
			
			my $logFile = catfile(getLogPath(), $astep . $tid . ".log");
			# create log file for step
			open ($fh, ">:encoding(UTF-8)", $logFile);
			select((select($fh), $| = 1)[0]);
								 
			my $threadsToUse = $numThreads <= $numThreadsReq ? $numThreads : $numThreadsReq;
			my $taskType = $stephash->{$astep}->{taskType};
			my $taskList = $astepref->{$astep}{Module}->getTasks($taskType);
			my $numTasks = scalar @$taskList;
			my $numTasksCompleted = 0;
			my $remTasksToEnqueue = scalar @$taskList;
	        
			printf($fh "(%2d $astep) launching %d tasks using %d threads\n",
				$tid, $numTasks, $threadsToUse);
			
			while ($remTasksToEnqueue || ($numTasksCompleted < $numTasks)) {
				my $task;
				foreach my $wtid (1 .. $threadsToUse) {
					if (($remTasksToEnqueue) && $threadEnqueueLimit && ($workerQs{$wtid}->pending() < $threadEnqueueLimit)) {
						$task = shift @$taskList;
						$remTasksToEnqueue--;
						my @taskinfo = ($astep, $task);
						printf($fh "(%2d $astep) Enqueueing $task (%d out of %d)\n", $wtid, ($numTasks - $remTasksToEnqueue), $numTasks);
						$workerQs{$wtid}->enqueue(\@taskinfo);
						printf($fh "(%2d $astep) done Enqueueing $task\n", $wtid);
						if ($remTasksToEnqueue == 0) {
							printf($fh "(%2d $astep) done enqueue\n", $tid);
						}
					}
				}
				
				$taskInfo = $workerTaskDoneQ->dequeue(1);
				$numTasksCompleted++;
				# task, id, return value, log
				printf($fh "(%2d $astep) Job status: %d/%d\n", $tid, $numTasksCompleted, $numTasks);
				printf($fh "(%2d $astep) Task: %s exit status %d\n", $taskInfo->[1], $taskInfo->[0], $taskInfo->[2]);
				#printf($fh "(%2d $astep) Log:\n", $taskInfo->[1]);
				#printf($fh "                 %s\n", $taskInfo->[3]);
				printf($fh "DONE PRINT\n");
			}
				
			$schedulerStepDoneQ->enqueue($astep);
			printf($fh "(%2d $astep) Completed\n", $tid);
		}

        # Loop back to idle state if not told to terminate
    } while (! $TERM && (defined $stepInfo));

    # All done
    #printf("(%2d) Scheduler exiting!\n", $tid);
	close($fh);
}

sub scheduler
{
    my ($schedulerQ, $schedulerStepDoneQ, $workerTaskDoneQ, $workerQs, $astepref) = @_;
	my $astep;
	my $logfile;
	# the number of threads this step is requesting
	my $numthreadsreq;
	my $tid;
	my $qinfo;
	my $numTasks = 0;
	
	# flush to stdout
	$| = 1;

    $tid = threads->tid();
	# the number of worker threads running
	my $numthreads = scalar keys %workerQs;

    # Work loop
    do {
        # Indicate that we are ready to do work
        printf("(%2d) Idle\n", $tid);

        # Wait for work from the queue
        $qinfo = $schedulerQ->dequeue(1);
		
		# Do some work while monitoring $TERM
        if (! $TERM && (defined $qinfo)) {
			($astep, $logfile, $numthreadsreq) = @$qinfo;
			my $threadstouse = $numthreads <= $numthreadsreq ? $numthreads : $numthreadsreq;
			my $tasks;
			my $taskType = $stephash->{$astep}->{taskType};
			$tasks = $astepref->{$astep}{Module}->getTasks($taskType);
			$numTasks = scalar @$tasks;
	        printf("(%2d $astep) launching %d tasks using %d threads\n",
				$tid, $numTasks, $threadstouse);
			my $t = 1;
			# simple allocation of tasks to threads
			while (@$tasks) {
				my $task = shift @$tasks;
				my $remTasks = scalar @$tasks;
				printf("(%2d $astep) Enqueueing $task ($remTasks out of $numTasks)\n", $t);
				my @taskinfo : shared = ($astep, $task);
				$workerQs{$t}->enqueue(\@taskinfo);
				printf("(%2d $astep) done Enqueueing $task\n", $t);
			#{
			#	lock($workerQs{1});
			#	my $item = $workerQs{1}->pending();
			#	if (defined $item) {
			#		printf("(%2d $astep) thread 1 has %d tasks\n", $tid, $item);
			#	}
			#	else {
			#		printf("(%2d $astep) thread 1 has 0 tasks\n", $tid);
			#	}
			#}
				$t++;
				if ($t > $threadstouse) { $t = 1; }
				#sleep 1;
				usleep(7500);
			}
			printf("(%2d $astep) done enqueue\n", $tid);
		
			print "\n";
			my $numTasksCompleted = 0;
			while ($numTasks > $numTasksCompleted) {
				# Expect $qinfo as 0:task string, 1:tid, [2]:log string, [3]:exit status
				$qinfo = $workerTaskDoneQ->dequeue(1);
				$numTasksCompleted++;
				#print "\r";
				printf("(%2d $astep) Job status: %d/%d\n", $tid, $numTasksCompleted, $numTasks);
				printf("(%2d $astep) Task: %s exit status %d\n", $qinfo->[1], $qinfo->[0], $qinfo->[3]);
				printf("(%2d $astep) Log:\n", $qinfo->[1]);
				printf("                 %s\n", $qinfo->[2]);
			}
			$schedulerStepDoneQ->enqueue($astep);
			printf("(%2d $astep) Completed\n", $tid);
		}

        # Loop back to idle state if not told to terminate
    } while (! $TERM && (defined $qinfo));

    # All done
    printf("(%2d) Scheduler exiting!\n", $tid);
}


=head1 AUTHOR

  © 2014 Steve Coward <steve-coward@comcast.net>
 *All rights reserved. No warranty, explicit or implicit, provided.
 In no event shall the author be liable for any claim or damages.
 
=cut
