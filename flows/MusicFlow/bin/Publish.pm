
=head1 NAME

=head1 SYNOPSIS

      use MusicFlow::bin::Publish;
      my $Publish = MusicFlow::bin::Publish->new();
      $Publish->RunStep;

=head1 DESCRIPTION

  This is an object-oriented library that moves music files from development
  directory to the music library directory.

=head2 Methods

=cut

package MusicFlow::bin::Publish;
use base MusicFlow::bin::Common;

use strict;
use warnings;
#use utf8;
use File::stat;
use File::Spec::Functions;
#use Tk::Dialog;
#use open qw{:encoding(utf8) :std};
use Encode qw(is_utf8 encode decode);
use MP3::Tag;
use MP3::Info;
use Win32API::File 0.08 qw( :ALL );
use Win32::FindFile qw(wchar);

our $VERSION = "2.00";
 
# The constructor of an object is called new() by convention.  Any
# method may construct an object and you can have as many as you like.
 
sub new {
	my($class, %args) = @_;
	my $self = bless({}, $class);
	return $self;
}
   
sub RunStep {
	my $self = shift;
	my $logFile = shift;
	my $trackinfo = shift;
	my @output;
	my @albumsprocessed;
	my $retval = 0;
	my $rval;
	my $trackpath;
	my $artist;
	my $album;
	my $track;
	my $cmd;
	my $libpath = $ENV{"Music_Lib"};
	my $publishFromITunes = 0;
	my $publishHiRes = 1;
	my $publishLoRes = 1;
	my $DIR;
	my $log;
	my $tid = threads->tid();
	
	$log .= "Publish\n";
	
	if ($ENV{"Music_Lib"} eq $ENV{"Music_Dir"}) {
		# nothing to publish?
		goto EXIT;
	}
			
	if (defined $trackinfo) {
		# trackinfo contains path, artists, album, track
		($trackpath, $artist, $album, $track) = split(/::/, $trackinfo);
		
		my $fullfile = catfile($trackpath, $artist, $album, $track);
		my $artistpath = catfile($trackpath, $artist);
		my $albumpath = catfile($trackpath, $artist, $album);
		my $newfile = catfile($libpath, $artist, $album, $track);
		my $logfile = catfile($libpath, $artist, $album, $track);
		my $jpgfile = catfile($trackpath, $artist, $album, "Folder.jpg");
		my $tagfile = catfile($trackpath, $artist, $album, "tags.txt");
		my $libjpgfile = catfile($libpath, $artist, $album, "Folder.jpg");
		my $cuefile = catfile($libpath, $artist, $album, $track);
		my $flaclibartistpath;
		my $flaclibalbumpath;
		my $hilibartistpath;
		my $hilibalbumpath;
		my $lolibartistpath;
		my $lolibalbumpath;
		
		my $noexttrack = encode( 'utf8', $track);
		$noexttrack =~ s/\.mp3$//;
		$noexttrack =~ s/\.flac$//;
		$noexttrack =~ s/\d\d\d_q\d$//;
		$noexttrack =~ s/\(/\\\(/g;
		$noexttrack =~ s/\)/\\\)/g;
		$noexttrack =~ s/\[/\\\[/g;
		$noexttrack =~ s/\]/\\\]/g;
		my $flacfile;
		my $hiresmp3file;
		my $hiresmp3path;
		my $loresmp3file;
		my $outflacfile;
		my $outhiresmp3file;
		my $outloresmp3file;
		my @files;
		
		my $talbum = $album;
		$talbum =~ s/\[/\\\[/g;
		$talbum =~ s/\]/\\\]/g;
		$talbum =~ s/\(/\\\(/g;
		$talbum =~ s/\)/\\\)/g;
		if (!grep(!/$talbum/, @albumsprocessed)) {
			push(@albumsprocessed, $talbum);
		}
		
		my $ap = $self->WindowizePath($albumpath);
		#$albumpath =~ s/\s/\\ /g;
		#open (DIR, "<:encoding(UTF-8)", "$albumpath") or die $!;
		opendir(DIR, "$albumpath") or die $!;
		@files = sort grep { /$noexttrack/ } readdir(DIR);
		closedir(DIR);
		# if @files == 3 have x.flac, x320_q0.mp3, x128_q0.mp3
		# if @files == 2 have x.mp3, x128_q0.mp3
		# if @files == 1 have x.mp3 (at lowest user bitrate)
		
		$hilibartistpath = catfile($libpath . " (hi res)", $artist);
		if (!-e $hilibartistpath) {
			mkdir($hilibartistpath);
		}
		$hilibalbumpath = catfile($libpath . " (hi res)", $artist, $album);
		if (!-e $hilibalbumpath) {
			mkdir($hilibalbumpath);
		}
		$lolibartistpath = catfile($libpath . " (lo res)", $artist);
		if (!-e $lolibartistpath) {
			mkdir($lolibartistpath);
		}
		$lolibalbumpath = catfile($libpath . " (lo res)", $artist, $album);
		if (!-e $lolibalbumpath) {
			mkdir($lolibalbumpath);
		}
		$flaclibartistpath = catfile($libpath, $artist);
		if (!-e $flaclibartistpath) {
			mkdir($flaclibartistpath);
		}
		$flaclibalbumpath = catfile($libpath, $artist, $album);
		if (!-e $flaclibalbumpath) {
			mkdir($flaclibalbumpath);
		}
		
		if ($#files == 2) {
			$flacfile = shift @files;
			my $dflacfile = decode( 'utf8', $flacfile );
			my $outflacfile = catfile($flaclibalbumpath, $dflacfile);
			my $flacpath = catfile($trackpath, $artist, $album, $dflacfile);
			if ($publishFromITunes == 0) {
				if (-e $outflacfile) {
					DeleteFileW(wchar($outflacfile));
				}
				if (MoveFile($flacpath, $outflacfile) == 0) {
					$log .= "ERROR: Move unsuccessful:\n\t$flacpath =>\n\t$outflacfile\n$!\n\t";
					$retval += 1;
					goto EXIT;
				}
			
				my $mp3file = $outflacfile;
				$mp3file =~ s/\.flac$/.mp3/;
				if (-e $mp3file) {
					DeleteFileW(wchar($mp3file));
				}
				#opendir(DIR, $flaclibalbumpath);
				#my @oldmp3files = sort grep { /.mp3/ } readdir(DIR);
				#closedir(DIR);
		
				#foreach my $file (@oldmp3files) {
				#	DeleteFileW(wchar(catfile($flaclibalbumpath, $file)));
				#}
			}
							
			$loresmp3file = shift @files;
			my $dloresmp3file = decode( 'utf8', $loresmp3file );
			$outloresmp3file = catfile($lolibalbumpath, $dloresmp3file);
			$outloresmp3file =~ s/\d\d\d_q\d//;
			$loresmp3file = catfile($trackpath, $artist, $album, $dloresmp3file);
			if ($publishLoRes == 1) {
				if (-e $outloresmp3file) {
					DeleteFileW(wchar($outloresmp3file));
				}
				if (MoveFile($loresmp3file, $outloresmp3file) == 0) {
					$log .= "ERROR: Move unsuccessful:\n\t$loresmp3file =>\n\t$outloresmp3file\n$!\n\t";
					$retval += 1;
					goto EXIT;
				}
			}

			$hiresmp3file = shift @files;
			my $dhiresmp3file = decode( 'utf8', $hiresmp3file );
			$outhiresmp3file = catfile($hilibalbumpath, $dhiresmp3file);
			$outhiresmp3file =~ s/\d\d\d_q\d//;
			$hiresmp3path = catfile($trackpath, $artist, $album, $dhiresmp3file);
			if ($publishHiRes == 1) {
				if (-e $outhiresmp3file) {
					DeleteFileW(wchar($outhiresmp3file));
				}
				if (MoveFile($hiresmp3path, $outhiresmp3file) == 0) {
					$log .= "ERROR: Move unsuccessful:\n\t$hiresmp3path =>\n\t$outhiresmp3file\n$!\n\t";
					$retval += 1;
					goto EXIT;
				}
			}
		}
		elsif ($#files == 1) {
			$hiresmp3file = shift @files;
			my $dhiresmp3file = decode( 'utf8', $hiresmp3file );
			$outhiresmp3file = catfile($libpath, $artist, $album, $dhiresmp3file);
			$hiresmp3path = catfile($trackpath, $artist, $album, $dhiresmp3file);
			if ($publishFromITunes == 0) {
				if (-e $outhiresmp3file) {
					DeleteFileW(wchar($outhiresmp3file));
				}
				CopyFile($hiresmp3path, $outhiresmp3file, 0);
			}
			
			$outhiresmp3file = catfile($libpath . " (hi res)", $artist, $album, $dhiresmp3file);
			if ($publishHiRes == 1) {
				if (-e $outhiresmp3file) {
					DeleteFileW(wchar($outhiresmp3file));
				}
				if ($publishFromITunes == 0) {
					if (MoveFile($hiresmp3path, $outhiresmp3file) == 0) {
						$log .= "ERROR: Move unsuccessful:\n\t$hiresmp3path =>\n\t$outhiresmp3file\n$!\n\t";
						$retval += 1;
						goto EXIT;
					}
				}
				else {
					CopyFile($hiresmp3path, $outhiresmp3file, 0);
				}
			}
			
			if ($publishLoRes == 1) {
				$loresmp3file = shift @files;
				my $dloresmp3file = decode( 'utf8', $loresmp3file );
				$outloresmp3file = catfile($libpath . " (lo res)", $artist, $album, $dloresmp3file);
				$outloresmp3file =~ s/\d\d\d_q\d//;
				$loresmp3file = catfile($trackpath, $artist, $album, $dloresmp3file);
				if (-e $outloresmp3file) {
					DeleteFileW(wchar($outloresmp3file));
				}
				if (MoveFileW(wchar($loresmp3file), wchar($outloresmp3file)) == 0) {
					$log .= "ERROR: Move unsuccessful:\n\t$loresmp3file =>\n\t$outloresmp3file\n$!\n\t";
					$retval += 1;
					goto EXIT;
				}
			}
		}
		elsif ($#files == 0) {
			$hiresmp3file = shift @files;
			my $dhiresmp3file = decode( 'utf8', $hiresmp3file );
			$hiresmp3path = catfile($trackpath, $artist, $album, $dhiresmp3file);
			
			if ($hiresmp3file =~ /\.mp3/) {
				my $mp3 = MP3::Tag->new($hiresmp3path);
				my $fileinfo;
				$fileinfo = get_mp3info($hiresmp3path);
				my $mp3bitrate;
				if (exists($fileinfo->{BITRATE})) {
					$mp3bitrate = $fileinfo->{BITRATE};
				}
				else {
					$mp3bitrate = 0;
				}
				$mp3->close();
			
				if ($mp3bitrate <= 128) {
					if ($publishFromITunes == 0) {
						$outhiresmp3file = catfile($libpath,   $artist, $album, $dhiresmp3file);
						if (-e $outhiresmp3file) {
							DeleteFileW(wchar($outhiresmp3file));
						}
						CopyFileW(wchar($hiresmp3path), wchar($outhiresmp3file), 0);
					}
					
					if ($publishHiRes == 1) {
						$outhiresmp3file = catfile($libpath . " (hi res)", $artist, $album, $dhiresmp3file);
						if (-e $outhiresmp3file) {
							DeleteFileW(wchar($outhiresmp3file));
						}
						CopyFileW(wchar($hiresmp3path), wchar($outhiresmp3file), 0);
					}
			
					if ($publishLoRes == 1) {
						$loresmp3file = $dhiresmp3file;
						$outloresmp3file = catfile($libpath . " (lo res)", $artist, $album, $loresmp3file);
						$outloresmp3file =~ s/\d\d\d_q\d//;
						$loresmp3file = catfile($trackpath, $artist, $album, $loresmp3file);
						if ($publishFromITunes == 0) {
							if (-e $outloresmp3file) {
								DeleteFileW(wchar($outloresmp3file));
							}
							if (MoveFileW(wchar($loresmp3file), wchar($outloresmp3file)) == 0) {
								$log .= "ERROR: Move unsuccessful:\n\t$loresmp3file =>\n\t$outloresmp3file\n$!\n\t";
								$retval += 1;
								goto EXIT;
							}
						}
						else {
							if (-e $outloresmp3file) {
								DeleteFileW(wchar($outloresmp3file));
							}
							CopyFileW(wchar($loresmp3file), wchar($outloresmp3file), 0);
						}
					}
				}
			}
		}
		
		
		#opendir(DIR, $artistpath);
		#@files = grep { /\.log$/ } readdir(DIR);
		#closedir(DIR);
		#foreach my $file (@files) {
		#	move($file, $libalbumpath);
		#}
		
		
		if ($publishFromITunes == 0) {
			opendir(DIR, $albumpath);
			@files = sort grep { /.jpg$|.cue$|.log$|.txt$|.text$|.html$/ } readdir(DIR);
			closedir(DIR);
		
			foreach my $file (@files) {
				my $f = $self->WindowizePath(catfile($albumpath, $file));
				my $g = $self->WindowizePath(catfile($flaclibalbumpath, $file));
				if (-e $g) {
					DeleteFileW(wchar($g));
				}
				if (MoveFile($f, $g) == 0) {
					$log .= "ERROR: Move unsuccessful:\n\t$f =>\n\t$g\n$!\n\t";
					$retval += 1;
					goto EXIT;
				}
			}
		}
	}

EXIT:
	$log .= "($tid Publish) exits with status $retval\n";
	my @retarray = ($tid, $log, $retval >> 8);
	
	return(\@retarray);
}

sub GetLibFile
{
	my $file = shift;
	my $libfile;
	my ($trackpath, $artist, $album, $track) = split(/::/, $file);
	$libfile = catfile($ENV{"Music_Lib"}, $artist, $album, $track);
	$libfile =~ s/\d\d\d_q\d//;
	
	return($libfile);
}
sub GetOptions {
	my %opthash;
	
	#$opthash{"opt_minsize"} = $opt_minsize;
	
	return(\%opthash);
}
sub SetOptions {
	my $self = shift;
	my $optref = shift;
	my %opthash = %$optref;
	
	#$opt_minsize = $opthash{"opt_minsize"};
}
sub QueryOptions {
	my $self = shift;
	print $MainWindow::fhLogFile "Set some options in Publish.\n";
}
  
# return >0 if this step needs to run
# return 0	if this step does not need to run
# return <0 if this step can not run
sub CheckDependencies {
	my $self = shift;
	my $lastrun = shift;
	my $sbi;
	my $sbo;

	if (!defined $ENV{"Selection"}) {
		#print $MainWindow::fhLogFile "The environment variable \"Bin_Dir\" must be defined.\n";
		#print $MainWindow::fhLogFile "Exiting ...\n";
		exit(-1);
	}
	
	my $selectFile = $ENV{"Selection"};
	
	if (!-e $selectFile) {
		#print $MainWindow::fhLogFile "Selection file $selectFile does not exist.\n";
		return(-1);
	}
	
	$sbi = stat($selectFile);
	my $in_time = $sbi->mtime;
	
	if ($in_time >= $lastrun) {
		# run the step
		#print $MainWindow::fhLogFile "Select file has changed since this step's last execution.\n";
		return(1);
	}

	# skip the step
	return(0);
}

=head1 AUTHOR

  � 2014 Steve Coward <steve-coward@comcast.net>
 *All rights reserved. No warranty, explicit or implicit, provided.
 In no event shall the author be liable for any claim or damages.

=cut

1;
