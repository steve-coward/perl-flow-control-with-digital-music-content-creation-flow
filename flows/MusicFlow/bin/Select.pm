
=head1 NAME

=head1 SYNOPSIS

      use MusicFlow::bin::Select;
      my $Select = MusicFlow::bin::Select->new();
      $Select->RunStep;

=head1 DESCRIPTION

  This is an object-oriented library that prompts the user to select a
  set of directories containing music files to be processed.

  MusicFlow is a ten step process comprised of the following steps:

=over

=item *

  Music selection: User selects set of music files to process.

=item *

  Music file validation: Integrity of selected music files is verified.

=item *

  HDCD decode: HDCD encoded files are decoded to 24 bit FLAC format.

=item *

  FLAC encode: Selected wav files are decoded to FLAC format.

=item *

  MP3 decode (high resolution): FLAC files are decoded to MP3 format.

=item *

  MP3 decode (low resolution)): FLAC files are decoded to low quality MP3 format.

=item *

  Album artwork acquisition: User interactively selects album artwork for the Web.

=item *

  Music file tagging: Music tags are applied to each music file.

=item *

  Quality check: Various quality checks are applied.

=item *

  Publish: Music files are published to the music library.

=back

  The components comprising MusicFlow include flac.exe (FLAC encode),
  lame.exe (MP3 encode), albumArt.exe (artwork acquisition),
  metaflac.exe (artwork insertion), hdcd.exe (HDCD decode)
  and tag.exe (tag insertion).

=head2 Methods

=cut

package MusicFlow::bin::Select;
use base MusicFlow::bin::Common;

use strict;
use warnings;
#use Tk::FileSelect 'as default';
use File::Spec::Functions;
use File::Spec;
our $VERSION = "2.00";
use File::Find;
use utf8;
use Encode qw(is_utf8 encode decode);
use Win32::Unicode;
#use open qw{:encoding(utf8) :std};

# Inherit from the "Exporter" module which handles exporting functions.
# Most procedural modules make use of this.
use base 'Exporter';

my $selectFile;
my $hLogFile;
my $log;
my $opt_promptrootdir = 0;
my $opt_rootdir = $ENV{"Music_Dir"};
my @selectedDirs;
my @selectedTracks;

=head3 new
 
	my $select = MusicFlow::bin::Select->new();
 
Instantiates an object which holds a greeting message.  If a C<$target> is
given it is passed to C<< $hello->target >>.
The constructor of an object is called new() by convention.
Any method may construct an object and you can have as many as you like.

Arguments: $class

Return: $self

=cut

sub new {
	my($class, %args) = @_;
	my $self = bless({}, $class);
	return $self;
}


sub PrintTracks
{
	my $self = shift;
	my $tracks = shift;
	print STDOUT "\n\n";
	foreach my $track (@$tracks) {
		print STDOUT $track . "\n";
	}
	print STDOUT "\n\n";
}

sub setPath {
	my $self = shift;
	my $path = shift;
	$opt_rootdir = $path;
	return;
}

=head3 RunStep
 
	$select->RunStep;
 
Perform the step functionality.
 
Arguments: none

Return: error status (0 for success, <0 for warnings, >0 errors)

=cut

sub RunStep {
	my $self = shift;
	my $logFile = shift;
	my $dir;

	open (my $hLogFile, ">:encoding(UTF-8)", $logFile);
	print $hLogFile "Starting Select.\n";
	
	@selectedDirs = ();
			
	if ($opt_promptrootdir) {
		my($ok) = ('OK');
		my($cancel) = ('Cancel');
		my $DIALOG;
		my $prevval = $opt_rootdir;

		$DIALOG = $flow::MainWindow->Dialog(
		    -title          => "Set Music Root Directory",
		    -default_button => $ok,
		    -buttons        => [$ok, $cancel],
		);

		my $sb;
		my $radiovalue;
		$sb = $DIALOG->Radiobutton(
			-variable => \$radiovalue,
			-text => 'J:/My Music/Flac Archive',
			-value => 'J:/My Music/Flac Archive',
			-command => [ \&setPath, $radiovalue ],
			-relief => 'flat',
		)->pack(-side => 'left');
		
		$sb = $DIALOG->Radiobutton(
			-variable => \$radiovalue,
			-text => 'K:/My Music/iTunes',
			-value => 'K:/My Music/iTunes',
			-command => [ \&setPath, $radiovalue ],
			-relief => 'flat',
		)->pack(-side => 'left');
    
		my $button = $DIALOG->Show;
	
		if ($button eq $cancel) {
			$opt_rootdir = $prevval;
		}
		else {
			$opt_rootdir = $radiovalue;
		}
	}
		
	if (!defined $opt_rootdir) {
		print $hLogFile
			"The environment variable \"Music_Dir\" must be defined.\n";
		print $hLogFile "Exiting ...\n";
		return(-1);
	}
	elsif (!-e $opt_rootdir) {
		print $hLogFile
			"The music root directory $opt_rootdir does not exist.\n";
		print $hLogFile "Exiting ...\n";
		return(-1);
	}
	else {
		$ENV{"Music_Dir"} = $opt_rootdir;
	}
		
	my $music_dir = $ENV{Music_Dir};
	do {
		$dir = $flow::MainWindow->chooseDirectory(
			-initialdir => $music_dir,
			-title => "Select Music Files");
		if (defined $dir) {
			#my $dir8 = encode('UTF-8', $dir);
			#print $hLogFile "You selected $dir8\n";
			print  "You selected $dir\n";
			push(@selectedDirs, "$dir");
		}
	} while (defined $dir);
			
	print "RunStep Selected Dirs\n:";
	print @selectedDirs;

	open (my $outfh, '>:encoding(UTF-8)', $ENV{"Selection"});
	foreach  my $dir (@selectedDirs) {
		print $outfh "$dir\n";
	}
	close $outfh;

	return(0);
}

## rewrite music files for selected directories
#sub writeMusicList
#{
#	my $selectedDirs = shift;
#	my $dir;
#	my $music_dir = $ENV{Music_Dir};
#	my @tracks;
#	my $outfh;
#	
#	#$DB::single = 1;
#	if (scalar @$selectedDirs > 0) {
#		#print $MainWindow::fhLogFile "Final Selection: @selections\n";
#		#print STDOUT "Using selection file: $ENV{'Selection'}\n";
#		open ($outfh, '>:encoding(UTF-8)', $ENV{"Selection"});
#		findW(makeExtClosure($outfh,
#		sub {
#			#print "$_ $Win32::Unicode::Dir::name\n";
#			if (/.(wav|flac|mp3)$/) {
#				my ($volume, $directories, $file) =
#					File::Spec->splitpath( $Win32::Unicode::Dir::name, 0 );
#				my @dirs = File::Spec->splitdir( $directories );
#				pop @dirs; # toss empty element (due to trailing /)
#				my $album = pop @dirs;
#				my $artist = pop @dirs;
#				my $dir = File::Spec->catdir( @dirs );
#				my $selection =
#					$volume . $dir . "::" . $artist . "::" . $album . "::" . $_;
#				#my $selection =
#				#	$volume . $dir . "::" . $artist . "::" . $album . "::" . $_;
#				print $outfh $selection . "\n";
#				push(@tracks, $selection);
#				#print $selection . "\n";
#			}
#		}), @$selectedDirs);
#		close $outfh;
#	}
#		
#	return();
#}
sub wantedaction
{
	my $self = shift;	
}
sub wanted
{
	my $self = shift;
	#my $fh = shift;
	my $args = shift;
	
	if (/.(wav|flac|mp3)$/) {
		my ($volume, $directories, $file) =
			File::Spec->splitpath( $args->{path}, 0 );
		my @dirs = File::Spec->splitdir( $directories );
		pop @dirs; # toss empty element (due to trailing /)
		my $album = pop @dirs;
		my $artist = pop @dirs;
		my $dir = File::Spec->catdir( @dirs );
		my $selection =
			encode('UTF-8', $volume . $dir . "::" . $artist . \
				   "::" . $album . "::" . $args->{file});
		#print $fh $selection . "\n";
		#print $selection . "\n";
	}
	
}

=head3 CheckDependencies

	my $bRunStep = $select->CheckDependencies($logtime);

Compute module specific dependency check and return result.
Module timestamps are checked against the input timestamp.

Arguments: a timestamp value of type File::stat::mtime

Return: return 0 if this step does not need to run else return 1

=cut

sub CheckDependencies {
	my $self = shift;
	my $lastrun = shift;
	
	# run the step
	return(1);
}

=head3 GetOptions

	$select->GetOptions();

Get module options.

Arguments: none

Return: reference to hash of option name/value pairs

=cut

sub GetOptions {
	my $self = shift;
	my %opthash;
	
	#$opthash{"opt_minsize"} = $opt_minsize;
	
	return(\%opthash);
}

=head3 SetOptions

	$select->SetOptions($optionsRef);

Set module options according to contents of hash argument.

Arguments: hash of option name/value pairs

Return: none

=cut

sub SetOptions {
	my $self = shift;
	my $optref = shift;
	my %opthash = %$optref;
	
	#$opt_minsize = $opthash{"opt_minsize"};
}

=head3 QueryOptions

	$select->QueryOptions;

Display a Tk dialog containing widgets allowing user to change all
module options.

Arguments: none

Return: none

=cut

sub QueryOptions {
	my $self = shift;
	my($ok) = ('OK');
	my($cancel) = ('Cancel');
    my $DIALOG;
	my $prevval = $opt_promptrootdir;

	$DIALOG = $flow::MainWindow->Dialog(
	    -title          => 'Set Prompt For Music Root Directory',
        -default_button => $ok,
        -buttons        => [$ok, $cancel],
    );

    my $sb = $DIALOG->Checkbutton(
		-variable => \$opt_promptrootdir,
		-relief => 'flat',
    )->pack(-side => 'left');
	$DIALOG->Label(
		-text => 'Select to set music root directory.'
	)->pack(-side => 'left');
    
    my $button = $DIALOG->Show;
	
	if ($button eq $cancel) {
		$opt_promptrootdir = $prevval;
	}
}

=head1 AUTHOR

  � 2014 Steve Coward <steve-coward@comcast.net>
 *All rights reserved. No warranty, explicit or implicit, provided.
 In no event shall the author be liable for any claim or damages.

=cut

1;
