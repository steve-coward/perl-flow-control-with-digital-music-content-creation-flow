
=head1 NAME

=head1 SYNOPSIS

      use MusicFlow::bin::Validate;
      my $Validate = MusicFlow::bin::Validate->new();
      $Validate->RunStep;

=head1 DESCRIPTION

  This is an object-oriented library that does a temporary decode of flac files
  to check validity. Also performs basic file renaming and file capitalization
  to ensure consistency across library.
  
  Issue: this step should be moved if EAC generates .wav files.

=head2 Methods

=cut

package MusicFlow::bin::Validate;
use base MusicFlow::bin::Common;

use strict;
use warnings;
use utf8;
use File::stat;
use File::Spec::Functions;
use Audio::FLAC::Header;
#use open qw{:encoding(utf8) :std};
use Encode qw(is_utf8 encode decode);
use Win32API::File 0.08 qw( :ALL );
use Win32::FindFile qw(wchar);

our $VERSION = "2.00";

my $opt_noAction = 1;

# The constructor of an object is called new() by convention.  Any
# method may construct an object and you can have as many as you like.
sub new {
	my($class, %args) = @_;
	my $self = bless({}, $class);
	$self->{Log} = "Step Validate created\n";
	return $self;
}

sub RunStep {
	my $self = shift;
	my $logFile = shift;
	my $trackinfo = shift;
	my $retval = 0;
	my $rval;
	my $trackpath;
	my $artist;
	my $album;
	my $track;
	my $tracknum;
	my $cmd;
	my $flacpath = $ENV{"Flac_Exe"};
	my $tagpath = $ENV{"Tag_Exe"};
	my $tid = threads->tid();
	
	$DB::single = 1;
	$self->{Log} .= "INFO: Start file validation\n";
	print "INFO: validating:$trackinfo\n";
	if (defined $trackinfo) {
		# trackinfo contains path, artists, album, track
		($trackpath, $artist, $album, $track) = split(/::/, $trackinfo);
		my $musicfile = catfile($trackpath, $artist, $album, $track);
		my $newmusicfile = $musicfile;
		my $newtrack = $track;
	
		if ($track =~ m/\.flac$/i) {
			$self->{Log} .= "INFO: validating:\n\t$musicfile\n";
			my $wf = $self->WindowizePath($musicfile);
			$cmd = "\"$flacpath\" -t -s -w \"$wf\"";
			# to re encode in attempt to fix flac errors
			#$cmd = "\"$flacpath\"  -f -F \"$wf\"";
			$self->{Log} .= `$cmd 2>&1`;
			$rval = $?;
			if ($rval ne 0) {
				$self->{Log} .= "ERROR: $musicfile failed flac test (exit code: $rval)\n";
				$retval += $rval;
			}
		}
		
		if ($track =~ m/.wav$/i) {
			# no tag info for wav files
			$newtrack = $self->parseTrackNum($track);
		}
		elsif ($track =~ m/.mp3$/i) {
			my $title;
			my $tracknum;
			my $discnum;
			my $mp3 = MP3::Tag->new($musicfile);
			if (defined $mp3) {
				$mp3->get_tags();
				my $id3v2 = $mp3->{ID3v2};
				$mp3->close();
				
				if (defined $id3v2) {
					my $tag_long;
					($title, $tag_long) = $id3v2->get_frame('TIT2');
					($discnum, $tag_long) = $id3v2->get_frame('TPOS');
					($tracknum, $tag_long) = $id3v2->get_frame('TRCK');
					$tracknum =~ s/(\d+)\/\d*/$1/;
					$newtrack = $discnum . "-" . $tracknum . " " . $title . ".mp3";
				}
				else {
					$newtrack = $self->parseTrackNum($track);
				}
			}
			else {
				$newtrack = $self->parseTrackNum($track);
			}
		}
		elsif ($track =~ m/.flac$/i) {
			my $flac = Audio::FLAC::Header->new("$musicfile");
			my $info = $flac->info();
			my $tags = $flac->tags();

			my $title = $tags->{TITLE};
			my $tracknum = $tags->{Tracknumber};
			if (!defined $tracknum) {
				$tracknum = $tags->{TRACKNUMBER};
			}
			if ((defined $title) && (defined $tracknum)) {
				if (($tracknum < 10) && ($tracknum !~ /^0/)) {
					$tracknum = "0" . $tracknum;
				}
				$title =~ s/([\w']+)/\u\L$1/g;
				$newtrack = "1-" . $tracknum . " " . $title . ".flac";
			}
			else {
				$newtrack = $self->parseTrackNum($track);
			}
		}
		
		$newmusicfile = $self->Capitalize(catfile($trackpath, $artist, $album, $newtrack));
		$self->{Log} .= "INFO: Renaming:\n\t$musicfile=>\n\t$newmusicfile\n";
		if (!-e $newmusicfile) {
			if (!$opt_noAction) {
				if (MoveFileW(wchar($musicfile), wchar($newmusicfile)) == 1) {
					# success
				}
				else {
					$self->{Log} .= "ERROR: Move unsuccessful:\n\t$musicfile =>\n\t$newmusicfile\n$!\n\t";
					$retval += 1;
				}
			}
		}
		else {
			$self->{Log} .= "INFO: File already exists - not renaming:\n\t$musicfile =>\n\t$newmusicfile\n";
		}
	}
	
	$self->{Log} .= "($tid Validate) exits with status $retval\n";
	my @retarray = ($tid, $self->{Log}, $opt_noAction || ($retval >> 8));
	
	return(\@retarray);
}

# return >0 if this step needs to run
# return 0  if this step does not need to run
# return <0 if this step can not run
sub CheckDependencies {
	my $self = shift;
	my $lastrun = shift;
	my $sbi;
	my $sbo;

	return(1);
	if (!defined $ENV{"Selection"}) {
		#print $MainWindow::fhLogFile "The environment variable \"Bin_Dir\" must be defined.\n";
		#print $MainWindow::fhLogFile "Exiting ...\n";
		exit(-1);
	}
	
	my $selectFile = $ENV{"Selection"};
	
	if (!-e $selectFile) {
		#print $MainWindow::fhLogFile "ERROR: Selection file $selectFile does not exist.\n";
		return(-1);
	}
	
	$sbi = stat($selectFile);
	my $in_time = $sbi->mtime;
	
	if ($in_time >= $lastrun) {
		# run the step
		return(1);
	}

	# skip the step
	return(0);
}

sub GetOptions {
	my $self = shift;
	my %opthash;
	
	$opthash{"opt_noAction"} = $opt_noAction;
	
	return(\%opthash);
}
sub SetOptions {
	my $self = shift;
	my $optref = shift;
	my %opthash = %$optref;
	
	$opt_noAction = $opthash{"opt_noAction"};
}
sub QueryOptions {
	my $self = shift;
	my($ok) = ('OK');
	my($cancel) = ('Cancel');
    my $DIALOG;
	my $prevval = $opt_noAction;

	$DIALOG = $flow::MainWindow->Dialog(
	    -title          => 'Set Validate Options',
        -default_button => $ok,
        -buttons        => [$ok, $cancel],
    );

    my $sb = $DIALOG->Checkbutton(
		-variable => \$opt_noAction,
		-relief => 'flat',
    )->pack(-side => 'left');
	$DIALOG->Label(-text => 'Select to disable file renaming.')->pack(-side => 'left');
    
    my $button = $DIALOG->Show;
	
	if ($button eq $cancel) {
		$opt_noAction = $prevval;
	}
}

=head1 AUTHOR

  � 2014 Steve Coward <steve-coward@comcast.net>
 *All rights reserved. No warranty, explicit or implicit, provided.
 In no event shall the author be liable for any claim or damages.

=cut

1;
