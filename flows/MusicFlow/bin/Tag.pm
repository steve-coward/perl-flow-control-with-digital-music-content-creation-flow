
=head1 NAME

=head1 SYNOPSIS

      use MusicFlow::bin::Tag;
      my $Tag = MusicFlow::bin::Tag->new();
      $Tag->RunStep;

=head1 DESCRIPTION

  This is an object-oriented library that reads a tags.txt file
  in the album directory. If the file does not exist, functionality is TBD.
  The tags defined in the file are processed and applied to individual music
  tracks.  Artwork is added to the music files if a Folder.jpg exists in the
  album directory.  All pre-existing tags/artwork are deleted.

=head2 Methods

=cut

package MusicFlow::bin::Tag;
use base MusicFlow::bin::Common;

use strict;
use warnings;
use utf8;
use File::stat;
use File::Spec::Functions;
#use Tk::Dialog;
use MP3::Tag;
use MP3::Info;
#use LWP::Simple;
use LWP;
use HTML::Parser;
require HTML::TokeParser::Simple;
use Encode qw(is_utf8 encode decode);

our $VERSION = "2.00";

# The constructor of an object is called new() by convention.	Any
# method may construct an object and you can have as many as you like.
sub new {
	my($class, %args) = @_;
	my $self = bless({}, $class);
	return $self;
}


sub RunStep {
	my $self = shift;
	my $logFile = shift;
	my $trackinfo = shift;
	my $artists; # everything stored here as $artists->{$artist}{$album}
	my $albumPath = "";
	my $log;
	my $tid = threads->tid();
	my $retval = 0;
		
	$log .= "Start file tagging\n\n";
		
	if (defined $trackinfo) {
		# trackinfo contains path, artists, album, track
		my ($trackpath, $artist, $album, $track) = split(/::/, $trackinfo);
				
		my $EACLogDir = catfile($trackpath, $artist, $album);
		#my $shortmusicfile = Win32::GetShortPathName($EACLogDir);
		my $ripresults = $self->readLogs($EACLogDir);
		
		my $tagfile = catfile($trackpath, $artist, $album, "tags.text");
		$artists = $self->readTags($tagfile, $ripresults);
		
		$albumPath = catfile($trackpath, $artist, $album);
		$self->checkFileExistence($artists->{$artist}{$album}{tracks}, $albumPath);
		
		my $musicfile = catfile($albumPath, $track);
		$log .= "Tagging $musicfile\n";
		my $artfile = catfile($albumPath, "Folder.jpg");
		$track =~ s/\.flac//;
		$track =~ s/\.mp3//;
		$artist = $self->WinToName($artist);
		$album = $self->WinToName($album);
		$track = $self->WinToName($track);
		
		#my $xartist = encode('utf8', $artist);
		#my $xalbum = encode('utf8', $album);
		#my $xtrack = encode('utf8', $track);
		#$albumtrack = decode('utf8', $albumtrack);
		my $truetrack = $track;
		$truetrack =~ s/128\/q0//;
		$truetrack =~ s/320\/q0//;
		my $hrTrack = $artists->{$artist}{$album}{tracks}{$truetrack};
		if (defined $hrTrack->{title}) {
			my $t = $hrTrack->{title};
			$t =~ s/^\d+-\d\d //;
			$hrTrack->{title} = $t;
		}
		$self->tagFile($musicfile, $artfile, $hrTrack);
	}

	$log .= "($tid Tag) exits with status $retval\n";
	my @retarray = ($tid, $log, $retval >> 8);
	
	return(\@retarray);
}

sub GetOptions {
	my $self = shift;
	my %opthash;
	
	#$opthash{"opt_minsize"} = $opt_minsize;
	
	return(\%opthash);
}
sub SetOptions {
	my $self = shift;
	my $optref = shift;
	my %opthash = %$optref;
	
	#$opt_minsize = $opthash{"opt_minsize"};
}
sub QueryOptions {
	my $self = shift;
}
	
# return >0 if this step needs to run
# return 0	if this step does not need to run
# return <0 if this step can not run
sub CheckDependencies {
	my $self = shift;
	my $lastrun = shift;
	my $sbi;
	my $sbo;

	if (!defined $ENV{"Selection"}) {
		#print $MainWindow::fhLogFile "The environment variable \"Bin_Dir\" must be defined.\n";
		#print $MainWindow::fhLogFile "Exiting ...\n";
		exit(-1);
	}
	
	my $selectFile = $ENV{"Selection"};
	
	if (!-e $selectFile) {
		#print $MainWindow::fhLogFile "Selection file $selectFile does not exist.\n";
		return(-1);
	}
	
	# always run  the step
	return(1);
	
	$sbi = stat($selectFile);
	my $in_time = $sbi->mtime;
	
	if ($in_time >= $lastrun) {
		# run the step
		#print $MainWindow::fhLogFile "Select file has changed since this step's last execution.\n";
		return(1);
	}

	# skip the step
	return(0);
}
	
sub printAll
{
	my $self = shift;
	my $filename = shift;
	my $tag_info = '';
	my $tag_long = '';
	my %tag_hash = ();

	my $mp3 = MP3::Tag->new($filename);
	$mp3->get_tags();
	my $id3v2 = $mp3->{ID3v2};
	$mp3->close();

	print_simpleTag('IPLS', $id3v2); # IPLS : Involved people list
	print_simpleTag('MCDI', $id3v2); # MCDI : Music CD identifier
	print_simpleTag('PCNT', $id3v2); # PCNT : Play counter
	print_simpleTag('TALB', $id3v2); # TALB : Album/Movie/Show title
	print_simpleTag('TBPM', $id3v2); # TBPM : BPM (beats per minute)
	print_simpleTag('TCOM', $id3v2); # TCOM : Composer
	print_simpleTag('TCON', $id3v2); # TCON : Content type
	print_simpleTag('TCOP', $id3v2); # TCOP : Copyright message
	print_simpleTag('TDAT', $id3v2); # TDAT : Date
	print_simpleTag('TDLY', $id3v2); # TDLY : Playlist delay
	print_simpleTag('TENC', $id3v2); # TENC : Encoded by
	print_simpleTag('TEXT', $id3v2); # TEXT : Lyricist/Text writer
	print_simpleTag('TFLT', $id3v2); # TFLT : File type
	print_simpleTag('TIME', $id3v2); # TIME : Time
	print_simpleTag('TIPL', $id3v2); # TIPL : Involved people list
	print_simpleTag('TIT1', $id3v2); # TIT1 : Content group description
	print_simpleTag('TIT2', $id3v2); # TIT2 : Title/songname/content description
	print_simpleTag('TIT3', $id3v2); # TIT3 : Subtitle/Description refinement
	print_simpleTag('TKEY', $id3v2); # TKEY : Initial key
	print_simpleTag('TLAN', $id3v2); # TLAN : Language(s)
	print_simpleTag('TLEN', $id3v2); # TLEN : Length
	print_simpleTag('TMCL', $id3v2); # TMCL : Musician credits list
	print_simpleTag('TMED', $id3v2); # TMED : Media type
	print_simpleTag('TOAL', $id3v2); # TOAL : Original album/movie/show title
	print_simpleTag('TOFN', $id3v2); # TOFN : Original filename
	print_simpleTag('TOLY', $id3v2); # TOLY : Original lyricist(s)/text writer(s)
	print_simpleTag('TOPE', $id3v2); # TOPE : Original artist(s)/performer(s)
	print_simpleTag('TORY', $id3v2); # TORY : Original release year
	print_simpleTag('TOWN', $id3v2); # TOWN : File owner/licensee
	print_simpleTag('TPE1', $id3v2); # TPE1 : Lead performer(s)/Soloist(s)
	print_simpleTag('TPE2', $id3v2); # TPE2 : Band/orchestra/accompaniment
	print_simpleTag('TPE3', $id3v2); # TPE3 : Conductor/performer refinement
	print_simpleTag('TPE4', $id3v2); # TPE4 : Interpreted, remixed, or otherwise modified by
	print_simpleTag('TPOS', $id3v2); # TPOS : Part of a set
	print_simpleTag('TPUB', $id3v2); # TPUB : Publisher
	print_simpleTag('TRCK', $id3v2); # TRCK : Track number/Position in set
	print_simpleTag('TRDA', $id3v2); # TRDA : Recording dates
	print_simpleTag('TRSN', $id3v2); # TRSN : Internet radio station name
	print_simpleTag('TRSO', $id3v2); # TRSO : Internet radio station owner
	print_simpleTag('TSIZ', $id3v2); # TSIZ : Size
	print_simpleTag('TSRC', $id3v2); # TSRC : ISRC (international standard recording code)
	print_simpleTag('TSSE', $id3v2); # TSSE : Software/Hardware and settings used for encoding
	print_simpleTag('TYER', $id3v2); # TYER : Year
	print_simpleTag('WCOM', $id3v2); # WCOM : Commercial information
	print_simpleTag('WCOP', $id3v2); # WCOP : Copyright/Legal information
	print_simpleTag('WOAF', $id3v2); # WOAF : Official audio file webpage
	print_simpleTag('WOAR', $id3v2); # WOAR : Official artist/performer webpage
	print_simpleTag('WOAS', $id3v2); # WOAS : Official audio source webpage
	print_simpleTag('WORS', $id3v2); # WORS : Official internet radio station homepage
	print_simpleTag('WPAY', $id3v2); # WPAY : Payment
	print_simpleTag('WPUB', $id3v2); # WPUB : Publishers official webpage 

	print_complexTag('AENC', $id3v2); # AENC : Audio encryption
	print_complexTag('APIC', $id3v2); # APIC : Attached picture
	print_complexTag('COMM', $id3v2); # COMM : Comments
	print_complexTag('COMR', $id3v2); # COMR : Commercial frame
	print_complexTag('ENCR', $id3v2); # ENCR : Encryption method registration
	print_complexTag('GEOB', $id3v2); # GEOB : General encapsulated object
	print_complexTag('GRID', $id3v2); # GRID : Group identification registration
	print_complexTag('LINK', $id3v2); # LINK : Linked information
	print_complexTag('OWNE', $id3v2); # OWNE : Ownership frame
	print_complexTag('POPM', $id3v2); # POPM : Popularimeter
	print_complexTag('PRIV', $id3v2); # PRIV : Private frame
	print_complexTag('RBUF', $id3v2); # RBUF : Recommended buffer size
	print_complexTag('RVRB', $id3v2); # RVRB : Reverb
	print_complexTag('SYTC', $id3v2); # SYTC : Synchronized tempo codes
	print_complexTag('TXXX', $id3v2); # TXXX : User defined text information frame
	print_complexTag('TXXX01', $id3v2);
	print_complexTag('TXXX02', $id3v2);
	print_complexTag('TXXX03', $id3v2);
	print_complexTag('TXXX04', $id3v2);
	print_complexTag('UFID', $id3v2); # UFID : Unique file identifier
	print_complexTag('USER', $id3v2); # USER : Terms of use
	print_complexTag('USLT', $id3v2); # USLT : Unsychronized lyric/text transcription
	print_complexTag('WXXX', $id3v2); # WXXX : User defined URL link frame

	# Following frames are only supported in raw mode:
	# CRM : Encrypted meta frame
	# EQUA : Equalization
	# ETCO : Event timing codes
	# LNK : Linked information
	# MLLT : MPEG location lookup table
	# PIC : Attached picture
	# POSS : Position synchronisation frame
	# RVAD : Relative volume adjustment
	# SYLT : Synchronized lyric/text 
}



sub print_simpleTag($)
{
	my $self = shift;
	my $tag = shift;
	my $id3v2 = shift;

	my ($tag_info, $tag_long) = $id3v2->get_frame($tag);
	if ($tag_long) {
		print STDOUT "$tag: $tag_long = $tag_info\n";
	}
} 



sub print_complexTag($)
{
	my $self = shift;
	my $tag = shift;
	my $id3v2 = shift;
	my $key = '';
	my $value = '';

	my ($tag_info, $tag_long) = $id3v2->get_frame($tag);
	if ($tag_long) {
		my %tag_hash = %$tag_info;
		print STDOUT "$tag: $tag_long\n"; 
		while (($key, $value) = each %tag_hash) {
			if (($key eq '_Data') || ($key eq '_Logo')) {
				print STDOUT " $key = not listed (possibly binary data)\n";
			} 
			else {
				print STDOUT " $key = $value\n";
			} 
		}
	}
}

=head1 AUTHOR

  � 2014 Steve Coward <steve-coward@comcast.net>
 *All rights reserved. No warranty, explicit or implicit, provided.
 In no event shall the author be liable for any claim or damages.

=cut

1;
