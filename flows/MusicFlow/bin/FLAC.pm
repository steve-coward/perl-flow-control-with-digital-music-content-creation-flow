
=head1 NAME

=head1 SYNOPSIS

      use MusicFlow::bin::FLAC;
      my $flac = MusicFlow::bin::FLAC->new();
      $flac->RunStep;

=head1 DESCRIPTION

  This is an object-oriented library that performs
  .wav to .flac file conversion.

=head2 Methods

=cut

package MusicFlow::bin::FLAC;
use base MusicFlow::bin::Common;

use strict;
use warnings;
use File::stat;
use File::Spec::Functions;
use Win32API::File 0.08 qw( :ALL );
use Win32::FindFile qw(wchar);

our $VERSION = "2.00";

# Inherit from the "Exporter" module which handles exporting functions.
# Most procedural modules make use of this.
use base 'Exporter';

# When the module is invoked, export, by default, the function "hello" into 
# the namespace of the using code.
#our @EXPORT = qw(Wave2FLAC);

my $opt_verifyencode = 0;

# The constructor of an object is called new() by convention.  Any
# method may construct an object and you can have as many as you like.
sub new {
	my($class, %args) = @_;
	my $self = bless({}, $class);
	return $self;
}

sub RunStep {
	my $self = shift;
	my $logFile = shift;
	my $trackinfo = shift;
	my @output;
	my $retval = 0;
	my $rval;
	my $trackpath;
	my $artist;
	my $album;
	my $track;
	my $cmd;
	my $flacpath = $ENV{"Flac_Exe"};
	my $log;
	my $tid = threads->tid();
	
	$log .= "Start FLAC conversion\n";
	
	if (defined $trackinfo) {
		# trackinfo contains path, artists, album, track
		($trackpath, $artist, $album, $track) = split(/::/, $trackinfo);
		my $wavefile = catfile($trackpath, $artist, $album, $track);
		$wavefile = $self->WindowizePath($wavefile);
	
		if ($track =~ m/\.wav$/i) {
			$log .= "converting wave to flac $trackpath $artist $album $track\n";
			if ($opt_verifyencode) {
				$rval = $self->runSystemCommand($flacpath, $wavefile, "", 0, "-f", "-s", "--verify");
			}
			else {
				$rval = $self->runSystemCommand($flacpath, $wavefile, "", 0, "-f", "-s");
			}
			
			if ($rval ne 0) {
				$log .= "ERROR: $wavefile conversion to flac failed (exit code: $rval)\n";
				$retval += $rval;
				my $flacfile = $wavefile;
				$flacfile =~ s/.wav$/.flac/;
				DeleteFile($flacfile);
			}
			else {
				DeleteFile($wavefile);
			}
		}
	}

	$log .= "($tid FLAC) exits with status $retval\n";
	my @retarray = ($tid, $log, $retval >> 8);
	
	return(\@retarray);
}

# return >0 if this step needs to run
# return 0  if this step does not need to run
# return <0 if this step can not run
sub CheckDependencies {
	my $self = shift;
	my $lastrun = shift;
	my $sbi;
	my $sbo;

	if (!defined $ENV{"Selection"}) {
		#print $MainWindow::fhLogFile "The environment variable \"Bin_Dir\" must be defined.\n";
		#print $MainWindow::fhLogFile "Exiting ...\n";
		exit(-1);
	}
	
	my $selectFile = $ENV{"Selection"};
	
	if (!-e $selectFile) {
		#print $MainWindow::fhLogFile "Selection file $selectFile does not exist.\n";
		return(-1);
	}
	
	$sbi = stat($selectFile);
	my $in_time = $sbi->mtime;
	
	if ($in_time >= $lastrun) {
		# run the step
		#print $MainWindow::fhLogFile "Select file has changed since this step's last execution.\n";
		return(1);
	}

	# skip the step
	return(0);
}

sub GetOptions {
	my $self = shift;
	my %opthash;
	
	$opthash{"opt_verifyencode"} = $opt_verifyencode;
	
	return(\%opthash);
}
sub SetOptions {
	my $self = shift;
	my $optref = shift;
	my %opthash = %$optref;
	
	$opt_verifyencode = $opthash{"opt_verifyencode"};
}
sub QueryOptions {
	my $self = shift;
	my($ok) = ('OK');
	my($cancel) = ('Cancel');
    my $DIALOG;
	my $prevval = $opt_verifyencode;

	$DB::single = 1;
	$DIALOG = $flow::MainWindow->Dialog(
	    -title          => 'Set FLAC encoding Options',
        -default_button => $ok,
        -buttons        => [$ok, $cancel],
    );

    my $sb = $DIALOG->Checkbutton(
		-variable => \$opt_verifyencode,
		-relief => 'flat',
    )->pack(-side => 'left');
	$DIALOG->Label(-text => 'Select to enable comparison of output to input.')->pack(-side => 'left');
    
    my $button = $DIALOG->Show;
	
	if ($button eq $cancel) {
		$opt_verifyencode = $prevval;
	}
}

=head1 AUTHOR

  � 2014 Steve Coward <steve-coward@comcast.net>
 *All rights reserved. No warranty, explicit or implicit, provided.
 In no event shall the author be liable for any claim or damages.
 
=cut

1;
