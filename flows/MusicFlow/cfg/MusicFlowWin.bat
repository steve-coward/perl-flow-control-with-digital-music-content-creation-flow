# the incoming music
set Music_Dir=n:\My Music\FLAC Archive
#set Music_Dir=j:\My Music\FLAC Archive\John Scofield
#set Music_Dir=k:\My Music\iTunes
# the released library
set Music_Lib=k:\My Music\iTunes
set MusicFlow_Dir=k:\My Music\FlowControl\flows\MusicFlow
set Selection=$ENV{'MusicFlow_Dir'}\selection.txt
set Cfg_Dir=$ENV{'MusicFlow_Dir'}\cfg
set Comp_Dir=$ENV{'MusicFlow_Dir'}\bin\components
set HDCD_Exe=$ENV{'MusicFlow_Dir'}\bin\components\HDCD\hdcd.exe
set Tag_Exe=$ENV{'MusicFlow_Dir'}\bin\components\Tag\Tag.exe
set AddMP3Art_Exe=$ENV{'MusicFlow_Dir'}\bin\components\AddMP3Art.exe
set Open_Exe=k:\My Music\FlowControl\bin\open.exe
set Flac_Exe=c:\Program Files (x86)\Exact Audio Copy\Flac\flac.exe
set MetaFlac_Exe=c:\Program Files (x86)\FLAC\metaflac.exe
set Lame_Exe=c:\Program Files (x86)\foobar2000 v0.9.6.8\components\lame.exe
set Art_Exe=c:\Program Files (x86)\AlbumArtDownloader\AlbumArt.exe
set MP3Tag_Exe=c:\Program Files (x86)\Mp3tag\Mp3tag.exe
