
=head1 NAME

=head1 SYNOPSIS

      use MusicFlow::bin::GetArtwork;
      my $GetArtwork = MusicFlow::bin::GetArtwork->new();
      $GetArtwork->RunStep;

=head1 DESCRIPTION

  This is an object-oriented library that prompts searhes the web for cover art
  for a music file and prompts the user to select cover art from the covers
  that were found.

=head2 Methods

=cut

package MusicFlow::bin::GetArtwork;
use base MusicFlow::bin::Common;

use strict;
use warnings;
use File::stat;
use File::Spec::Functions;
#use Tk::Dialog;
 
our $VERSION = "2.00";

my $opt_minsize = 400;
  
# The constructor of an object is called new() by convention.  Any
# method may construct an object and you can have as many as you like.
 
sub new {
	my($class, %args) = @_;
	my $self = bless({}, $class);
	return $self;
}
   
sub RunStep {
	my $self = shift;
	my $logFile = shift;
	my $trackinfo = shift;
	my $cmd;
	my $retval = 0;
	my $rval;
	my $trackpath;
	my $artist;
	my $album;
	my $track;
	my $artexepath = $ENV{"Art_Exe"};
	my $artsavepath;
	my @processedalbums;
	my $log;
	my $tid = threads->tid();
	
	$log .= "Start art work search\n";
		
	if (defined $trackinfo) {
		# trackinfo contains path, artists, album, track
		($trackpath, $artist, $album, $track) = split(/::/, $trackinfo);
		
		if ( grep { $_ eq $album} @processedalbums ) {
			# already downloaded artwork for this album
			next;
		}
		
		$artsavepath = catfile($trackpath, $artist, $album, "Folder.jpg");
		$artsavepath = $self->WindowizePath($artsavepath);
	
		$log .= "Retrieve artwork for $trackpath $artist $album $track\n";
		$cmd = "\"$artexepath\" -artist \"$artist\" -album \"$album\" -path \"$artsavepath\" -minsize $opt_minsize -sort size -autoclose";
		my $output = `$cmd 2>&1`;
		$rval = $?;
		print $MainWindow::fhLogFile $output . "\n";
		if ($rval ne 0) {
			$log .= "ERROR: Album Art download encountered error (exit code: $rval)\n";
			$retval += $rval;
		}
		push(@processedalbums, $album);
	}

EXIT:
	$log .= "($tid Album Art) exits with status $retval\n";
	my @retarray = ($tid, $log, $retval >> 8);
	
	return(\@retarray);
}
  
sub GetOptions {
	my $self = shift;
	my %opthash;
	
	$opthash{"opt_minsize"} = $opt_minsize;
	
	return(\%opthash);
}
sub SetOptions {
	my $self = shift;
	my $optref = shift;
	my %opthash = %$optref;
	
	$opt_minsize = $opthash{"opt_minsize"};
}
sub QueryOptions {
	my $self = shift;
    my($ok) = ('OK');
	my($cancel) = ('Cancel');
    my $DIALOG;
    
	$DIALOG = $flow::MainWindow->Dialog(
	    -title          => 'Set Artwork Options',
        -default_button => $ok,
        -buttons        => [$ok, $cancel],
    );

    my $sb = $DIALOG->Spinbox(
        qw/-from 100 -to 800 -increment 100 -width 20/,)->pack(-side => 'left');
	$DIALOG->Label(-text => 'Select a minimum size for displayed artwork.')->pack(-side => 'left');
    
    $sb->set($opt_minsize);
    
    my $button = $DIALOG->Show;
    
    if ($button eq $ok) {
		$opt_minsize = $sb->get();
	}
} # end dialog1

# return >0 if this step needs to run
# return 0  if this step does not need to run
# return <0 if this step can not run
sub CheckDependencies {
	my $self = shift;
	my $lastrun = shift;
	my $sbi;
	my $sbo;

	if (!defined $ENV{"Selection"}) {
		#print $MainWindow::fhLogFile "The environment variable \"Bin_Dir\" must be defined.\n";
		#print $MainWindow::fhLogFile "Exiting ...\n";
		exit(-1);
	}
	
	my $selectFile = $ENV{"Selection"};
	
	if (!-e $selectFile) {
		#print $MainWindow::fhLogFile "Selection file $selectFile does not exist.\n";
		return(-1);
	}
	
	#print $MainWindow::fhLogFile $selectFile . "\n";
	$sbi = stat($selectFile);
	my $in_time = $sbi->mtime;
	
	if ($in_time >= $lastrun) {
		# run the step
		#print $MainWindow::fhLogFile "Select file has changed since this step's last execution.\n";
		return(1);
	}

	# skip the step
	return(0);
}
	
=head1 AUTHOR

  � 2014 Steve Coward <steve-coward@comcast.net>
 *All rights reserved. No warranty, explicit or implicit, provided.
 In no event shall the author be liable for any claim or damages.

=cut

1;
