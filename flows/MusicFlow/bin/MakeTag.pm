
=head1 NAME

=head1 SYNOPSIS

      use MusicFlow::bin::MakeTag;
      my $makeTag = MusicFlow::bin::MakeTag->new();
      $makeTag->RunStep;

=head1 DESCRIPTION

  This is an object-oriented library that creates a tags.txt file
  in the album directory if the file does not already exist.  The file
  is created by first reading "all tags.txt" to find existing tags and then
  overwriting those tags with info gathered from parsing the wiki page
  for the album if it exists.  Several
  automatic attempts are made to find the wiki page.  If they all fail the
  user is prompted for the proper url.  Once the page is found it is parsed
  for album tag information.  The information is then presented to the user
  via a dialog window to enable him to verify and edit the tags. Then the
  final tags are written to a tags.txt file.

=head2 Methods

=cut

package MusicFlow::bin::MakeTag;
use base MusicFlow::bin::Common;

use strict;
use warnings;
use File::stat;
use File::Spec::Functions;
#use Tk::Dialog;
use MP3::Tag;
use MP3::Info;
#use LWP::Simple;
use LWP;
use HTML::Parser;
require HTML::TokeParser::Simple;
use Encode;
#use Win32API::File 0.08 qw( :ALL );
#use Win32::FindFile qw(wchar);

my $opt_prompt = 1;
my $opt_url;
our $VERSION = "2.00";

# Inherit from the "Exporter" module which handles exporting functions.
# Most procedural modules make use of this.
use base 'Exporter';

# The constructor of an object is called new() by convention.	Any
# method may construct an object and you can have as many as you like.
sub new {
	my($class, %args) = @_;
	my $self = bless({}, $class);
	return $self;
}

sub RunStep {
	my $self = shift;
	my $logFile = shift;
	my $trackinfo = shift;
	my $retval = 0;
	my $trackpath;
	my $musicfile;
	my $tagfile;
	my $artfile;
	# is this needed?
	my $regExComposerName = "\\D[\\w\\s,\.\\-'\\/áéíóúÁÉÍÓÚÑñäëïöüÄËÏÖÜçãôênÅåø!]+";
	my $artist;
	my $album;
	my $track;
	my $ALLTAGSFILE;
	my $tagstext;
	my @tagtracks;
	my @tagcomposers;
	my @tagbands;
	my @tagcomments;
	my @tagdiscs;
	my $tagyear;
	my $tagrecordingdate;
	my $tagsource;
	my $taggenre;
	my $tagtotaltracks = 0;
	my $tagpublisher;
	my $log;
	my $tid = threads->tid();
	
	# find each track in iTunes/tags.txt file
	# and, if found, parse metatags contained
	$tagstext = "";
	if (defined $trackinfo) {
		# trackinfo contains path, artists, album, track
		($trackpath, $artist, $album, $track) = split(/::/, $trackinfo);
		#if ( grep { $_ eq $album} @processedalbums ) {
			# already downloaded artwork for this album
			#next;
		#}
		#push(@processedalbums, $album);
		#push(@processedtracks, $trackinfo);

		$musicfile = catfile($trackpath, $artist, $album, $track);
		$tagfile = catfile($trackpath, $artist, $album, "tags.txt");
		$artfile = catfile($trackpath, $artist, $album, "Folder.jpg");
		
		if (-e $tagfile) {
			#$log .= "Tag file already exists.\n";
			goto EXIT;
		}
		$log .= "Start creating tag files\n";

		my $title;
		my $albumartist;
		my $releasedate;
		my $recorddate;
		my $location;
		my $defaultcomposer;
		my $genre;
		my $recordlabel;
		my $producer;
		my @titles;
		my %credits;

		# parse "all tags.txt"
		my $alltagsfile = "K:\\My Music\\iTunes\\HTMLtags.txt";
		my $lines;
		my @lines;
		my $alltagsfh;
		if (-e $alltagsfile) {
			open ($alltagsfh, '<', $alltagsfile) or die $!;
			$/ = undef;
			$lines = <$alltagsfh>;
			close $alltagsfh;
			@lines = split(/\n/, $lines);
		}

		my $pattern = '([^^]*)\^([a-zA-Z_1-9/]*)';
		# set tokens to their reversed values!
		my $separator = "^";
		my $rpath = "htap";
		my $rband = "dnab";
		my $rtitle = "eltit";
		my $rskip = "piks";
		my $rsource = "ecruos";
		my $rencodedby = "ybdedocne";
		my $rdisc = "rebmuncsid";
		my $rtrack = "kcart";
		my $rtotaltracks = "skcartlatot";
		my $rcomposer = "resopmoc";
		my $rcomment = "tnemmoc";
		my $rpublisher = "rehsilbup";
		my $ryear = "raey";
		my $rgenre = "erneg";
		my $rrecordingdate = "etadgnidrocer";
		
		my $tagartist;
		my $tagalbum;
		my $tagtitle;
		my $tagdisc;
		my $tagencode;
		my $tagband;
		my $tagtrack;
		my %tagcredits;
		my $tagtracknum;
		my $tagcomposer;
		my $tagdiscnum;
		my $tagcomment;
		
		foreach my $line (@lines) {
			my $basicalbum = $album;
			$basicalbum =~ s/\[[ a-zA-Z]*\]//;
			if (($line =~ m/\Q$artist/) && ($line =~ m/\Q$basicalbum/) && ($line =~ m/\Q$track/)) {
				chomp($line);
				$line = $self->trim($line);
				my $rline = reverse $line;
				while ($rline =~ m/$pattern/) {
					my $token = $2;
					my $value = $1;

					$rline =~ s/$pattern//;
					$rline = $self->trim($rline);
				
					if ($token eq $rpath) {
					}
					elsif ($token eq $rskip) {
						$value = 1;
					}
					elsif ($token eq $rsource) {
						$tagsource = $value;
						#$tagsource =~ s/$/ ypoc calf/;
						$tagsource = reverse $tagsource;
					}
					elsif ($token eq $rencodedby) {
						$tagencode = $value;
						$tagencode =~ s/^/emal>- /;
						$tagencode = reverse $tagencode;
					}
					elsif ($token eq $rtitle) {
						$tagtitle = reverse $value;
						push(@tagtracks, $tagtitle);
					}
					elsif ($token eq $rcomment) {
						$tagcomment = reverse $value;
						push(@tagcomments, $tagcomment);
					}
					elsif ($token eq $rcomposer) {
						$tagcomposer = reverse $value;
						push(@tagcomposers, $tagcomposer);
					}
					elsif ($token eq $rdisc) {
						$tagdisc = reverse $value;
						$tagdisc =~ s/\/\d+//;
						push(@tagdiscs, $tagdisc);
					}
					elsif ($token eq $rtrack) {
						$tagtrack = reverse $value;
					}
					elsif ($token eq $rtotaltracks) {
						$tagtotaltracks = reverse $value;
					}
					elsif ($token eq $rgenre) {
						$taggenre = reverse $value;
					}
					elsif ($token eq $rsource) {
						$tagsource = reverse $value;
					}
					elsif ($token eq $ryear) {
						$tagyear = reverse $value;
					}
					elsif ($token eq $rrecordingdate) {
						$tagrecordingdate = reverse $value;
					}
					elsif ($token eq $rpublisher) {
						$tagpublisher = reverse $value;
					}
					elsif ($token eq $rband) {
						$tagband = reverse $value;
						push(@tagbands, $tagband);
					}
					#$outline = $outline . $value . $separator . $token . " ";
				}
				print $tagtitle . "\n";
				print $tagsource . "\n";
				print $tagencode . "\n";
				print $tagdisc . "\n";
				print $tagtrack . "\n";
				#print $tagtitle . "\n";
				#last;
			}
		}

		close $alltagsfile;
	}
	else {
		if (@tagtracks == 0) {
			$retval |= 8;
			goto EXIT;
		}
		last;
	}
	
	# if the track is present in iTunes/tags.txt
	# transform into $tagstext string
	if (@tagtracks > 0) {
		$tagstext = "# from music file metatag\nArtist $artist\nAlbum $album\n";
		$tagstext = $tagstext . "Recorded " . $tagrecordingdate . "\n";
		$tagstext = $tagstext . "Released " . $tagyear . "\n";
		$tagstext = $tagstext . "Genre " . $taggenre . "\n";
		$tagstext = $tagstext . "Source " . $tagsource . "\n";
		$tagstext = $tagstext . "Label " . $tagpublisher . "\n";
		$tagstext = $tagstext . "\nTrack Listing\n";
		my $discn = 1;
		my $tracknum = 0;
		foreach my $t (@tagtracks) {
			my $d = shift(@tagdiscs);
			my $l = shift(@tagcomments);
			my $b = shift(@tagbands);
			my $c = shift(@tagcomposers);
			$tracknum++;
			if ($d != $discn) {
				$discn = $d;
				$tracknum = 1;
				#$tagstext = $tagstext . "Disc $discn\n";
			}
			my $ftracknum = $self->FormatTrackNumber($tracknum);
			$tagstext = $tagstext . "\"" . "$discn-$ftracknum " . $t . "\"";
			if ((defined $c) && ($c ne "")) {
				$tagstext = $tagstext . " \(" . $c . "\)";
			}
			if ((defined $l) && ($l ne "")) {
				$tagstext = $tagstext . " \<" . $l . "\>";
			}
			$tagstext = $tagstext . "\n";
			my @b = split(/\\\\/, $b);
			foreach my $m (@b) {
				$tagstext = $tagstext . $m . "\n";
			}
		}
		$tagstext = $tagstext . "\nPersonnel\n";
	}
	
	if (($tagtotaltracks == @tagtracks) && (@tagtracks > 0)) {
		# all tracks present in iTunes/tags.txt
		# create file and exit
		open (TAG, ">", "$tagfile") or die $!;
		print TAG $tagstext;
		close TAG;
	}
	elsif (($tagtotaltracks != @tagtracks) || (@tagtracks == 0)) {
		# no tracks or some tracks present in iTunes/tags.txt
		# search for wiki page for album, or prompt user for a url
		# parse html for metatag info
		my $title;
		my $albumartist;
		my $releasedate;
		my $recorddate;
		my $location;
		my $defaultcomposer;
		my $genre;
		my $recordlabel;
		my $producer;
		my @titles;
		my %credits;
		if (defined $trackinfo) {
			# trackinfo contains path, artists, album, track
			($trackpath, $artist, $album, $track) = split(/::/, $trackinfo);
			#if ( grep { $_ eq $album} @processedalbums ) {
				# already downloaded artwork for this album
				#next;
			#}
			#push(@processedalbums, $album);

			$musicfile = catfile($trackpath, $artist, $album, $track);
			$tagfile = catfile($trackpath, $artist, $album, "tags.txt");
			$artfile = catfile($trackpath, $artist, $album, "Folder.jpg");
			
			#DeleteFileW(wchar($tagfile));
			if (!-e $tagfile) {
				
				my $ua = LWP::UserAgent->new;
				my $response;
				my $tp;

				#Wikipedia does not have an article with this exact name
				#$theurl = 'http://en.wikipedia.org/wiki/A_Hard_Day%27s_Night_(album)';
				#$theurl = 'http://en.wikipedia.org/wiki/Born_to_Be_Blue_(Grant_Green_album)';
				#$theurl = 'http://en.wikipedia.org/wiki/The_Doors_(album)';
				#$theurl = 'http://en.wikipedia.org/wiki/No_Woman,_No_Cry';
				#$theurl = 'http://en.wikipedia.org/wiki/Gimme_Back_My_Bullets';
				my %albuminfo;
				my $tracktext;
				my $url;
				my $urlhead = "http://en.wikipedia.org/wiki/";
				my @pages;
				my $page;
				my $token;
				my $theurl;
				my $foundURL = 0;
				
				if (!defined $opt_url) {
					$page = $album;
					$page =~ s/ /_/g;
					push @pages, $page;
					$page = $page . "_(album)";
					push @pages, $page;
					$page = $album . " (" . $artist . "_album)";
					$page =~ s/ /_/g;
					push @pages, $page;
					foreach $page (reverse @pages) {
						if ($foundURL == 0) {
							$foundURL = 1;
							$url = $urlhead . $page;
							$response = $ua->get($url);
							$tp = HTML::TokeParser::Simple->new(\$response->content) || die "Can't open: $!";
							while ( $token = $tp->get_token ) {
								next unless @$token[0] eq 'T';
								if (@$token[1] =~ /Wikipedia does not have an article with this exact name/) {
									$foundURL = 0;
									last;
								}
								if (@$token[1] =~ /Disambiguation/) {
									$foundURL = 0;
									last;
								} 
							}
							if ($foundURL == 1) {
								$theurl = $url;
								last;
							}
						}
					}
				}
				else {
					$theurl = $opt_url;
				}
								
				# get text from url page
				$response = $ua->get($theurl);
				#print $MainWindow::fhLogFile $response;
				#print $MainWindow::fhLogFile $response->content;
				
				$DB::single = 1;
				$tp = HTML::TokeParser::Simple->new(\$response->content) || die "Can't open: $!";
				
				###########################
				# search for album info
				###########################
				# album title
				$tp->get_tag('title');
				$title = $tp->get_token->as_is;
				$title =~ s/\s*-\s*Wikipedia, the free encyclopedia//;
				$title =~ s/\s*\([\w\s]*album\)\s*//;
				$title = capitalize($title);
			 
				# artist name
				$albumartist = "undefined";
				while ( $token = $tp->get_tag('a') ) {
					my $href = $token->get_attr('href');
					next unless (defined($href)) and ($href =~ /album/i);
					$albumartist = "";
					while ( $token = $tp->get_token ) {
						if ($token->is_end_tag('th')) {
							last;
						}
						next unless @$token[0] eq 'T';
						$albumartist = $albumartist . @$token[1];
					}
					last;
				}
				$albumartist =~ s/^[\s\w]* by //i;
				$albumartist = capitalize($albumartist);
					
				# release date
				$releasedate = "undefined";
				while ( $token = $tp->get_tag('td') ) {
					my $class = $token->get_attr('class');
					next unless (defined($class)) and ($class =~ /published/i);
					while ( $token = $tp->get_token ) {
						next unless @$token[0] eq 'T';
						$releasedate = @$token[1];
						$releasedate = FormatDate($releasedate);
						last;
					}
					
					last;
				}
				
				# record date
				$recorddate = "undefined";
				$location = "undefined";
				while ( $token = $tp->get_tag('th') ) {
					$token = $tp->get_token;
					next unless @$token[0] eq 'T';
					next unless @$token[1] =~ /recorded/i;
					my $date;
					$token = $tp->get_tag('td');
					while ( $token = $tp->get_token ) {
						next unless @$token[0] eq 'T';
						$date = @$token[1];
						while ( $token = $tp->get_token ) {
							last if $token->is_end_tag('td');
							if (@$token[0] eq 'T') {
								$date = $date . @$token[1];
							}
						}
						$recorddate = $date;
						$location = $date;
						if ($location =~ /\s+(at|in) (.*)/i) {
							# location
							$location = $2;
							$recorddate =~ s/\s*$1 $2//;
						}
						$recorddate = FormatDate($recorddate);
						last;
					}
					
					last;
				}
				
				# genre
				$genre = "undefined";
				while ( $token = $tp->get_tag('th') ) {
					$token = $tp->get_tag('a');
					my $title = $token->get_attr('title');
					next unless (defined  $title) and ($title =~ /music genre/i);
					$token = $tp->get_tag('td');
					$token = $tp->get_tag('a');
					while ( $token = $tp->get_token ) {
						next unless @$token[0] eq 'T';
						$genre = @$token[1];
						last;
					}
					
					last;
				}
				$genre = capitalize($genre);
				
				# record label
				$recordlabel = "undefined";
				while ( $token = $tp->get_tag('th') ) {
					$token = $tp->get_tag('a');
					my $title = $token->get_attr('title');
					next unless (defined  $title) and ($title =~ /record label/i);
					$token = $tp->get_tag('td');
					$token = $tp->get_tag('a');
					while ( $token = $tp->get_token ) {
						next unless @$token[0] eq 'T';
						$recordlabel = @$token[1];
						last;
					}
					
					last;
				}
				
				# producer
				$producer = "undefined";
				while ( $token = $tp->get_tag('th') ) {
					$token = $tp->get_tag('a');
					my $title = $token->get_attr('title');
					next unless (defined  $title) and ($title =~ /record producer/i);
					$token = $tp->get_tag('td');
					$token = $tp->get_tag('a');
					while ( $token = $tp->get_token ) {
						next unless @$token[0] eq 'T';
						$producer = @$token[1];
						last;
					}
					
					last;
				}
				$producer = capitalize($producer);
				
				###########################
				# search for album tracks
				###########################
				while ( my $table = $tp->get_tag('table') ) {
					my $tableclass = $table->get_attr('class');
					next unless (defined($tableclass)) and ($tableclass eq 'tracklist');
					while ( $token = $tp->get_token ) {
						last if ($token->is_end_tag('table'));
						next unless ($token->is_tag('a'));
						next if ($token->is_end_tag('a'));
						my $songtitle = $tp->get_token;
						$songtitle = capitalize($songtitle->as_is);
						push @titles, $songtitle;
					}
				}
				if (!@titles) {
					# try again - look for list this time
					$tp = HTML::TokeParser::Simple->new(\$response->content) || die "Can't open: $!";

					my $foundtracklist = 0;
					while (($foundtracklist == 0) && (my $list = $tp->get_tag('span') )) {
						my $listid = $list->get_attr('id');
						next unless (defined($listid)) and ($listid =~ /track_listing/i);
						
						while ( $token = $tp->get_token ) {
							if ($token->is_start_tag('h2')) {
								$foundtracklist = 1;
								last;
							}
							elsif ($token->is_start_tag('p')) {
								$token = $tp->get_token;
								if (@$token[0] eq 'T') {
									if (@$token[1] =~ /all/i) {
										$defaultcomposer = @$token[1];
										if (defined $defaultcomposer) {
											while ( $token = $tp->get_token ) {
												last if $token->is_end_tag('p');
												if (@$token[0] eq 'T') {	
													$defaultcomposer = $defaultcomposer . @$token[1];
												}
											}
											$defaultcomposer =~ /^All (compositions|songs|tracks|pieces|music and lyrics|music)( are| were|)( written and arranged| written| composed|) by ($regExComposerName)\./i;
											$defaultcomposer = $4;
											$defaultcomposer =~ s/[,]*\s*except [\w\s]*//;
											$defaultcomposer =~ s/unless [\w\s]*//;
											$defaultcomposer =~ s/\s*,\s*and\s+/, /;
											$defaultcomposer =~ s/\s+and\s+/, /;
											$defaultcomposer = $self->trim($defaultcomposer);
											$defaultcomposer =~ s/\.$//;
											$defaultcomposer =~ s/,$//;
											$defaultcomposer = $self->trim($defaultcomposer);
											$defaultcomposer = $self->Capitalize($defaultcomposer);
										}
									}
								}
							}
							next unless $token->is_tag('li');
							
							my $songtitle;
							my $composer;
							while ( $token = $tp->get_token ) {
								last if ($token->is_end_tag('li'));
								next unless @$token[0] eq 'T';
								# found a track title
								$songtitle = $songtitle . @$token[1];
							}
							$songtitle =~ s/(\"\s+\(.*\)).*/$1/;
							$composer = $songtitle;
							$songtitle =~ s/(\".*\").*/$1/;
							$composer =~ s/(\".*\"\s*)(.*)/$2/;
							if ($composer =~ /^\(/) {
								$composer =~ s/(\()(.*)(\)).*/$2/;
							}
							else {
								$composer = "";
							}
							
							$songtitle = capitalize($songtitle);
							push @titles, $songtitle;
							if ((!defined $composer) || ($composer eq "")) {
								push @titles, $defaultcomposer;
							}
							else {
								$composer = capitalize($composer);
								push @titles, $composer;
							}
						}
					}
				}
				
				$tp = HTML::TokeParser::Simple->new(\$response->content) || die "Can't open: $!";
				
				###########################
				# search for album credits
				###########################
				my $artist;
				my $foundpersonnel = 0;
				my $list;
				while ( ($foundpersonnel == 0) && ($list = $tp->get_tag('span')) ) {
					my $listid = $list->get_attr('id');
					next unless (defined($listid)) and ($listid eq 'Personnel');
					# found personnel list
					
					$token = $tp->get_tag('li');
					while ( $foundpersonnel == 0 ) {
						# found a list item
						
						undef $artist;
						# iterate through list components
						while ( $token = $tp->get_token ) {
							
							last if ($token->is_end_tag('li'));
							
							if ($token->is_end_tag('a')) {
								next;
							}
							elsif ($token->is_text()) {
								my $text = $token->as_is;
								$text = decode( 'utf8', $text );
								$text =~ s/&#160;//g;
								$text =~ s/\x{2013}//g;
								next unless ($text =~ /[0-9a-z;,]+/);
								if (!defined $artist) {
									if (UNIVERSAL::can($text, 'as_is')) {
										$artist = $text->as_is;
										$artist = capitalize($artist);
										$credits{$artist} = "";
									}
									else {
										$credits{$text} = "";
									}
									
								}
								else {
									if ($credits{$artist}  =~ / $/) {
										$credits{$artist} = $credits{$artist} . $text;
									}
									else {
										$credits{$artist} = $credits{$artist} . " " . $text;
									}
								}
								next;
							}
							next unless ($token->is_tag('a'));
							
							my $atext = $tp->get_token;
							if (!defined $artist) {
								$artist = $atext->as_is;
								$artist = capitalize($artist);
								$credits{$artist} = "";
							}
							else {
								if ($credits{$artist}  =~ / $/) {
									$credits{$artist} = $credits{$artist} . $atext->as_is;
								}
								else {
									$credits{$artist} = $credits{$artist} . " " . $atext->as_is;
								}
							}
							$credits{$artist} =~ s/\s+,/,/g;
						}
						while ( $token = $tp->get_token ) {
							if ($token->is_end_tag('ul')) {
								$foundpersonnel = 1;
								last;
							}
							elsif ($token->is_tag('li')) {
								last;
							}
						}
					}
				}
				
				###########################
				# Print tags
				###########################
				
				$albuminfo{hdr}{artist} = $albumartist if defined $albumartist;
				$albuminfo{hdr}{album} = $title if defined $title;
				$albuminfo{hdr}{releasedate} = $releasedate if defined $releasedate;
				$albuminfo{hdr}{recordingdate} = $recorddate if defined $recorddate;
				$albuminfo{hdr}{genre} = $genre if defined $genre;
				$albuminfo{hdr}{label} = $recordlabel if defined $recordlabel;
				$albuminfo{hdr}{producer} = $producer if defined $producer;
				$albuminfo{hdr}{artist} = $albumartist if defined $albumartist;
				
				$tagstext = $tagstext . "# " . "$theurl\n" if defined $theurl;
				$tagstext = $tagstext . "Artist $albumartist\n" if defined $albumartist;
				$tagstext = $tagstext . "Album $title\n" if defined $title;
				$tagstext = $tagstext . "Released $releasedate\n" if defined $releasedate;
				$tagstext = $tagstext . "Recorded $recorddate\n" if defined $recorddate;
				$tagstext = $tagstext . "Location $location\n" if defined $location;
				$tagstext = $tagstext . "Genre $genre\n" if defined $genre;
				$tagstext = $tagstext . "Label $recordlabel\n" if defined $recordlabel;
				$tagstext = $tagstext . "Producer $producer\n" if defined $producer;
				$tagstext = $tagstext . "UPC \n";
				$tagstext = $tagstext . "Catalog \n";
				#$tagstext = $tagstext . "$defaultcomposer\n" if defined $defaultcomposer;
				$tagstext = $tagstext . "\nTrack Listing\n";
				my $tracknum = 0;
				while (@titles) {
					# sometimes non title info sneaks in
					if (!defined $titles[0]) {
						shift(@titles);
						next;
					}
					elsif ($titles[0] !~ /^\"/) {
						# spit out as comment - it may be an interesting bit of trivia
						$tagstext = $tagstext . "# " . shift(@titles) . "\n";
						next;
					}
					$tracknum++;
					my $ftracknum = $self->FormatTrackNumber($tracknum);
					my $thistitle = shift(@titles);
					$thistitle =~ s/\"//g;
					$tracktext = "\"1-$ftracknum " . $thistitle . "\" (" . shift(@titles). ")\n";
					$tagstext = $tagstext . $tracktext;
					
					foreach my $musician (keys %credits) {
						$tracktext = $tracktext . $musician . ":" . $credits{$musician} . "\n";
						$tagstext = $tagstext . $musician . ":" . $credits{$musician} . "\n";
					}
					$albuminfo{tracks}{100*1+$tracknum} = $tracktext;
				}
				
				$self->writeTagsFile($tagfile, \%albuminfo);
				
				$tagstext = $tagstext . "\nPersonnel\n";
				foreach my $musician (keys %credits) {
					$tagstext = $tagstext . $musician . ":" . $credits{$musician} . "\n";
				}
				for (keys %credits)
				{
					delete $credits{$_};
				}
				
				#open (TAGFILE, ">", "$tagfile") or die $!;
				#print TAGFILE $tagstext;
				#close TAGFILE;
				$tagstext = "";
			}
		}
		else {
			last;
		}
	
		my @tagrecord;
		if ((-e $tagfile) && (-f $tagfile)) {
			open (TAG, "$tagfile") or die $!;
	
			my $tmpstring;
			while (my $line = <TAG>) {
				#$log .= $line;
				#$log .= "\n";
			
				chomp($line);
				$line = $self->trim($line);
						
				#$log .= $line . "\n";
				
				if ($line =~ /^\s*$/) {
					next;
				}
				if ($line =~ /^#/) {
					next;
				}
				push(@tagrecord, $line);
			}
			close TAG;
		}
		else {
			$log .= "ERROR: could not open: $tagfile\n";
			$retval |= 2;
			goto EXIT;
		}
			
		my $mismatchfound = 0;
		my $thisonefound;
	
		if (defined $trackinfo) {
			# trackinfo contains path, artists, album, track
			($trackpath, $artist, $album, $track) = split(/::/, $trackinfo);

			my $musicfile = $track;
			$musicfile =~ s/\.(flac|mp3)$//;
			$musicfile =~ s/128_q0//;
			$musicfile =~ s/320_q0//;
			$musicfile =~ s/^\d-\d\d //;
			$musicfile =~ s/;/:/g;
			$musicfile =~ s/=/\?/g;
			$musicfile =~ s/_/\//g;
			# does not check track ordering
			$thisonefound = 0;
			if (grep {$_ =~ /"$musicfile"/} @tagrecord) {
				$thisonefound = 1;
			}
			if ($thisonefound == 0) {
				$log .= "ERROR: track name mismatch: $musicfile\n";
				$mismatchfound = 1;
			}
		}
		else {
			last;
		}
	
		if ($mismatchfound) {
			$retval |= 4;
			goto EXIT;
		}
	}
	else {
		$retval |= 8;
		goto EXIT;
	}
	
EXIT:
	$log .= "($tid MakeTag) exits with status $retval\n";
	my @retarray = ($tid, $log, $retval >> 8);
	
	return(\@retarray);
}
	
sub GetOptions {
	my $self = shift;
	my %opthash;
	
	$opthash{"opt_prompt"} = $opt_prompt;
	
	return(\%opthash);
}
sub SetOptions {
	my $self = shift;
	my $optref = shift;
	my %opthash = %$optref;
	
	$opt_prompt = $opthash{"opt_prompt"};
}
sub QueryOptions {
	my $self = shift;
	my($ok) = ('OK');
	my($cancel) = ('Cancel');
    my $DIALOG;

	$DIALOG = $flow::MainWindow->Dialog(
	    -title          => 'Enter URL for music tag page',
        -default_button => $ok,
        -buttons        => [$ok, $cancel],
    );
    
	my(@pl) = qw/-side top -fill x/;
	my(@scrolled_attributes) = qw/Entry -relief sunken -scrollbars s/;
	my(@spacer_attributes) = qw/-width 20 -height 10/;
	
	$DIALOG->Label(-text => 'Enter URL:')->pack(-side => 'left');
	my $e1 = $DIALOG->Scrolled(@scrolled_attributes)->pack(@pl);
	my $button = $DIALOG->Show;
	my $url = $e1->get( );
	
	if ($button eq $cancel) {
		return;
	}
	
	$opt_url = $url;
}

# return >0 if this step needs to run
# return 0	if this step does not need to run
# return <0 if this step can not run
sub CheckDependencies {
	my $self = shift;
	my $lastrun = shift;
	my $sbi;
	my $sbo;

	if (!defined $ENV{"Selection"}) {
		#print $MainWindow::fhLogFile "The environment variable \"Bin_Dir\" must be defined.\n";
		#print $MainWindow::fhLogFile "Exiting ...\n";
		exit(-1);
	}
	
	my $selectFile = $ENV{"Selection"};
	
	if (!-e $selectFile) {
		#print $MainWindow::fhLogFile "Selection file $selectFile does not exist.\n";
		return(-1);
	}
	
	$sbi = stat($selectFile);
	my $in_time = $sbi->mtime;
	
	if ($in_time >= $lastrun) {
		# run the step
		#print $MainWindow::fhLogFile "Select file has changed since this step's last execution.\n";
		return(1);
	}

	# skip the step
	return(0);
}

sub FormatDate($)
{
	my $self = shift;
	my $datestring = shift;
	my $date;
	my $mo;
	my $dy = "";
	my $yr;
		
	if ($datestring =~ /(\d\d\d\d)/) {
		if (length($1) lt 10) { $yr = "0" . $1; }
		else { $yr = $1; }
	}
	if ($datestring =~ /([a-zA-Z]+)/) {
		$mo = $1;
		if ($mo =~ /jan/i) { $mo = "01"; }
		if ($mo =~ /feb/i) { $mo = "02"; }
		if ($mo =~ /mar/i) { $mo = "03"; }
		if ($mo =~ /apr/i) { $mo = "04"; }
		if ($mo =~ /may/i) { $mo = "05"; }
		if ($mo =~ /jun/i) { $mo = "06"; }
		if ($mo =~ /jul/i) { $mo = "07"; }
		if ($mo =~ /aug/i) { $mo = "08"; }
		if ($mo =~ /sep/i) { $mo = "09"; }
		if ($mo =~ /oct/i) { $mo = "10"; }
		if ($mo =~ /nov/i) { $mo = "11"; }
		if ($mo =~ /dec/i) { $mo = "12"; }
	}
	elsif ($datestring =~ /([\s^\-\/])(\d\d?)(\s|\,|$|\-|\/)/) {
		$mo = $2;
		$datestring =~ s/$2$3//;
		if (length($mo) lt 10) { $mo = "0" . $mo; }			
	}
		
	if ($datestring =~ /([\s^\-\/])(\d\d?)(st|nd|rd|th|)(\s|\,|$|\-|\/)/) {
		$dy = $2;
		$datestring =~ s/$1$2$3//;
		if (length($dy) lt 10) { $dy = "0" . $dy; }
	}

	$date = "${yr}";
	if (length($mo) eq 2) {
		$date = $date . "-${mo}";
		if (length($dy) eq 2) {
				$date = $date . "-${dy}";
		}
	}
	
	return $date;
}

sub printAll
{
	my $self = shift;
	my $filename = shift;
	my $tag_info = '';
	my $tag_long = '';
	my %tag_hash = ();

	my $mp3 = MP3::Tag->new($filename);
	$mp3->get_tags();
	my $id3v2 = $mp3->{ID3v2};
	$mp3->close();

	print_simpleTag('IPLS', $id3v2); # IPLS : Involved people list
	print_simpleTag('MCDI', $id3v2); # MCDI : Music CD identifier
	print_simpleTag('PCNT', $id3v2); # PCNT : Play counter
	print_simpleTag('TALB', $id3v2); # TALB : Album/Movie/Show title
	print_simpleTag('TBPM', $id3v2); # TBPM : BPM (beats per minute)
	print_simpleTag('TCOM', $id3v2); # TCOM : Composer
	print_simpleTag('TCON', $id3v2); # TCON : Content type
	print_simpleTag('TCOP', $id3v2); # TCOP : Copyright message
	print_simpleTag('TDAT', $id3v2); # TDAT : Date
	print_simpleTag('TDLY', $id3v2); # TDLY : Playlist delay
	print_simpleTag('TENC', $id3v2); # TENC : Encoded by
	print_simpleTag('TEXT', $id3v2); # TEXT : Lyricist/Text writer
	print_simpleTag('TFLT', $id3v2); # TFLT : File type
	print_simpleTag('TIME', $id3v2); # TIME : Time
	print_simpleTag('TIPL', $id3v2); # TIPL : Involved people list
	print_simpleTag('TIT1', $id3v2); # TIT1 : Content group description
	print_simpleTag('TIT2', $id3v2); # TIT2 : Title/songname/content description
	print_simpleTag('TIT3', $id3v2); # TIT3 : Subtitle/Description refinement
	print_simpleTag('TKEY', $id3v2); # TKEY : Initial key
	print_simpleTag('TLAN', $id3v2); # TLAN : Language(s)
	print_simpleTag('TLEN', $id3v2); # TLEN : Length
	print_simpleTag('TMCL', $id3v2); # TMCL : Musician credits list
	print_simpleTag('TMED', $id3v2); # TMED : Media type
	print_simpleTag('TOAL', $id3v2); # TOAL : Original album/movie/show title
	print_simpleTag('TOFN', $id3v2); # TOFN : Original filename
	print_simpleTag('TOLY', $id3v2); # TOLY : Original lyricist(s)/text writer(s)
	print_simpleTag('TOPE', $id3v2); # TOPE : Original artist(s)/performer(s)
	print_simpleTag('TORY', $id3v2); # TORY : Original release year
	print_simpleTag('TOWN', $id3v2); # TOWN : File owner/licensee
	print_simpleTag('TPE1', $id3v2); # TPE1 : Lead performer(s)/Soloist(s)
	print_simpleTag('TPE2', $id3v2); # TPE2 : Band/orchestra/accompaniment
	print_simpleTag('TPE3', $id3v2); # TPE3 : Conductor/performer refinement
	print_simpleTag('TPE4', $id3v2); # TPE4 : Interpreted, remixed, or otherwise modified by
	print_simpleTag('TPOS', $id3v2); # TPOS : Part of a set
	print_simpleTag('TPUB', $id3v2); # TPUB : Publisher
	print_simpleTag('TRCK', $id3v2); # TRCK : Track number/Position in set
	print_simpleTag('TRDA', $id3v2); # TRDA : Recording dates
	print_simpleTag('TRSN', $id3v2); # TRSN : Internet radio station name
	print_simpleTag('TRSO', $id3v2); # TRSO : Internet radio station owner
	print_simpleTag('TSIZ', $id3v2); # TSIZ : Size
	print_simpleTag('TSRC', $id3v2); # TSRC : ISRC (international standard recording code)
	print_simpleTag('TSSE', $id3v2); # TSSE : Software/Hardware and settings used for encoding
	print_simpleTag('TYER', $id3v2); # TYER : Year
	print_simpleTag('WCOM', $id3v2); # WCOM : Commercial information
	print_simpleTag('WCOP', $id3v2); # WCOP : Copyright/Legal information
	print_simpleTag('WOAF', $id3v2); # WOAF : Official audio file webpage
	print_simpleTag('WOAR', $id3v2); # WOAR : Official artist/performer webpage
	print_simpleTag('WOAS', $id3v2); # WOAS : Official audio source webpage
	print_simpleTag('WORS', $id3v2); # WORS : Official internet radio station homepage
	print_simpleTag('WPAY', $id3v2); # WPAY : Payment
	print_simpleTag('WPUB', $id3v2); # WPUB : Publishers official webpage 

	print_complexTag('AENC', $id3v2); # AENC : Audio encryption
	print_complexTag('APIC', $id3v2); # APIC : Attached picture
	print_complexTag('COMM', $id3v2); # COMM : Comments
	print_complexTag('COMR', $id3v2); # COMR : Commercial frame
	print_complexTag('ENCR', $id3v2); # ENCR : Encryption method registration
	print_complexTag('GEOB', $id3v2); # GEOB : General encapsulated object
	print_complexTag('GRID', $id3v2); # GRID : Group identification registration
	print_complexTag('LINK', $id3v2); # LINK : Linked information
	print_complexTag('OWNE', $id3v2); # OWNE : Ownership frame
	print_complexTag('POPM', $id3v2); # POPM : Popularimeter
	print_complexTag('PRIV', $id3v2); # PRIV : Private frame
	print_complexTag('RBUF', $id3v2); # RBUF : Recommended buffer size
	print_complexTag('RVRB', $id3v2); # RVRB : Reverb
	print_complexTag('SYTC', $id3v2); # SYTC : Synchronized tempo codes
	print_complexTag('TXXX', $id3v2); # TXXX : User defined text information frame
	print_complexTag('TXXX01', $id3v2);
	print_complexTag('TXXX02', $id3v2);
	print_complexTag('TXXX03', $id3v2);
	print_complexTag('TXXX04', $id3v2);
	print_complexTag('UFID', $id3v2); # UFID : Unique file identifier
	print_complexTag('USER', $id3v2); # USER : Terms of use
	print_complexTag('USLT', $id3v2); # USLT : Unsychronized lyric/text transcription
	print_complexTag('WXXX', $id3v2); # WXXX : User defined URL link frame

	# Following frames are only supported in raw mode:
	# CRM : Encrypted meta frame
	# EQUA : Equalization
	# ETCO : Event timing codes
	# LNK : Linked information
	# MLLT : MPEG location lookup table
	# PIC : Attached picture
	# POSS : Position synchronisation frame
	# RVAD : Relative volume adjustment
	# SYLT : Synchronized lyric/text 
}



sub print_simpleTag($)
{
	my $self = shift;
	my $tag = shift;
	my $id3v2 = shift;

	my ($tag_info, $tag_long) = $id3v2->get_frame($tag);
	if ($tag_long) {
		print STDOUT "$tag: $tag_long = $tag_info\n";
	}
} 



sub print_complexTag($)
{
	my $self = shift;
	my $tag = shift;
	my $id3v2 = shift;
	my $key = '';
	my $value = '';

	my ($tag_info, $tag_long) = $id3v2->get_frame($tag);
	if ($tag_long) {
		my %tag_hash = %$tag_info;
		print STDOUT "$tag: $tag_long\n"; 
		while (($key, $value) = each %tag_hash) {
			if (($key eq '_Data') || ($key eq '_Logo')) {
				print STDOUT " $key = not listed (possibly binary data)\n";
			} 
			else {
				print STDOUT " $key = $value\n";
			} 
		}
	}
}

sub capitalize
{
	my $self = shift;
	my $string = shift;
	
	$string =~ s/ (
        (^\w)    #at the beginning of the line
          |      # or
        (\s\w)   #preceded by whitespace
	 ) /\U$1/xg;
    $string =~ s/([\w']+)/\u\L$1/g;
	
	return($string);

}

=head1 AUTHOR

  © 2014 Steve Coward <steve-coward@comcast.net>
 *All rights reserved. No warranty, explicit or implicit, provided.
 In no event shall the author be liable for any claim or damages.

=cut

1;

