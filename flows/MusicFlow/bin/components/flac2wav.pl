#!/usr/bin/perl
# simple driver to convert flac files to wav
# just drives flac, mainly to allow
# wildcards and to remember options so I don't 
# have to. Assumes flac is  on path.
#
# Created on May 22, 2004
# Copyright (c) 2004, Pat Farrell, All rights reserved.
# flac2wav.pl -output "K:/My Music/iTunes/10,000 Maniacs/Blind Man's Zoo/"
#   "K:/My Music/iTunes/10,000 Maniacs/Blind Man's Zoo/*.flac"
#
# This code will be released with a suitable Open Source
# license, probably BSD-like.
# 
# make it handle arguments the way Windows users expect.
#use File::DosGlob 'glob';
use strict;
use File::Glob qw(bsd_glob);
use Getopt::Long;
use File::Basename;

my $retval = 0;
my $help;
my $inspec;
my $outspec;
my $quality = 2;
my $otherLameOptions;
my $flacpath = $ENV{"Flac_Exe"};

usage() if ( $#ARGV < 0 ); 
GetOptions('help|?' => \$help,
			'input=s' => \$inspec,
			'output=s' => \$outspec,
            'quality=i' => \$quality, 
	        'lame=s' => \$otherLameOptions);
usage() if $help;

if ($inspec =~ /.flac$/i ) {
	my $needQuotes = ($inspec =~ m/\s/) ? "\"" : "";
	my $cmd =  "\"$flacpath\" -f -d --silent ";
	#if (defined $outspec) {
	#	$outspec =~ s/\\ / /g;
	#	$outspec = WindowizePath($outspec);
	#	$cmd .= "-o " . $needQuotes . $outspec . $needQuotes . " ";
	#}
	$cmd .= $needQuotes . $inspec . $needQuotes;
	print "$cmd\n";
	my $output = `$cmd 2>&1`;
	print $output;
	$retval = $?;
}

exit($retval);

sub WindowizePath
{
	my $path = shift;
	$path =~ s/\/cygdrive\/k/K:/i;
	$path =~ s/\/cygdrive\/j/J:/i;
	$path =~ s/\//\\/g;
	return $path;
}

sub usage {
    print "convert flac files to wav (PCM)\n";
    print "Usage:\n   flac2wav.pl [-output=path]  {files}\n";
    print "note, the output path has to end with the directory";
    print " separator character.\n";
    print "   \\ on Windows, \/ on linux, etc.\n";
    exit;
}
