
=head1 NAME

=head1 SYNOPSIS

      use MusicFlow::bin::Quality;
      my $Quality = MusicFlow::bin::Quality->new();
      $Quality->RunStep;

=head1 DESCRIPTION

  This is an object-oriented library that performs quality check on music files.
  Currently it is largely a place holder for as yet unidentified checks.

=head2 Methods

=cut

package MusicFlow::bin::Quality;
use base MusicFlow::bin::Common;

use strict;
use warnings;
use File::stat;
use File::Spec::Functions;
#use Tk::Dialog;

our $VERSION = "2.00";

# The constructor of an object is called new() by convention.  Any
# method may construct an object and you can have as many as you like.
sub new {
	my($class, %args) = @_;
	my $self = bless({}, $class);
	return $self;
}

sub RunStep {
	my $self = shift;
	my $logFile = shift;
	my $mp3tagpath = $ENV{"MP3Tag_Exe"};
	my $cmd;
	my $rval;
	my $retval = 0;
	my $log;
	my $tid = threads->tid();
	
    $log .= "Quality\n";

	foreach my $dir (@MainWindow::selectedDirs) {
		print $MainWindow::fhLogFile "QualityCheck:$dir\n";
		$cmd = "\"$mp3tagpath\"  /fp:\"$dir\""; 
		#my $output = `$cmd 2>&1`;
		my $output = qx/`$cmd 2>&1`/;
		$rval = $?;
		$log .= $output . "\n";
		if ($rval ne 0) {
			$log .= "ERROR: mp3tag unable to load $dir\n";
			$retval += $rval;
		}
	}
				
	$log .= "($tid Quality) exits with status $retval\n";
	my @retarray = ($tid, $log, $retval >> 8);
	
	return(\@retarray);
}

sub GetOptions {
	my $self = shift;
	my %opthash;
	
	#$opthash{"opt_minsize"} = $opt_minsize;
	
	return(\%opthash);
}

sub SetOptions {
	my $self = shift;
	my $optref = shift;
	my %opthash = %$optref;
	
	#$opt_minsize = $opthash{"opt_minsize"};
}

sub QueryOptions {
	my $self = shift;
	print STDOUT "Set some options in Quality.\n";
}

# return >0 if this step needs to run
# return 0	if this step does not need to run
# return <0 if this step can not run
sub CheckDependencies {
	my $self = shift;
	my $lastrun = shift;
	my $sbi;
	my $sbo;

	if (!defined $ENV{"Selection"}) {
		#print $MainWindow::fhLogFile "The environment variable \"Bin_Dir\" must be defined.\n";
		#print $MainWindow::fhLogFile "Exiting ...\n";
		exit(-1);
	}
	
	my $selectFile = $ENV{"Selection"};
	
	if (!-e $selectFile) {
		#print $MainWindow::fhLogFile "Selection file $selectFile does not exist.\n";
		return(-1);
	}
	
	$sbi = stat($selectFile);
	my $in_time = $sbi->mtime;
	
	if ($in_time >= $lastrun) {
		# run the step
		#print $MainWindow::fhLogFile "Select file has changed since this step's last execution.\n";
		return(1);
	}

	# skip the step
	return(0);
}

=head1 AUTHOR

  � 2014 Steve Coward <steve-coward@comcast.net>
 *All rights reserved. No warranty, explicit or implicit, provided.
 In no event shall the author be liable for any claim or damages.

=cut

1;
