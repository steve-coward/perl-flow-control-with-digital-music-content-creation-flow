
=head1 NAME

=head1 SYNOPSIS

      use MusicFlow::bin::HDCDCheck;
      my $HDCDCheck = MusicFlow::bin::HDCDCheck->new();
      $HDCDCheck->RunStep;

=head1 DESCRIPTION

  This is an object-oriented library that checks a .wav or .flac file for
  the presence of HDCD encoding. If the encoding is present a 24-bit .flac file
  is generated, preserving the HDCD content.

=head2 Methods

=cut

package MusicFlow::bin::HDCDCheck;
use base MusicFlow::bin::Common;

use strict;
use warnings;
our $VERSION = "2.00";
use File::stat;
use File::Spec::Functions;
#use Tk::Dialog;
use Win32API::File 0.08 qw( :ALL );
use Win32::FindFile qw(wchar);
use Text::Unidecode;

my $opt_noAction = 1;

# The constructor of an object is called new() by convention.  Any
# method may construct an object and you can have as many as you like.
sub new {
	my($class, %args) = @_;
	my $self = bless({}, $class);
	return $self;
}

 
sub RunStep {
	my $self = shift;
	my $logFile = shift;
	my $trackinfo = shift;
	my $retval = 0;
	my $rval = 0;
	my $rvalarr;
	my $trackpath;
	my $artist;
	my $album;
	my $track;
	my $cmd;
	my $flacpath = $ENV{"Flac_Exe"};
	my @albumsprocessed;
	my $log;
	my $tid = threads->tid();

	print "INFO: Step HDCDCheck\n";
	if (defined $trackinfo) {
		# trackinfo contains path, artists, album, track
		($trackpath, $artist, $album, $track) = split(/::/, $trackinfo);
		my $albumpath = catfile($trackpath, $artist, $album);
		my $inflacwav16bitfile = catfile($trackpath, $artist, $album, $track);
		my $inwave16bitfile = $inflacwav16bitfile;
		$inwave16bitfile =~ s/.flac$/.wav/;
		my $cmd;
		my $output;
		my $convertedFromFlac = 0;
			
		if (($track =~ m/\.flac$/i) || ($track =~ m/\.wav$/i)) {
			$log .= "INFO: checking\n\t$trackpath $artist $album $track\n";
			push(@albumsprocessed, $album);
			
			if ($track =~ m/.flac$/) {
				# first convert flac to wave format
				my $flac2wav = catfile($ENV{"Comp_Dir"}, "flac2wav.pl");

				$cmd = "perl \"$flac2wav\" -output \"$inwave16bitfile\" -input \"$inflacwav16bitfile\"";
				print "Running $cmd\n";
				$log .= `$cmd 2>&1`;
				$rval = $?;
				#print $MainWindow::fhLogFile $output . "\n";
				if ($rval ne 0) {
					$log .= "ERROR: failed flac2wave conversion:\n\t$inflacwav16bitfile\n";
					$retval += $rval;
					DeleteFile($inwave16bitfile);
					goto EXIT;
				}
				$convertedFromFlac = 1;
			}
			
			# now we will have a wave file so
			# check wave file for hdcd content
			my $hdcd = $ENV{"HDCD_Exe"};
			$inwave16bitfile = $self->WindowizePath($inwave16bitfile);
			$DB::single = 1;
			$rvalarr = $self->runSystemCommand($hdcd, $inwave16bitfile, "", 0, "-i");
			$rval = $rvalarr->[1];
			$log .= $rvalarr->[0];
			
			if ($log =~ /expected 16 bits per sample/i) {
				$log .= "INFO: input file $inwave16bitfile is already in 24 bit format.\n";
			}
			elsif ($rval ne 0) {	
				$log .= "INFO: no hdcd content in:\n\t$inwave16bitfile\n";
			}
			elsif (!$opt_noAction) {
				#print $MainWindow::fhLogFile $output . "\n";
				$log .= "INFO: hdcd content in:\n\t$inwave16bitfile\n";
				
				# convert wave to 24 bit wave
				my $out24bitwavefile = $inwave16bitfile;
				$out24bitwavefile =~ s/.wav$/24bit.wav/;
				my $out24bitflacfile = $out24bitwavefile;
				$out24bitflacfile =~ s/.wav$/.flac/;
				$out24bitwavefile = $self->WindowizePath($out24bitwavefile);
				$out24bitflacfile = $self->WindowizePath($out24bitflacfile);
				$cmd = "\"$hdcd\" -o \"$out24bitwavefile\" \"$inwave16bitfile\"";
				$log .= `$cmd 2>&1`;
				$rval = $?;
				#print $MainWindow::fhLogFile $output . "\n";
				if ($rval ne 0) {
					$log .= "ERROR: failed hdcd conversion:\n\t$inwave16bitfile\n";
					$retval += $rval;
					DeleteFile($out24bitwavefile);
					if ($convertedFromFlac) {
						DeleteFile($inwave16bitfile);
					}
					goto EXIT;
				}

				# finally create 24 bit flac file
				$cmd = "\"$flacpath\" -f \"$out24bitwavefile\"";
				my $output = `$cmd 2>&1`;
				$rval = $?;
				#print $MainWindow::fhLogFile $output . "\n";
				if ($rval ne 0) {
					$log .= "ERROR: conversion to 24 bit flac failed:\n\t$out24bitwavefile\n";
					$retval += $rval;
					DeleteFile($out24bitwavefile);
					DeleteFile($out24bitflacfile);
					if ($convertedFromFlac) {
						# remove wave file that was generated from flac file
						DeleteFile($inwave16bitfile);
					}
					goto EXIT;
				}
				else {
					#print $MainWindow::fhLogFile "INFO: HDCD conversion completed successfully:\n\t$out24bitflacfile\n";
					DeleteFile($out24bitwavefile);
					DeleteFile($inflacwav16bitfile);
					if ($convertedFromFlac) {
						# remove wave file that was generated from flac file
						DeleteFile($inwave16bitfile);
					}
					my $outflacfile = $out24bitflacfile;
					$outflacfile =~ s/24bit//;
					if (MoveFile($out24bitflacfile, $outflacfile) == 0) {
						$log .= "ERROR: Move unsuccessful:\n\t$out24bitflacfile =>\n\t$outflacfile\n$!\n\t";
						$retval = 1<<8;
						goto EXIT;
					}
				}
			}
			else {
				#print $MainWindow::fhLogFile $output . "\n";
				#print $MainWindow::fhLogFile "INFO: HDCD conversion not performed for:\n\t$inwave16bitfile\n";
			}
			
			if ($convertedFromFlac == 1) {
				# we did a format conversion so delete wave file
				DeleteFile($inwave16bitfile);
			}
		}
	}

EXIT:
	#print "INFO: Step HDCDCheck exits with status $retval\n";
	$log .= "($tid HDCDCheck) exits with status $retval\n";
	my @retarray = ($tid, $log, $retval >> 8);
	
	return(\@retarray);
}
 
# return >0 if this step needs to run
# return 0  if this step does not need to run
# return <0 if this step can not run
sub CheckDependencies {
	my $self = shift;
	my $lastrun = shift;
	my $sbi;
	my $sbo;

	if (!defined $ENV{"Selection"}) {
		#print $MainWindow::fhLogFile "The environment variable \"Bin_Dir\" must be defined.\n";
		#print $MainWindow::fhLogFile "Exiting ...\n";
		exit(-1);
	}
	
	my $selectFile = $ENV{"Selection"};
	
	if (!-e $selectFile) {
		#print $MainWindow::fhLogFile "Selection file $selectFile does not exist.\n";
		return(-1);
	}
	
	$sbi = stat($selectFile);
	my $in_time = $sbi->mtime;
	
	if ($in_time >= $lastrun) {
		# run the step
		return(1);
	}

	# skip the step
	return(0);
}

sub GetOptions {
	my $self = shift;
	my %opthash;
	
	$opthash{"opt_noAction"} = $opt_noAction;
	
	return(\%opthash);
}
sub SetOptions {
	my $self = shift;
	my $optref = shift;
	my %opthash = %$optref;
	
	$opt_noAction = $opthash{"opt_noAction"};
}
sub QueryOptions {
	my $self = shift;
    my($ok) = ('OK');
	my($cancel) = ('Cancel');
    my $DIALOG;
	my $prevval = $opt_noAction;

	$DIALOG = $flow::MainWindow->Dialog(
	    -title          => 'Set HDCD Check Options',
        -default_button => $ok,
        -buttons        => [$ok, $cancel],
    );

    my $sb = $DIALOG->Checkbutton(
		-variable => \$opt_noAction,
		-relief => 'flat',
    )->pack(-side => 'left');
	$DIALOG->Label(-text => 'Select to disable HDCD format to 20-bit.')->pack(-side => 'left');
    
    my $button = $DIALOG->Show;
	
	if ($button eq $cancel) {
		$opt_noAction = $prevval;
	}
}
 
=head1 AUTHOR

  � 2014 Steve Coward <steve-coward@comcast.net>
 *All rights reserved. No warranty, explicit or implicit, provided.
 In no event shall the author be liable for any claim or damages.

=cut

1;

