
=head1 NAME

=head1 SYNOPSIS

      use MusicFlow::bin::ExtractTag;
      my $ExtractTag = MusicFlow::bin::ExtractTag->new();
      $ExtractTag->RunStep;

=head1 DESCRIPTION

  This is an object-oriented library that extracts tags from .mp3 or .flac files
  and writes them to a file named tags.text.

=head2 Methods

=cut

package MusicFlow::bin::ExtractTag;
use base MusicFlow::bin::Common;

use strict;
use warnings;
use utf8;
use File::stat;
use File::Spec::Functions qw(splitdir catfile catdir);
use Encode qw(is_utf8 encode decode);

our $VERSION = "2.00";

# Inherit from the "Exporter" module which handles exporting functions.
# Most procedural modules make use of this.
use base 'Exporter';

# When the module is invoked, export, by default, the function "hello" into 
# the namespace of the using code.
#our @EXPORT = qw(Wave2FLAC);

my $opt_verifyencode = 0;

# The constructor of an object is called new() by convention.  Any
# method may construct an object and you can have as many as you like.
sub new {
	my($class, %args) = @_;
	my $self = bless({}, $class);
	return $self;
}

sub RunStep {
	my $self = shift;
	my $logFile = shift;
	my %albuminfo;
	my $ripresults;
	my $printhdr = 1;
	my $lastdisc = 0;
	my $lasttrack = 0;
	my $trackCount = 0;
	my $allTracksPresent = 1;
	my $log;
	my $tid = threads->tid();
	
	my $artists; # everything stored here as $artists->{$artist}{$album}
	my $bNewAlbum;
	my $trackinfo;
	my $trackpath;
	my $artist = "";
	my $album = "";
	my $track;
	my $retval = 0;
		
	$log .= "Start file extracting\n\n";
	
	if (defined $trackinfo) {		
		# trackinfo contains path, artists, album, track
		my ($newtrackpath, $newartist, $newalbum, $newtrack) = split(/::/, $trackinfo);
		my $tagFile = catfile($trackpath, $artist, $album, "tags.text");
		
		$bNewAlbum = ($newartist ne $artist) || ($newalbum ne $album);
		
		$trackpath = $newtrackpath;
		$artist = $newartist;
		$album = $newalbum;
		$track = $newtrack;
		
		my $albumPath = catfile($trackpath, $artist, $album);
		
		if ($bNewAlbum) {
			if (keys %albuminfo) {
				if ($allTracksPresent && ($trackCount > 2)) {
					$albuminfo{hdr}{totaltracks} = $trackCount;
				}
				else {
					undef $albuminfo{hdr}{totaltracks};
				}
				$self->writeTagsFile($tagFile, \%albuminfo);
				%albuminfo = ();
			}
			my $EACLogDir = $albumPath;
			$ripresults = $self->readLogs($EACLogDir);
			
			my $tagfile = catfile($albumPath, "tags.text");
			$artists = $self->readTags($tagfile, $ripresults);
			
			$printhdr = 1;
			$trackCount = 0;
			$allTracksPresent = 1;
			$lastdisc = 0;
			$lasttrack = 0;
		}
				
		my $musicfile = catfile($albumPath, $track);
		my $artfile = catfile($albumPath, "Folder.jpg");
		my $xartist = $self->WinToName($artist);
		my $xalbum = $self->WinToName($album);
		
		if ($printhdr) {
			if ($musicfile =~ /.mp3/) {
				$albuminfo{hdr} = extractMP3Hdr($musicfile, $printhdr, $artists->{$xartist}{$xalbum}{tracks});
			}
			elsif ($musicfile =~ /.flac/) {
				$albuminfo{hdr} = extractFlacHdr($musicfile, $printhdr, $artists->{$xartist}{$xalbum}{tracks});
			}
		}
		# mp3 files store rip_quality in hdr
		if ($musicfile =~ /.mp3/) {
			my $trackhash = extractMP3Track($musicfile, $printhdr, $lastdisc, $lasttrack, $artists->{$xartist}{$xalbum}{tracks});
			for my $track (keys %$trackhash) {
				if (!defined $albuminfo{tracks}{$track}) {
					$albuminfo{tracks}{$track} = $trackhash->{$track};
				}
			}
			my $thistrack = $self->getTrackNum($musicfile);
			my $thisdisc = $self->getDiscNum($musicfile);
			if ((($thisdisc == $lastdisc) || ($lastdisc < 1)) && ($thistrack != $lasttrack+1)) {
				$allTracksPresent = 0;
			}
			$lastdisc = $thisdisc;
			$lasttrack = $thistrack;
		}
		elsif ($musicfile =~ /.flac/) {
			my $trackhash = extractFlacTrack($musicfile, $printhdr, $lastdisc, $lasttrack, $artists->{$xartist}{$xalbum}{tracks}, $ripresults);
			for my $track (keys %$trackhash) {
				$albuminfo{tracks}{$track} = $trackhash->{$track};
			}
			my $thistrack = $self->getTrackNum($musicfile);
			my $thisdisc = $self->getDiscNum($musicfile);
			if ((($thisdisc == $lastdisc) || ($lastdisc < 1)) && ($thistrack != $lasttrack+1)) {
				$allTracksPresent = 0;
			}
			$lastdisc = $thisdisc;
			$lasttrack = $thistrack;
		}
		$trackCount++;
		
		if (!defined $trackinfo) {
			if (keys %albuminfo) {
				if ($allTracksPresent && ($trackCount > 2)) {
					$albuminfo{hdr}{totaltracks} = $trackCount;
				}
				else {
					undef $albuminfo{hdr}{totaltracks};
				}
				my $tagFile = catfile($albumPath, "tags.text");
				$self->writeTagsFile($tagFile, \%albuminfo);
				%albuminfo = ();
			}
		}
	} # end while
	
EXIT:
	$log .= "($tid ExtractTag) exits with status $retval\n";
	my @retarray = ($tid, $log, $retval >> 8);
	
	return(\@retarray);
}


# return >0 if this step needs to run
# return 0  if this step does not need to run
# return <0 if this step can not run
sub CheckDependencies {
	my $self = shift;
	my $lastrun = shift;
	my $sbi;
	my $sbo;

	if (!defined $ENV{"Selection"}) {
		#print $MainWindow::fhLogFile "The environment variable \"Bin_Dir\" must be defined.\n";
		#print $MainWindow::fhLogFile "Exiting ...\n";
		exit(-1);
	}
	
	my $selectFile = $ENV{"Selection"};
	
	if (!-e $selectFile) {
		#print $MainWindow::fhLogFile "Selection file $selectFile does not exist.\n";
		return(-1);
	}
	
	$sbi = stat($selectFile);
	my $in_time = $sbi->mtime;
	
	if ($in_time >= $lastrun) {
		# run the step
		#print $MainWindow::fhLogFile "Select file has changed since this step's last execution.\n";
		return(1);
	}

	# skip the step
	return(0);
}

sub GetOptions {
	my $self = shift;
	my %opthash;
	
	#$opthash{"opt_minsize"} = $opt_minsize;
	
	return(\%opthash);
}
sub SetOptions {
	my $self = shift;
	my $optref = shift;
	my %opthash = %$optref;
	
	#$opt_minsize = $opthash{"opt_minsize"};
}
sub QueryOptions {
	my $self = shift;
	print $MainWindow::fhLogFile "Set some options in ExtractTag.\n";
}

sub extractMP3Hdr
{
	my $self = shift;
	my $musicfile = shift;
	my $printhdr = shift;
	my $tracks = shift;
	
	print $MainWindow::fhLogFile "Extracting tags for $musicfile\n";

	my %trackinfo;
	$musicfile =~ /\/(\d+)-(\d\d) /;
	
	my $mp3 = MP3::Tag->new($musicfile);
	if (!defined $mp3) {
		print $MainWindow::fhLogFile "mp3 undefined - Skipping " . $musicfile . "\n";
		return(undef);
	}
	$mp3->get_tags();
	my $id3v2 = $mp3->{ID3v2};
	if (defined $id3v2) {
		my $frameIDs_hash = $id3v2->get_frame_ids('truename');

		my @info;
		my $tagArtist;
		my $tagAlbum;
		my $tagTotalTracks;
		my $tagReleased;
		my $tagLabel;
		my $tagGenre;
		my $tagRecordingDate;
		my $tagRipQuality;
		my $tagProducer;
		my $tagSource;
		my $tagTotalDiscs;
		
		if ($printhdr) {
			$printhdr = 0;
			($tagArtist, @info) = $id3v2->get_frame('TPE1');
			$trackinfo{artist} = $tagArtist;
			($tagAlbum, @info) = $id3v2->get_frame('TALB');
			$trackinfo{album} = $tagAlbum;
		
			@info = $id3v2->get_frame('TXXX');
			for my $info (@info) {  
				if (ref $info) {
					my $mode;	
					while(my ($key,$val) = each %$info) {
						if ($val =~ /source/i) {
							$mode = "source";
						}
						elsif ($val =~ /TOTALTRACKS/i) {
							$mode = "totaltracks";
						}
						elsif ($val =~ /RIP_QUALITY/i) {
							$mode = "rip_quality";
						}
						elsif ($val =~ /recordingdate/i) {
							$mode = "recordingdate";
						}
						elsif ($val =~ /producer/i) {
							$mode = "producer";
						}
						elsif ($val =~ /year/i) {
							$mode = "year";
						}
						elsif ((defined $mode) && ($key =~ /Text/)) {
							if ($mode =~ /source/) {
								$trackinfo{source} = $val;
								$tagSource = $val;
							}
							elsif ($mode =~ /totaltracks/) {
								$trackinfo{totaltracks} = $val;
								$tagTotalTracks = $val;
							}
							elsif ($mode =~ /rip_quality/) {
								$trackinfo{quality} = $val;
								$tagRipQuality = $val;
							}
							elsif ($mode =~ /recordingdate/) {
								$trackinfo{recordingdate} = $val;
								$tagRecordingDate = $val;
							}
							elsif ($mode =~ /producer/) {
								$trackinfo{producer} = $val;
								$tagProducer = $val;
							}
							elsif ($mode =~ /year/) {
								$trackinfo{year} = $val;
								$tagReleased = $val;
							}
						}
					}
				}
			}
			if (!defined $tagReleased) {
				($tagReleased, @info) = $id3v2->get_frame('TYER');
			}
			if (!defined $tagReleased) {
				($tagReleased, @info) = $id3v2->get_frame('YEAR');
			}
			if (!defined $tagReleased) {
				# even though TDRC generally used for recording time
				($tagReleased, @info) = $id3v2->get_frame('TDRC');
			}
			$trackinfo{releasedate} = $tagReleased;
			($tagLabel, @info) = $id3v2->get_frame('TPUB');
			$trackinfo{publisher} = $tagLabel;
			($tagGenre, @info) = $id3v2->get_frame('TCON');
			$trackinfo{genre} = $tagGenre;
			($tagTotalDiscs, @info) = $id3v2->get_frame('TPOS');
			if (defined $tagTotalDiscs) {
				$tagTotalDiscs =~ s/\d\///;
				$trackinfo{totaldiscs} = $tagTotalDiscs;
			}
			else {
				$trackinfo{totaldiscs} = 1;
			}
		}
	}
	$mp3->close();
	
	return(\%trackinfo);
}

sub extractFlacHdr
{
	my $self = shift;
	my $musicfile = shift @_;
	my $printhdr = shift @_;
	my $tracks = shift @_;
	
	print $MainWindow::fhLogFile "Extracting tags for $musicfile\n";
	
	my %trackinfo;
	$musicfile =~ /\/(\d+)-(\d\d) /;
	
	my $flac = Audio::FLAC::Header->new("$musicfile");
	if (!defined $flac) {
		print $MainWindow::fhLogFile "flac undefined - Skipping " . $musicfile . "\n";
		return(undef);
	}
	my $info = $flac->info();
	my $tags = $flac->tags();
	my $tagArtist;
	my $tagAlbum;
	my $tagTotalTracks;
	my $tagReleased;
	my $tagLabel;
	my $tagGenre;
	my $tagRecordingDate;
	my $tagProducer;
	my $tagSource;
	my $tagTotalDiscs;

	if ($printhdr) {
		$printhdr = 0;
		$tagArtist = $tags->{ARTIST};
		if (!defined $tagArtist) {
			$tagArtist = $tags->{artist};
		}
		if (!defined $tagArtist) {
			$tagArtist = $tags->{Artist};
		}
		$tagArtist = decode( 'utf8', $tagArtist );
		$trackinfo{artist} = $tagArtist;
		$tagAlbum = $tags->{ALBUM};
		if (!defined $tagAlbum) {
			$tagAlbum = $tags->{album};
		}
		if (!defined $tagAlbum) {
			$tagAlbum = $tags->{Album};
		}
		$tagAlbum = decode( 'utf8', $tagAlbum );
		$trackinfo{album} = $tagAlbum;
		
		$tagSource = $tags->{SOURCE};
		if (!defined $tagSource) {
			$tagSource = $tags->{source};
		}
		if (!defined $tagSource) {
			$tagSource = $tags->{Source};
		}
		$trackinfo{source} = $tagSource;
		$tagProducer = $tags->{PRODUCER};
		if (!defined $tagProducer) {
			$tagProducer = $tags->{producer};
		}
		if (!defined $tagProducer) {
			$tagProducer = $tags->{Producer};
		}
		$tagProducer = decode( 'utf8', $tagProducer );
		$trackinfo{producer} = $tagProducer;
		$tagTotalTracks = $tags->{TOTALTRACKS};
		if (!defined $tagTotalTracks) {
			$tagTotalTracks = $tags->{totaltracks};
		}
		if (!defined $tagTotalTracks) {
			$tagTotalTracks = $tags->{Totaltracks};
		}
		$trackinfo{totaltracks} = $tagTotalTracks;
		$tagRecordingDate = $tags->{RECORDINGDATES};
		if (!defined $tagRecordingDate) {
			$tagRecordingDate = $tags->{recordingdates};
		}
		if (!defined $tagRecordingDate) {
			$tagRecordingDate = $tags->{Recordingdates};
		}
		$trackinfo{recordingdate} = $tagRecordingDate;
		$tagTotalDiscs = $tags->{DISCNUMBER};
		if (!defined $tagTotalDiscs) {
			$tagTotalDiscs = $tags->{discnumber};
		}
		if (!defined $tagTotalDiscs) {
			$tagTotalDiscs = $tags->{Discnumber};
		}
		if (defined $tagTotalDiscs) {
			$tagTotalDiscs =~ s/\d+\///;
			$trackinfo{totaldiscs} = $tagTotalDiscs;
		}
		$tagReleased = $tags->{DATE};
		if (!defined $tagReleased) {
			$tagReleased = $tags->{date};
		}
		if (!defined $tagReleased) {
			$tagReleased = $tags->{Date};
		}
		$trackinfo{releasedate} = $tagReleased;
		$tagLabel = $tags->{ORGANIZATION};
		if (!defined $tagLabel) {
			$tagLabel = $tags->{organization};
		}
		if (!defined $tagLabel) {
			$tagLabel = $tags->{Organization};
		}
		if (!defined $tagLabel) {
			$tagLabel = $tags->{PUBLISHER};
		}
		if (!defined $tagLabel) {
			$tagLabel = $tags->{publisher};
		}
		if (!defined $tagLabel) {
			$tagLabel = $tags->{Publisher};
		}
		$tagLabel = decode( 'utf8', $tagLabel );
		$trackinfo{publisher} = $tagLabel;
		$tagGenre = $tags->{GENRE};
		if (!defined $tagGenre) {
			$tagGenre = $tags->{genre};
		}
		if (!defined $tagGenre) {
			$tagGenre = $tags->{Genre};
		}
		$trackinfo{genre} = $tagGenre;
	}
	
	return(\%trackinfo);
}


sub extractMP3Track
{
	my $self = shift;
	my $musicfile = shift;
	my $printhdr = shift;
	my $lastdisc = shift;
	my $lasttrack = shift;
	my $tracks = shift;
	
	#my @retInfo;
	my %trackinfo;
	my $currtrack = $self->getTrackNum($musicfile);
	my $currdisc = $self->getDiscNum($musicfile);
	
	my $mp3 = MP3::Tag->new($musicfile);
	print $MainWindow::fhLogFile "Extracting tags for $musicfile\n";
	if (!defined $mp3) {
		print $MainWindow::fhLogFile "mp3 undefined - Skipping " . $musicfile . "\n";
		return(undef);
	}
	$mp3->get_tags();
	my $id3v2 = $mp3->{ID3v2};
	if (defined $id3v2) {
		my $frameIDs_hash = $id3v2->get_frame_ids('truename');

		my $trackText;
		my @info;
		my $tagComposer;
		my $tagDisc;
		my $tagTrack;
		my $tagBand;
		my $tagComment;
		my $tagTotalDiscs;
		my $tagTrackNum;
		
		(@info, my $name) = $id3v2->get_frame('COMM');
		for my $info (@info) {  
			if (ref $info) {
				my $mode;	
				while(my ($key,$val) = each %$info) {
					if ($val =~ /^$/i) {
						$mode = "comment";
					}
					elsif ((defined $mode) && ($key =~ /Text/)) {
						if ($mode =~ /comment/) {
							$tagComment = $val;
						}
					}
				}
			}
		}
			
		($tagDisc, @info) = $id3v2->get_frame('TPOS');
		if (defined $tagDisc) {
			$tagDisc =~ s/\/\d//;
		}
		else {
			$tagDisc = 1;
		}
		
		($tagTrackNum, @info) = $id3v2->get_frame('TRCK');
		if (!defined $tagTrackNum) {
			$musicfile =~ /\/(\d+)-(\d+)/;
			$tagTrackNum = $2;
			$tagTrackNum =~ s/^0//;
		}
		else {
			$tagTrackNum =~ s/\/\d+//;
		}
		$tagTrackNum = $self->FormatTrackNumber($tagTrackNum);
		
		while ($self->FormatTrackNumber($currtrack) ne $tagTrackNum) {
			my @alltracks;
			for my $t (keys %{$tracks}) {
				push(@alltracks, $t);
			}
			if (scalar(@alltracks) > 0) {
				my $strTrack = $self->FormatTrackNumber(${currtrack});
				my $currdisc = 1;
				my @thistrack = grep(/^${currdisc}-$strTrack /, @alltracks);
				if (scalar(@thistrack)) {
					$tagComposer = $tracks->{$thistrack[0]}{composer};
					if (defined $tagComposer) {
					}
					$tagComment = $tracks->{$thistrack[0]}{comment};
					if (defined $tagComment) {
						$tagComment = $self->trim($tagComment);
						$tagComment =~ s/;$//;
					}
				}
				else {
					last;
				}
			}
			$currtrack++;
		}
		if ($self->FormatTrackNumber($currtrack) eq $tagTrackNum) {
			($tagTrack, @info) = $id3v2->get_frame('TIT2');
			$tagTrack =~ s/\s*\/\s*/\//g;
			if ($tagTrack =~ /^\d-\d\d /) {
				$trackText = "\"$tagTrack\"";
			}
			else {
				$trackText = "\"${tagDisc}-${tagTrackNum} $tagTrack\"";
			}
			
			($tagComposer, @info) = $id3v2->get_frame('TCOM');
			if (defined $tagComposer) {
				$trackText .= " ($tagComposer)";
			}
			if (defined $tagComment) {
				$tagComment = $self->trim($tagComment);
				$tagComment =~ s/;$//;
				$trackText .= " <$tagComment>";
			}
			$trackText .= "\n";
			($tagBand, @info) = $id3v2->get_frame('TPE2');
			if (defined $tagBand) {
				$tagBand = $self->trim($tagBand);
				$tagBand =~ s/\s*([\\\/]+)\s*/$1/g;
				my @mems = split(/[\\\/]+/, $tagBand);
				foreach my $mem (sort @mems) {
					$mem =~ s/\.$//;
					$trackText .= $self->trim($mem) . "\n";
				}
			}
			$trackinfo{100*$currdisc+$currtrack} = $trackText;
		}
	}
	$mp3->close();
	
	#push(@retInfo, $currtrack);
	#push(@retInfo, \%trackinfo);
	return(\%trackinfo);
}


sub extractFlacTrack
{
	my $self = shift;
	my $musicfile = shift;
	my $printhdr = shift;
	my $lastdisc = shift;
	my $lasttrack = shift;
	my $tracks = shift;
	my $ripresults = shift;

	#my @retInfo;
	my %trackinfo;
	my $currtrack = $self->getTrackNum($musicfile);
	my $currdisc = $self->getDiscNum($musicfile);
	
	my $flac = Audio::FLAC::Header->new("$musicfile");
	print $MainWindow::fhLogFile "Extracting tags for $musicfile\n";
	if (!defined $flac) {
		print $MainWindow::fhLogFile "flac undefined - Skipping " . $musicfile . "\n";
		return(undef);
	}
	my $info = $flac->info();
	my $tags = $flac->tags();
	
	my $trackText;
	my $tagComposer;
	my $tagDisc;
	my $tagTrack;
	my $tagBand;
	my $tagComment;
	my $tagTotalDiscs;
	my $tagTrackNum;
	my $tagRipQuality;

	$tagTrack = $tags->{TITLE};
	if (!defined $tagTrack) {
		$tagTrack = $tags->{title};
	}
	if (!defined $tagTrack) {
		$tagTrack = $tags->{Title};
	}
	if (!defined $tagTrack) {
		my @fields = splitdir($musicfile);
		$tagTrack = pop @fields;
		$tagTrack =~ s/\.flac//;
		$tagTrack =~ s/^\d+-\d+ //;
	}
	$tagTrackNum = $tags->{Tracknumber};
	if (!defined $tagTrackNum) {
		$tagTrackNum = $tags->{TRACKNUMBER};
	}
	if (!defined $tagTrackNum) {
		$musicfile =~ /\/(\d+)-(\d+)/;
		$tagTrackNum = $2;
		$tagTrackNum =~ s/^0//;
	}
	else {
		$tagTrackNum =~ s/\/\d+//;
		$tagTrackNum = $self->FormatTrackNumber($tagTrackNum);
	}
	
	$tagDisc = $tags->{DISCNUMBER};
	if (!defined $tagDisc) {
		$tagDisc = $tags->{discnumber};
	}
	if (!defined $tagDisc) {
		$tagDisc = $tags->{Discnumber};
	}
	if (defined $tagDisc) {
		$tagDisc =~ s/\/\d//;
	}
	else {
		$tagDisc = 1;
	}
	
	$tagRipQuality = $tags->{RIP_QUALITY};
	if (!defined $tagRipQuality) {
		$tagRipQuality = $tags->{rip_quality};
	}
	if (!defined $tagRipQuality) {
		$tagRipQuality = $tags->{Rip_quality};
	}
	if (!defined $tagTotalDiscs) {
		$tagTotalDiscs = 1;
	}
	
	my $ripresultsfound = 0;
	
	foreach my $d (1 ... $tagTotalDiscs) {
		my $riptrack;
		if (($tagTrack =~ /^(\d+)-\d+ /) && ($1 < 13)) {
			$riptrack = $self->simplifyString($tagTrack, 1);
		}
		else {
			$riptrack = $self->simplifyString("${tagDisc}-${tagTrackNum} " . $tagTrack, 1);
		}
		$riptrack =~ s/^\d(\d+)/$d$1/;
		
		if (defined $ripresults->{$riptrack}) {
			$tagRipQuality = $ripresults->{$riptrack};
			$ripresultsfound = 1;
		}
	}
	if ($ripresultsfound == 0) {
		foreach my $d (1 ... $tagTotalDiscs) {
			my $riptrack;
			if ($tagTrack =~ /^\d+-\d+ /) {
				$riptrack = $self->simplifyString($tagTrack, 1);
			}
			else {
				$riptrack = $self->simplifyString("${tagDisc}-${tagTrackNum} " . $tagTrack, 1);
			}
			$riptrack =~ s/^\d(\d+)/$d$1/;
		
			if (defined $ripresults->{$riptrack}) {
				$tagRipQuality = $ripresults->{$riptrack};
				$ripresultsfound = 1;
			}
		}
	}
	if ($ripresultsfound == 0) {
		print "Unable to locate rip results for $musicfile\n";
		print $MainWindow::fhLogFile "Unable to locate rip results for $musicfile\n";
	}
	
	if (defined $tagTrack) {
		$tagTrack =~ s/\s*\/\s*/\//g;
		$tagTrack = decode( 'utf8', $tagTrack );
		if ($tagTrack =~ /^\d-\d\d /) {
			$trackText = "\"$tagTrack\"";
		}
		else {
			$trackText = "\"${tagDisc}-${tagTrackNum} $tagTrack\"";
		}

		$tagComment = $tags->{COMMENT};
		if (!defined $tagComment) {
			$tagComment = $tags->{comment};
		}
		if (!defined $tagComment) {
			$tagComment = $tags->{Comment};
		}
		$tagComposer = $tags->{COMPOSER};
		if (!defined $tagComposer) {
			$tagComposer = $tags->{composer};
		}
		if (!defined $tagComposer) {
			$tagComposer = $tags->{Composer};
		}
		if (defined $tagComposer) {
			$tagComposer = decode( 'utf8', $tagComposer );
			$trackText .= " ($tagComposer)";
		}
		if (defined $tagComment) {
			if (ref $tagComment) {
				if ($tagComment ne "") {
					$tagComment->[0] = decode( 'utf8', $tagComment->[0] );
					$tagComment->[0] = $self->trim($tagComment->[0]);
					$tagComment->[0] =~ s/;$//;
					$trackText .= " <$tagComment->[0]>";
				}
			}
			else {
				if ($tagComment ne "") {
					$tagComment = decode( 'utf8', $tagComment );
					$tagComment = $self->trim($tagComment);
					$tagComment =~ s/;$//;
					$trackText .= " <$tagComment>";
				}
			}
		}
		$trackText .= "\n";
		if (defined $tagRipQuality) {
			$trackText .= "Rip Quality $tagRipQuality\n";
		}
		$tagBand = $tags->{BAND};
		if (!defined $tagBand) {
			$tagBand = $tags->{band};
		}
		if (!defined $tagBand) {
			$tagBand = $tags->{Band};
		}
		if (defined $tagBand) {
			if (ref $tagBand) {
				my %seen = ();
				my @uniqueBand = grep { ! $seen{ $_ }++ } @$tagBand;
				foreach my $mems (sort @uniqueBand) {
					$mems =~ s/\s*([\\\/]+)\s*/$1/g;
					my @mems = split(/[\\\/]+/, $mems);
					my %seen = ();
					my @uniqueMems = grep { ! $seen{ $_ }++ } @mems;
					foreach my $mem (sort @uniqueMems) {
						$mem = decode( 'utf8', $mem );
						$mem =~ s/\.$//;
						$trackText .= $self->trim($mem) . "\n";
					}
				}
			}
			else {
				$tagBand =~ s/\s*([\\\/]+)\s*/$1/g;
				my @mems = split(/[\\\/]+/, $tagBand);
				my %seen = ();
				my @uniqueMems = grep { ! $seen{ $_ }++ } @mems;
				foreach my $mem (sort @uniqueMems) {
					$mem = decode( 'utf8', $mem );
					$mem =~ s/\.$//;
					$trackText .= $self->trim($mem) . "\n";
				}
			}
		}
		
		$trackinfo{100*$currdisc+$currtrack} = $trackText;
	}
	
	#push(@retInfo, $currtrack);
	#push(@retInfo, \%trackinfo);
	return(\%trackinfo);
}

=head1 AUTHOR

  � 2014 Steve Coward <steve-coward@comcast.net>
 *All rights reserved. No warranty, explicit or implicit, provided.
 In no event shall the author be liable for any claim or damages.

=cut

1;

