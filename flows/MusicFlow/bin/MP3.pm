
=head1 NAME

=head1 SYNOPSIS

      use MusicFlow::bin::MP3;
      my $MP3 = MusicFlow::bin::MP3->new();
      $MP3->RunStep;

=head1 DESCRIPTION

  This is an object-oriented library that prompts performs
  .wav or .flac to .mp3 file conversion.

=head2 Methods

=cut

package MusicFlow::bin::MP3;
use base MusicFlow::bin::Common;

use strict;
use warnings;
use File::stat;
use File::Spec::Functions;
#use Tk::Dialog;
use MP3::Tag;
use MP3::Info;
use Encode;
use Win32API::File 0.08 qw( :ALL );
#use Win32::FindFile qw(wchar);
use Time::HiRes qw(usleep);

our $VERSION = "2.00";
 
#my $opt_bitrate = 320;
#my $opt_quality = "q0";

# The constructor of an object is called new() by convention.  Any
# method may construct an object and you can have as many as you like.
sub new {
	my($class, %args) = @_;
	my $self = bless({}, $class);

	$self->{Log} = "Step MP3 created\n";
	# call a method on $self to set an initial value
	if (defined $args{name})
	{
		#$self->set_name($args{name});
		# alternatively we can set the name using
		$self->{NAME} = $args{name};
	} else {
		die "Please specify a name for the $class instance.";
	}

	return $self;
}
   
sub RunStep {
	my $self = shift;
	my $trackinfo = shift;
	my $retval = 0;
	my $rval;
	my $trackpath;
	my $artist;
	my $album;
	my $track;
	my $flacpath = $ENV{"Flac_Exe"};
	my $lamepath = $ENV{"Lame_Exe"};
	my $log;
	my $tid = threads->tid();
		
	$flacpath =~ s/flac.exe/flac${tid}.exe/;
	$lamepath =~ s/lame/lame$tid/;
	#usleep(7500);
	#sleep(20);
	#undef $trackinfo;
	if (defined $trackinfo) {
		# trackinfo contains path, artists, album, track
		($trackpath, $artist, $album, $track) = split(/::/, $trackinfo);
		my $musicfile = catfile($trackpath, $artist, $album, $track);

		$log .= "($tid MP3) RunStep $track\n";
		$log .= "($tid MP3) operating on $musicfile\n";
		
		#if ($track !~ /2-12/) {
		#	my @retarray = ($tid, $log, 0);
		#	return(\@retarray);
		#	#$DB::single = 1;
		#}
		if ($track =~ m/\.wav$/i) {
			$log .= "($tid MP3) convert $track to flac before converting to mp3\n";
		}
		elsif ($track =~ m/\.flac$/i) {
			$log .= "($tid MP3) converting flac to mp3 $trackpath $artist $album $track\n";

			my $musicwavefile = $musicfile;
			$musicwavefile =~ s/\.flac$/.wav/;
			my $musicmp3file = $musicfile;
			$musicmp3file =~ s/.flac$/$self->{BITRATE}_$self->{QUALITY}.mp3/;
			
			$log .= "($tid MP3) to wav\n";
			$rval = $self->runSystemCommand($flacpath, $musicfile, "", 0, "-ds");
			$log .= "($tid MP3) " . $rval->[0] . "\n";
			$retval += $rval->[1];
			
			if ($rval->[1] == 0) {
				$log .= "($tid MP3) to mp3\n";
				$rval = $self->runSystemCommand($lamepath, $musicwavefile, $musicmp3file, 0, "--silent", "-b $self->{BITRATE}", "-q $self->{QUALITY}", "--resample 44100");
				$log .= "($tid MP3) " . $rval->[0] . "\n";
				$retval += $rval->[1];
				$log .= "($tid MP3) done\n";
			}
			
			if ($rval->[1] != 0) {
				$log .= "($tid MP3) ERROR: $musicfile conversion to mp3 failed (exit code: $rval->[1])\n";
				$retval += $rval->[1];
				DeleteFile($musicwavefile);
				DeleteFile($musicmp3file);
			}
			else {
				DeleteFile($musicwavefile);
			}
		}
		elsif ($track =~ m/\.mp3$/i) {
			# create new MP3-Tag object			
			my $musicwavfile = $musicfile;
			$musicwavfile =~ s/.mp3$/.wav/;
			my $musicflacfile = $musicfile;
			$musicflacfile =~ s/.mp3$/.flac/;
			if (!-e $musicflacfile && !-e $musicwavfile) {
				# no better resolution source exists
				my $musicmp3file = $musicfile;
				$musicmp3file =~ s/.mp3$/$self->{BITRATE}_$self->{QUALITY}.mp3/;					
				my $fileinfo = get_mp3info($musicfile);
				my $mp3bitrate = 0;
				if (exists($fileinfo->{BITRATE})) {
					$mp3bitrate = $fileinfo->{BITRATE};
				}
				
				$log .= "($tid MP3) starting mp3 bitrate is " . $mp3bitrate;
				
				if ($mp3bitrate > $self->{BITRATE}) {
					# only makes sense to reduce bitrate
					$log .= "($tid MP3) converting mp3 to mp3 $trackpath $artist $album $track\n";
	
					$rval = $self->runSystemCommand($lamepath, $musicfile, $musicmp3file, 0, "--silent", "-b $self->{BITRATE}", "-q $self->{QUALITY}");
					$log .= "($tid MP3) " . $rval->[0] . "\n";
					$retval += $rval->[1];
	
					if ($rval->[1] != 0) {
						$log .= "($tid MP3) ERROR: $musicfile conversion to mp3 failed (exit code: $rval->[1])\n";
						DeleteFile($musicmp3file);
					}
				}
			}
		}
	}

	$log .= "($tid MP3) exits with status $retval\n";
	my @retarray = ($tid, $log, $retval >> 8);
	
	return(\@retarray);
}
	
sub GetOptions {
	my $self = shift;
	my %opthash;
	
	$opthash{"opt_bitrate"} = $self->{BITRATE};
	$opthash{"opt_quality"} = $self->{QUALITY};
	
	return(\%opthash);
}
sub SetOptions {
	my $self = shift;
	my $optref = shift;
	my %opthash = %$optref;
	
	$self->{BITRATE} = $opthash{"opt_bitrate"};
	$self->{QUALITY} = $opthash{"opt_quality"};
}
sub QueryOptions {
	my $self = shift;
	my($ok) = ('OK');
	my($cancel) = ('Cancel');
    my $DIALOG;
	my $prevval1 = $self->{BITRATE};
	my $prevval2 = $self->{QUALITY};
	
	die "Please call this method on an object instance." if (! ref $self);

	print STDOUT "Starting bitrate is " . $self->{BITRATE} .  "\n";

	$DIALOG = $flow::MainWindow->Dialog(
	    -title          => "Set MP3 encoding Options for $self->{NAME}",
        -default_button => $ok,
        -buttons        => [$ok, $cancel],
    );

	$DIALOG->Label(-text => 'Select a bitrate.')->pack(-side => 'left');
	my $list1 = $DIALOG->Scrolled("Listbox", -setgrid=>1, -height=>6, -width=>6, -selectmode=>"single", -exportselection=>0, -activestyle=>"dotbox");
    $list1->pack(qw/-side left -expand yes -fill both/);
    $list1->focus;
    
	my @bitrates = (320, 256, 192, 160, 128);
	$list1->insert(0, qw/320 256 192 160 128/);
	my( $index1 )= grep { $bitrates[$_] eq $self->{BITRATE} } 0..$#bitrates;
	$list1->selectionSet($index1);
	$list1->activate(0);
				  
	$DIALOG->Label(-text => 'Select an encoding quality.')->pack(-side => 'left');
	my $list2 = $DIALOG->Scrolled("Listbox", -setgrid=>1, -height=>6, -width=>4, -selectmode=>"single", -exportselection=>0, -activestyle=>"dotbox");
    $list2->pack(qw/-side left -expand yes -fill both/);
    $list2->focus;

    my @qualities = ('q0', 'q1', 'q2', 'q5', 'q7', 'q9');
	$list2->insert(0, qw/q0 q1 q2 q5 q7 q9/);
	my( $index2 )= grep { $qualities[$_] eq $self->{QUALITY} } 0..$#qualities;
	$list2->selectionSet($index2);
	$list2->activate(0);
	
	my $button = $DIALOG->Show;
	
	if ($button eq $ok) {
		my @sellist;
		@sellist = $list1->curselection();
		if (@sellist > 0) {
			my $opt_bitrate = shift @sellist;
			$self->{BITRATE} = $bitrates[$opt_bitrate];
		}
		@sellist = $list2->curselection();
		if (@sellist > 0) {
			my $opt_quality = shift @sellist;
			$self->{QUALITY} = $qualities[$opt_quality];
		}
	}
}
  
# return >0 if this step needs to run
# return 0  if this step does not need to run
# return <0 if this step can not run
sub CheckDependencies {
	my $self = shift;
	my $lastrun = shift;
	my $sbi;
	my $sbo;

	if (!defined $ENV{"Selection"}) {
		#print $MainWindow::fhLogFile "The environment variable \"Bin_Dir\" must be defined.\n";
		#print $MainWindow::fhLogFile "Exiting ...\n";
		exit(-1);
	}
	
	my $selectFile = $ENV{"Selection"};
	
	if (!-e $selectFile) {
		#print $MainWindow::fhLogFile "Selection file $selectFile does not exist.\n";
		return(-1);
	}
	
	$sbi = stat($selectFile);
	my $in_time = $sbi->mtime;
	
	if ($in_time >= $lastrun) {
		# run the step
		#print $MainWindow::fhLogFile "Select file has changed since this step's last execution.\n";
		return(1);
	}

	# skip the step
	return(0);
}

=head1 AUTHOR

  � 2014 Steve Coward <steve-coward@comcast.net>
 *All rights reserved. No warranty, explicit or implicit, provided.
 In no event shall the author be liable for any claim or damages.

=cut

1;
