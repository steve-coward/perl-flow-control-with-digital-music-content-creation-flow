
=head1 NAME

=head1 SYNOPSIS

	  use MusicFlow::bin::WriteDB;
	  my $Tag = MusicFlow::bin::WriteDB->new();
	  WriteDB->RunStep;

=head1 DESCRIPTION

  This is an object-oriented library that writes music file tag informatin to
  a sql database.

=head2 Methods

=cut

package MusicFlow::bin::WriteDB;
use base MusicFlow::bin::Common;

use strict;
use warnings;
use utf8;
use File::stat;
use File::Spec::Functions;
#use Tk::Dialog;
use MP3::Tag;
use MP3::Info;
#use LWP::Simple;
use LWP;
use HTML::Parser;
require HTML::TokeParser::Simple;
use Encode qw(is_utf8 encode decode);
use DBI;

# first bit of METADATA_BLOCK_HEADER: '1' if this block is the last
#  metadata block before the audio blocks, '0' otherwise.
my $last_block_flag = 0x80000000;
# bits 1-7 specify BLOCK_TYPE:
# 0 : STREAMINFO 
my $STREAMINFO=0;
# 1 : PADDING 
my $PADDING=1;
# 2 : APPLICATION 
my $APPLICATION=2;
# 3 : SEEKTABLE 
my $SEEKTABLE=3;
# 4 : VORBIS_COMMENT 
my $VORBIS_COMMENT=4;
# 5 : CUESHEET 
my $CUESHEET=5;
# 6 : PICTURE 
# 7-126 : reserved 
# 127 : invalid, to avoid confusion with a frame sync code 
my $block_type_flag = 0x7f000000;

# last 24 bits are Length (in bytes) of metadata to follow
#  (does not include the size of the METADATA_BLOCK_HEADER)
my $m_len_flag = 0x00ffffff;

my $keyInfoDuration = "Duration";
my $keyInfoSampleRate = "SampleRate";
my $keyInfoChannels = "Channels";
my $keyInfoBitsPerSample = "BitsPerSample";
my $keyInfoTotalSamples = "TotalSamples";


my $opt_append = 1;
my $sthArtistInsert;
my $sthAlbumInsert;
my $sthSongInsert;
my $sthSongDelete;

our $VERSION = "2.00";

# The constructor of an object is called new() by convention.	Any
# method may construct an object and you can have as many as you like.
sub new {
	my($class, %args) = @_;
	my $self = bless({}, $class);
	return $self;
}

sub addArtistToDB {
	my $self = shift;
	my $dbh = shift @_;
	my $artist = shift @_;
	my $artistID = shift @_;
	
	$sthArtistInsert->execute($artistID, $artist);
}
sub addAlbumToDB {
	my $self = shift;
	my $dbh = shift @_;
	my $album = shift @_;
	my $hrTrack = shift @_;
	my $artistID = shift @_;
	my $albumID = shift @_;
	
	my $artistutf8 = "";
	if (defined $hrTrack->{artist}) {
		$artistutf8 = $hrTrack->{artist};
	}		
	my $albumutf8 = "";
	if (defined $hrTrack->{album}) {
		$albumutf8 = $hrTrack->{album};
	}		
	my $genre = "";
	if (defined $hrTrack->{genre}) {
		$genre = $hrTrack->{genre};
	}
	my $releasedate = "";
	if (defined $hrTrack->{releasedate}) {
		$releasedate = $hrTrack->{releasedate};
	}
	my $totaltracks = "";
	if (defined $hrTrack->{totaltracks}) {
		$totaltracks = $hrTrack->{totaltracks};
	}
	my $producer = "";
	if (defined $hrTrack->{producer}) {
		$producer = $hrTrack->{producer};
	}
	my $label = "";
	if (defined $hrTrack->{label}) {
		$label = $hrTrack->{label};
	}
	my $encoding = "";
	if (defined $hrTrack->{encoding}) {
		$encoding = $hrTrack->{encoding};
	}
	my $source = "";
	if (defined $hrTrack->{source}) {
		$source = $hrTrack->{source};
	}
	my $samplerate = "";
	if (defined $hrTrack->{samplerate}) {
		$samplerate = $hrTrack->{samplerate};
	}
	my $bitrate = "";
	if (defined $hrTrack->{bitrate}) {
		$bitrate = $hrTrack->{bitrate};
	}

	$sthAlbumInsert->execute(
		$albumID, $albumutf8, $artistutf8, $genre, $producer, $releasedate,
		$encoding, $source, $label, $totaltracks, $samplerate,
		$bitrate, $artistID);
}
sub addSongToDB {
	my $self = shift;
	my $dbh = shift @_;
	my $hrTrack = shift @_;
	my $albumID = shift @_;
	my $songID = shift @_;
	
	my $artist = "";
	if (defined $hrTrack->{artist}) {
		$artist = $hrTrack->{artist};
	}		
	my $albumtitle = "";
	if (defined $hrTrack->{album}) {
		#my $t = decode('UTF-8', $hrTrack->{album});
		#$cmd = $cmd . "-u album=\"$t\" ";
		$albumtitle = $hrTrack->{album};
	}
	my $songtitle = "";
	if (defined $hrTrack->{title}) {
		$songtitle = $hrTrack->{title};
		$songtitle =~ s/\$/\\\$/g;
		$songtitle =~ s/^\d-\d\d //;
	}
	my $disc = 1;
	if (defined $hrTrack->{disc}) {
		$disc = $hrTrack->{disc};
	}
	my $track = 1;
	if (defined $hrTrack->{track}) {
		$track = $hrTrack->{track};
	}
	my $comment = "";
	if ((defined $hrTrack->{comment}) && (defined $hrTrack->{location})) {
		$comment = "$hrTrack->{comment} $hrTrack->{location}";
	}
	elsif (defined $hrTrack->{location}) {
		$comment = "$hrTrack->{location}";
	}
	elsif (defined $hrTrack->{comment}) {
		$comment = "$hrTrack->{comment}";
	}
	my $recordingdate = "";
	if (defined $hrTrack->{recordingdate}) {
		$recordingdate = $hrTrack->{recordingdate};
	}
	my $bandlisting = "";
	if (defined $hrTrack->{bandlisting}) {
		$bandlisting = $hrTrack->{bandlisting};
		$bandlisting =~ s/\\\\\\\\/;/g;
	}
	my $composer = "";
	if (defined $hrTrack->{composer}) {
		$composer = $hrTrack->{composer};
	}
	my $quality = "";
	if (defined $hrTrack->{quality}) {
		$quality = $hrTrack->{quality};
	}
	my $sizemb = "";
	if (defined $hrTrack->{sizemb}) {
		$sizemb = $hrTrack->{sizemb};
	}
	my $length = "";
	if (defined $hrTrack->{length}) {
		$length = $hrTrack->{length};
	}
	
	$sthSongDelete->bind_param( 1, $songtitle );
	$sthSongDelete->bind_param( 2, $albumID );
	$sthSongDelete->bind_param( 3, $disc );
	$sthSongDelete->bind_param( 4, $track );
	$sthSongDelete->execute();
	#$sth = $dbh->prepare('INSERT INTO songs VALUES (?, ?, ?, ?) SELECT ');
	$sthSongInsert->execute(
		$songID, $songtitle, $disc, $track,
		$comment, $recordingdate,
		$bandlisting, $composer,
		$quality, $sizemb, $length, $albumID);
	#$dbh->do( "INSERT INTO songs VALUES ( '$songtitle', '$albumtitle', '$disc', '$track' )" );

	# auto commit is default
	#$dbh->commit;
	 
	#$sth->finish;
	
	#$dbh->disconnect;
}


sub RunStep {
	my $self = shift;
	my $logFile = shift;
	my $trackpath = shift;
	my $artists; # everything stored here as $artists->{$artist}{$album}
	my $bNewArtist;
	my $bNewAlbum;
	my $prevAlbum = "";
	my $prevArtist = "";
	my $trackinfo;
	my $artist = "";
	my $album = "";
	my $albumPath = "";
	my $track;
	my $retval = 0;
	my $artistID = 0;
	my $albumID = 0;
	my $songID = 0;
	
	my $mp3bitrate = 0;
	my $frequency = 0;
	my $time = 0;
	my $size = 0;
	my $secs = 0;
	my $samplerate = 0;
	
	print $MainWindow::fhLogFile "Start Db update\n\n";
	
	my $dbfile = catfile($ENV{"Music_Lib"}, "musicdb.db");
	
	if ($self->{APPEND} == 0) {
		unlink $dbfile or warn "Could not unlink $dbfile: $!";;		
	}

	my $dbh = DBI->connect(          # connect to your database, create if needed
		"dbi:SQLite:dbname=$dbfile", # DSN: dbi, driver, database file
		"",                          # no user
		"",                          # no password
		{ RaiseError => 1 },         # complain if something goes wrong
	) or die $DBI::errstr;
	
	if (!table_exists( $dbh, "artists")) {
		$dbh->do( "CREATE TABLE artists ( artistID INTEGER PRIMARY KEY, name VARCHAR(30) )" );
	}
	if (!table_exists( $dbh, "albums")) {
		$dbh->do( "CREATE TABLE albums (
			albumID INTEGER PRIMARY KEY, title VARCHAR(30), artist VARCHAR(30), genre VARCHAR(30),
			producer VARCHAR(30), releasedate VARCHAR(30), encoding VARCHAR(30),
			source VARCHAR(30), label VARCHAR(30), totaltracks INTEGER,
			samplerate INTEGER, bitrate INTEGER, artistID INTEGER )" );
	}
	if (!table_exists( $dbh, "songs")) {
		$dbh->do( "CREATE TABLE songs (
			songID INTEGER PRIMARY KEY, title VARCHAR(30), disc INTEGER, track INTEGER,
			comment VARCHAR(30), recordingdate VARCHAR(30),
			bandlisting VARCHAR(30),
			composer VARCHAR(30), quality VARCHAR(30), 
			size INTEGER, length INTEGER, albumID INTEGER )" );
	}
	#my $sql = qq{SET NAMES 'utf8';};
	$dbh->{'mysql_enable_utf8'} = 1;
	
	$sthArtistInsert = $dbh->prepare('INSERT OR IGNORE INTO artists VALUES (?, ?)');
	$sthAlbumInsert = $dbh->prepare('INSERT INTO albums VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
	$sthSongDelete = $dbh->prepare('DELETE FROM songs WHERE title = ? AND albumID = ? AND disc = ? AND track = ?');
	$sthSongInsert = $dbh->prepare('INSERT INTO songs VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
	if (defined $trackinfo) {
		# trackinfo contains path, artists, album, track
		my ($newtrackpath, $newartist, $newalbum, $newtrack) = split(/::/, $trackinfo);
		
		$bNewArtist = ($newartist ne $prevArtist);
		$bNewAlbum = $bNewArtist || ($newalbum ne $prevAlbum);
		
		$trackpath = $newtrackpath;
		$artist = $newartist;
		$album = $newalbum;
		$prevArtist = $newartist;
		$prevAlbum = $newalbum;
		$track = $newtrack;
		
		$albumPath = catfile($trackpath, $artist, $album);
		my $musicfile = catfile($albumPath, $track);
		my $artfile = catfile($albumPath, "Folder.jpg");
		$track =~ s/\.flac//;
		$track =~ s/\.mp3//;
		$artist = $self->WinToName($artist);
		$album = $self->WinToName($album);
		$track = $self->WinToName($track);
		
		#my $xartist = encode('utf8', $artist);
		#my $xalbum = encode('utf8', $album);
		#my $xtrack = encode('utf8', $track);
		#$albumtrack = decode('utf8', $albumtrack);
		my $truetrack = $track;
		$truetrack =~ s/128\/q0//;
		$truetrack =~ s/320\/q0//;

		if ($bNewArtist) {
			$artistID++;
			addArtistToDB($dbh, $artist, $artistID);
		}
		if ($bNewAlbum) {
			$albumID++;

			my $EACLogDir = catfile($trackpath, $self->nameToWin($artist), $self->nameToWin($album));
			#my $shortmusicfile = Win32::GetShortPathName($EACLogDir);
			my $ripresults = $self->readLogs($EACLogDir);
			
			my $tagfile = catfile($trackpath, $newartist, $newalbum, "tags.text");
			$artists = $self->readTags($tagfile, $ripresults);
			
			$self->checkFileExistence($artists->{$newartist}{$newalbum}{tracks}, $albumPath);
		}
		
		if(!(($track eq ".") || ($track eq "..")) && ($musicfile =~ /.mp3$/)) {
			#$files++;
			# create new MP3-Tag object
			my $mp3 = MP3::Tag->new($musicfile);
			if (!defined $mp3) {
				$retval = -1;
				goto CLEANUP;
			}
			my $fileinfo;
			$fileinfo = get_mp3info($musicfile);
			if (exists($fileinfo->{BITRATE})) {
				$mp3bitrate = $fileinfo->{BITRATE};
			}
			else {
				$mp3bitrate = 0;
			}                  
			if (exists($fileinfo->{FREQUENCY})) {
				$frequency = ($fileinfo->{FREQUENCY}*1000);
			}
			else {
				$frequency = 0;
			}
			if (exists($fileinfo->{TIME})) {
				$time = $fileinfo->{TIME};
			}
			else {
				$time = 0;
			}
			if (exists($fileinfo->{SIZE})) {
				$size = ($fileinfo->{SIZE}/1000000);
			}
			else {
				$size = 0;
			}
			if (exists($fileinfo->{SECS})) {
				$secs = $fileinfo->{SECS};
			}
			else {
				$secs = 0;
			}

			#my $tagkey;
			#foreach $tagkey (keys %$fileinfo) 
			#{
			#      print $tagkey . "\n";
			#}
		
			## get tag information
			$mp3->get_tags();
			## check to see if an ID3v1 and an ID3v2 tag exists
			#if (exists $mp3->{ID3v1}) {
			#	print "ERROR: $musicfile contains ID3v1 tags\n";
			#}
			if (exists $mp3->{ID3v2}) {
			#
			#	#if (exists $mp3->{ID3v1})
			#	#{
			#	#        #print "Filename: $track\n";
			#	#        #print "Artist: " . $mp3->{ID3v1}->artist . "\n";
			#	#        #print "Title: " . $mp3->{ID3v1}->title . "\n";
			#	#        #print "Album: " . $mp3->{ID3v1}->album . "\n";
			#	#        #print "Year: " . $mp3->{ID3v1}->year . "\n";
			#	#        #print "Genre: " . $mp3->{ID3v1}->genre . "\n";
			#	#}
			#
			#	# if ID3v2 tags exists
			#	if (exists $mp3->{ID3v2})
			#	{
			#		# get a list of frames as a hash reference
			#		my $frames = $mp3->{ID3v2}->get_frame_ids();
			#	 
			#		#foreach $tagtype (keys %$mp3) 
			#		#{
			#		#     print $tagtype . "\n";
			#		#}
			#	 
			#		#print $frames{"TCOM"};
			#
			#		# iterate over the hash
			#		# process each frame
			#		foreach my $frame (keys %$frames) 
			#		{
			#			# for each frame
			#			# get a key-value pair of content-description
			#			my $value;
			#			my $desc;
			#			my $k;
			#			my $v;
			#			my $type;
			#			($value, $desc)= $mp3->{ID3v2}->get_frame($frame);
			#			#print "$frame $desc: \n";
			#			#print "$frame\n";
			#			if ($frame !~ m/APIC/) {
			#				# sometimes the value is itself a hash reference containing more values
			#				# deal with that here
			#				if (ref $value) {
			#					while (($k, $v) = each (%$value)) {
			#						if ($v =~ m/totaltracks/i) {
			#							#print "Found total tracks \n";
			#							$type = "totaltracks";
			#						} elsif ($type =~ m/totaltracks/) {
			#							#print "Found total tracks $v\n";
			#							$totaltracks = $v; $type = "";
			#						} elsif ($v =~ m/source/i) {
			#							#print "Found source \n";
			#							$type = "source";
			#						} elsif ($type =~ m/source/) {
			#							#print "Found source $v\n";
			#							$source = $v; $type = "";
			#						} elsif ($v =~ m/year/i) {
			#							#print "Found releasedates \n";
			#							$type = "releasedates";
			#						} elsif ($type =~ m/releasedates/) {
			#							#print "Found releasedate $v \n";
			#							$releasedate = $v; $type = "";
			#						} elsif ($v =~ m/recordingdates/i) {
			#							#print "Found recordingdates \n";
			#							$type = "recordingdates";
			#						} elsif ($type =~ m/recordingdates/) {
			#							#print "Found recordingdate $v \n";
			#							$recordingdate = $v; $type = "";
			#						}
			#						#elsif ($v =~ m/^Title/) {
			#						#    #print "Found title \n";
			#						#    $type = "Title";
			#						#} elsif ($type =~ m/Title/) {
			#						#    #print "Found title $v \n";
			#						#    $title = $v; $type = "";
			#						#}
			#					  
			#						#print "\n$v";
			#						#print "\n - $k: $v";
			#					}
			#					#print "\n";
			#			   }
			#			   elsif (defined $desc)
			#			   {
			#					if ($desc =~ m/^Composer/) {
			#						#print "Found a composer \n";
			#						$composer = $value;
			#					}
			#					elsif ($desc =~ m/^Band/) {
			#						#print "Found a band \n";
			#						$band = $value;
			#					}
			#					elsif ($desc =~ m/^Publisher/) {
			#						#print "Found a publisher \n";
			#						$publisher = $value;
			#					}
			#					elsif ($desc =~ m/^Encoded by/) {
			#						#print "Found an encoder \n";
			#						$encoder = $value;
			#					}
			#					elsif ($desc =~ m/^Part of a set/) {
			#						#print "Found disc number " . $value . "\n";
			#						$discnumber = $value;
			#					}
			#					elsif ($desc =~ m/^Track number/) {
			#						#print "Found track number " . $value . "\n";
			#						$tracknumber = $value;
			#					}
			#					elsif ($desc =~ m/^Album/) {
			#						#print "Found album " . $value . "\n";
			#						$album = $value;
			#					}
			#					elsif ($desc =~ m/^Content/) {
			#						#print "Found genre " . $value . "\n";
			#						$genre = $value;
			#					}
			#					elsif ($desc =~ m/^Year/) {
			#						#print "Found year " . $value . "\n";
			#						$releasedate = $value;
			#					}
			#					elsif ($desc =~ m/^Title/) {
			#						#print "Found title " . $value . "\n";
			#						$title = $value;
			#					}
			#					else {
			#						#print "$desc : $value\n";
			#					}
			#				}
			#			}
			#		}
			#	}
			#
			#	#path^G:\My Music\iTunes\Sam McClain\Give It Up To Love\1-08 I Feel Good.flac
			#	#album_artist^
			#	#artist^Sam McClain
			#	#composer^Reuben Fairfax, Jr., Al Green, Fred Jordan
			#	#album^Give It Up To Love
			#	#title^I Feel Good
			#	#track^08
			#	#totaltracks^11
			#	#totaldiscs^
			#	#source^lp
			#	#year^1992
			#	#genre^R&B
			#	#discnumber^1/1
			#	#samplerate^44100
			#	#bitrate^915
			#	#sizemb^27.54
			#	#length^04:12
			#	#band^Kevin Barry: Guitar; Lorne Entress: Drums; Bruce Katz: Organ, Piano, Organ (Hammond); Mighty Sam McClain: Vocals; Michael Rivard: Bass; Bennie Wallace: Sax (Tenor);
			#	print OUTFILE "path^" . $fullpath;
			#	if ($artist eq "Various Artists") {
			#		$albumArtist = "Various Artists";
			#	}
			#	else {
			#		$albumArtist = "";                                        
			#	}
			#	print OUTFILE " album_artist^" . $albumArtist;
			#	print OUTFILE " artist^" . $artist;
			#	print OUTFILE " composer^" . $composer;
			#	print OUTFILE " album^" . $album;
			#	print OUTFILE " title^" . $title;
			#	print OUTFILE " track^" . $tracknumber;
			#	print OUTFILE " totaltracks^" . $totaltracks;
			#	print OUTFILE " totaldiscs^";
			#	print OUTFILE " source^" . $source;
			#	# do not use recording date as this will break album ordering - many albums have tracks with differing recording dates
			#	print OUTFILE " year^" . $releasedate;
			#	print OUTFILE " genre^" . $genre;
			#	print OUTFILE " discnumber^" . $discnumber;
				$artists->{$newartist}{$newalbum}{tracks}{$truetrack}->{samplerate} = $frequency;
				$artists->{$newartist}{$newalbum}{tracks}{$truetrack}->{bitrate} = $mp3bitrate;
				$artists->{$newartist}{$newalbum}{tracks}{$truetrack}->{sizemb} = $size;
				$artists->{$newartist}{$newalbum}{tracks}{$truetrack}->{length} = $time;
				$artists->{$newartist}{$newalbum}{tracks}{$truetrack}->{encoding} = "mp3";
			#	print OUTFILE " samplerate^" . $frequency;
			#	print OUTFILE " bitrate^" . $mp3bitrate;
			#	printf( OUTFILE " sizemb^%.2f", $size);
			#	print OUTFILE " length^" . $time;
			#	print OUTFILE " band^" . $band;
			#	print OUTFILE "\n";

				# clean up
				$mp3->close();
				#$band = "";
				#$composer = "";
				#$publisher = "";
				#$encoder = "";
				#$discnumber = "";
				#$totaltracks = 99;
			}
			else {
				print "ERROR: $musicfile contains no mp3 tags\n";
			}
		}
		elsif(!(($track eq ".") || ($track eq "..")) && ($musicfile =~ /.flac$/)) {
			#$files++;
			my $flacfileinfo = {};
			my $status;
			$flacfileinfo->{FILE} = $musicfile;
			$status = read_flac($flacfileinfo);
		  
			if ($status eq 0) {
		  
				#print OUTFILE "path^" . $musicfile;
				#if ($artist eq "Various Artists") {
				#	$albumArtist = "Various Artists";
				#}
				#print OUTFILE " album_artist^" . trim($albumArtist);
				#print OUTFILE " artist^" . trim($artist);
				# this style of print does not handle special characters (eg. accents) properly
				# because promotion does not occur?
				#print OUTFILE " artist^$artist";
				#if (exists($flacfileinfo->{tags}->{"ARTIST"})) { print OUTFILE $flacfileinfo->{tags}->{"ARTIST"}; }
				#if (exists($flacfileinfo->{tags}->{"COMPOSER"})) {
				#	my $comp = $flacfileinfo->{tags}->{"COMPOSER"};
				#	Encode::_utf8_on( $comp );
				#	print OUTFILE " composer^" . $comp;
				#}
				#else {
				#	print OUTFILE " composer^";                                        
				#}
				my $flacalbum;
				$flacalbum = $album;
				$flacalbum  = WinFileFix($flacalbum);
				#print OUTFILE " album^$flacalbum";
				#if (exists($flacfileinfo->{tags}->{"ALBUM"})) { print OUTFILE $flacfileinfo->{tags}->{"ALBUM"}; }
				#my $title;
				#$title = $track;
				#$title =~ s/\.flac//;
				#$title =~ s/^\d-\d\d\s+//;
				#$title = WinFileFix($title);
				#print OUTFILE " title^" . $title;
				##if (exists($flacfileinfo->{tags}->{"TITLE"})) { print OUTFILE $flacfileinfo->{tags}->{"TITLE"}; }
				#print OUTFILE " track^";
				#if (exists($flacfileinfo->{tags}->{"TRACKNUMBER"})) { print OUTFILE $flacfileinfo->{tags}->{"TRACKNUMBER"}; }
				#print OUTFILE " totaltracks^";
				#if (exists($flacfileinfo->{tags}->{"TOTALTRACKS"})) { print OUTFILE $flacfileinfo->{tags}->{"TOTALTRACKS"}; }
				#print OUTFILE " totaldiscs^";
				#print OUTFILE " source^";
				#if (exists($flacfileinfo->{tags}->{"SOURCE"})) { print OUTFILE $flacfileinfo->{tags}->{"SOURCE"}; }
				#print OUTFILE " year^";
				#if (exists($flacfileinfo->{tags}->{"DATE"})) { print OUTFILE $flacfileinfo->{tags}->{"DATE"}; }
				#print OUTFILE " genre^";
				#if (exists($flacfileinfo->{tags}->{"GENRE"})) { print OUTFILE $flacfileinfo->{tags}->{"GENRE"}; }
				#print OUTFILE " discnumber^";
				#if (exists($flacfileinfo->{tags}->{"DISCNUMBER"})) { print OUTFILE $flacfileinfo->{tags}->{"DISCNUMBER"}; }
									
				$artists->{$newartist}{$newalbum}{tracks}{$truetrack}->{samplerate} = 0;
				if (exists($flacfileinfo->{streaminfo}->{$keyInfoSampleRate})) {
					$artists->{$newartist}{$newalbum}{tracks}{$truetrack}->{samplerate} = $flacfileinfo->{streaminfo}->{$keyInfoSampleRate};
				}
				
				$artists->{$newartist}{$newalbum}{tracks}{$truetrack}->{bitrate} = $flacfileinfo->{streaminfo}->{$keyInfoBitsPerSample};
				$artists->{$newartist}{$newalbum}{tracks}{$truetrack}->{sizemb} = (-s $musicfile)/1000000;
				if (exists($flacfileinfo->{streaminfo}->{$keyInfoDuration})) {
					my @timeparts = gmtime($flacfileinfo->{streaminfo}->{$keyInfoDuration});
					if ($flacfileinfo->{streaminfo}->{$keyInfoDuration} ge 3600) {
						$artists->{$newartist}{$newalbum}{tracks}{$truetrack}->{length} = sprintf("%02d:%02d:%02d", @timeparts[2,1,0]);
					}
					else {
						$artists->{$newartist}{$newalbum}{tracks}{$truetrack}->{length} = sprintf("%02d:%02d", @timeparts[1,0]);
					}
				}
				else {
					$artists->{$newartist}{$newalbum}{tracks}{$truetrack}->{length} = "00:00";
				}
				$artists->{$newartist}{$newalbum}{tracks}{$truetrack}->{encoding} = "flac";

				#print OUTFILE " samplerate^";
				#if (exists($flacfileinfo->{streaminfo}->{$keyInfoSampleRate})) { print OUTFILE $flacfileinfo->{streaminfo}->{$keyInfoSampleRate}; }
				#print OUTFILE " bitrate^0";
				#my $filesize = -s $musicfile;
				#printf( OUTFILE " sizemb^%.2f", $filesize/(2**20));
				#if (exists($flacfileinfo->{streaminfo}->{$keyInfoDuration})) {
				#	my @timeparts = gmtime($flacfileinfo->{streaminfo}->{$keyInfoDuration});
				#	if ($flacfileinfo->{streaminfo}->{$keyInfoDuration} ge 3600) {
				#		printf( OUTFILE " length^%02d:%02d:%02d", @timeparts[2,1,0]);
				#	}
				#	else {
				#		printf( OUTFILE " length^%02d:%02d", @timeparts[1,0]);
				#	}
				#}
				#else {
				#	printf( OUTFILE " length^");
				#}
				#if (exists($flacfileinfo->{tags}->{"BAND"})) {
				#	my $flacband;
				#	$flacband = $flacfileinfo->{tags}->{"BAND"};
				#	Encode::_utf8_on( $flacband );
				#	#my $check = utf8::is_utf8($flacband);
				#	print OUTFILE " band^" . $flacband;
				#}
				#else {
				#	print OUTFILE " band^";                                        
				#}
				#
				#print OUTFILE "\n";
			}
			else {
				$retval = -1;
				goto CLEANUP;
			}

		}

		my $hrTrack = $artists->{$newartist}{$newalbum}{tracks}{$truetrack};
		if ($bNewAlbum) {
			addAlbumToDB($dbh, $album, $hrTrack, $artistID, $albumID);
		}
		addSongToDB($dbh, $hrTrack, $albumID, $songID);
		$songID++;
	}

	$dbh->do( "CREATE INDEX idx_songs_albumid on songs(albumID)");

	my $res = $dbh->selectall_arrayref( q( SELECT s.albumID, s.disc, s.track, s.title,
									   s.comment, s.recordingdate, s.bandlisting,
									   s.composer, s.quality
										   FROM songs s ) ) ;
										   #WHERE a.title = s.album ) );

	foreach( @$res ) {
		foreach my $i (0..$#$_) {
			my $utfstring = decode('utf8',$_->[$i]);
			print $MainWindow::fhLogFile "$utfstring "
		}
		print $MainWindow::fhLogFile "\n";
	}

	$res = $dbh->selectall_arrayref( q( SELECT * FROM albums a) );
	
	foreach( @$res ) {
		foreach my $i (0..$#$_) {
			my $utfstring = decode('utf8',$_->[$i]);
			print "$utfstring "
		}
		print "\n";
	}
	
	$res = $dbh->selectall_arrayref( q( SELECT b.name, a.title, s.disc, s.track, s.title, a.genre
										   FROM songs s, albums a, artists b
										   WHERE "Guitar Bones" = a.title AND s.albumID = a.albumID AND b.artistID = a.artistID) );
	
	foreach( @$res ) {
		foreach my $i (0..$#$_) {
			my $utfstring = decode('utf8',$_->[$i]);
			print "$utfstring "
		}
		print "\n";
	}

CLEANUP:	
	$sthArtistInsert->finish();
	$sthAlbumInsert->finish();
	$sthSongDelete->finish();
	$sthSongInsert->finish();
	undef $dbh;
	#$dbh->disconnect;
	print $MainWindow::fhLogFile "Step WriteDB exits with status $retval\n";
	return($retval >> 8);
}

sub GetOptions {
	my $self = shift;
	my %opthash;
	
	$opthash{"opt_append"} = $self->{APPEND};
	
	return(\%opthash);
}
sub SetOptions {
	my $self = shift;
	my $optref = shift;
	my %opthash = %$optref;
	
	$self->{APPEND} = $opthash{"opt_append"};
}
sub QueryOptions {
	my $self = shift;
	my($ok) = ('OK');
	my($cancel) = ('Cancel');
	my $DIALOG;
	my $val = $self->{APPEND};

	$DIALOG = $flow::MainWindow->Dialog(
		-title          => 'Set DataBase append mode',
		-default_button => $ok,
		-buttons        => [$ok, $cancel],
	);

	my $sb = $DIALOG->Checkbutton(
		-variable => \$val,
		-relief => 'flat',
	)->pack(-side => 'left');
	$DIALOG->Label(-text => 'Select to append to Db, deselect to clear entire Db.')->pack(-side => 'left');
	
	my $button = $DIALOG->Show;
	
	if ($button eq $ok) {
		$self->{APPEND} = $val;
	}
}
	
# return >0 if this step needs to run
# return 0	if this step does not need to run
# return <0 if this step can not run
sub CheckDependencies {
	my $self = shift;
	my $lastrun = shift;
	my $sbi;
	my $sbo;

	if (!defined $ENV{"Bin_Dir"}) {
		#print $MainWindow::fhLogFile "The environment variable \"Bin_Dir\" must be defined.\n";
		#print $MainWindow::fhLogFile "Exiting ...\n";
		exit(-1);
	}
	
	if (!defined $ENV{"Selection"}) {
		#print $MainWindow::fhLogFile "The environment variable \"Bin_Dir\" must be defined.\n";
		#print $MainWindow::fhLogFile "Exiting ...\n";
		exit(-1);
	}
	
	my $selectFile = $ENV{"Selection"};
	
	if (!-e $selectFile) {
		#print $MainWindow::fhLogFile "Selection file $selectFile does not exist.\n";
		return(-1);
	}
	
	# always run  the step
	return(1);
	
	$sbi = stat($selectFile);
	my $in_time = $sbi->mtime;
	
	if ($in_time >= $lastrun) {
		# run the step
		#print $MainWindow::fhLogFile "Select file has changed since this step's last execution.\n";
		return(1);
	}

	# skip the step
	return(0);
}

sub table_exists {
	my $self = shift;
	my $db = shift;
	my $table = shift;
	my @tables = $db->tables('','','','TABLE');
	if (@tables) {
		for (@tables) {
			next unless $_;
			return 1 if $_ eq $table
		}
	}
	else {
		eval {
			local $db->{PrintError} = 0;
			local $db->{RaiseError} = 1;
			$db->do(qq{SELECT * FROM $table WHERE 1 = 0 });
		};
		return 1 unless $@;
	}
	return 0;
}

# Only argument is filename, with full path
sub read_flac
{
	my $self = shift;
	my $fileinfo = {};
	$fileinfo = shift;
	
	my $status = 0;
	my $filename;
	my ($flac_indicator,$tmp);
	my ($meta_head,$meta_last,$meta_type,$meta_size,$meta_contents);
	my ($meta_list,$numheads,$thr,@retlist);
	
	if (! exists($fileinfo->{FILE})) {
		print "ERROR: file name not provided to read_flac function\n";
		return -6;     
	}
	
	$filename = $fileinfo->{FILE};
	
	# Check for existence & readability
	if (! -e $filename) {
		  print "ERROR: file $filename does not exist\n";
		  return -1;
	}
	if (! -r $filename) {
		print "ERROR: file $filename is not readable\n";
		return -2;
	}

	$fileinfo->{filename} = $filename;

	if (-w $filename)
	{
		$fileinfo->{mode} = "Read/Write";
	}
	else
	{
		$fileinfo->{mode} = "Read Only";
	}

   if (! open(FLACFILE,'<:encoding(utf8)', $filename))
   #if (! open(FLACFILE, $filename))
   {
		print "ERROR: file $filename could not be opened\n";
		return -5;
	}
	
	binmode FLACFILE;

	# first four characters of a flac file must be "fLaC"
	read FLACFILE, $flac_indicator,4 or return -3;

	if ($flac_indicator ne "fLaC")
	{
		print "ERROR: file $filename does not appear to be a properly formatted flac file\n";
		close FLACFILE;
		return -4;
	}

	$meta_list=[];

	$numheads = 0;

	while(1)
	{
		#Parse METADATA_BLOCK_STREAMINFO
		# next 4 bytes is length of
		if (! read FLACFILE,$tmp, 4) {
			print "ERROR: file $filename problem reading contents\n";
			return -3;
		}

		$meta_head = unpack "N",$tmp;

		# What's the info stored here?
		$meta_last = ($last_block_flag & $meta_head)>>31;
		$meta_type = ($block_type_flag & $meta_head)>>24;
		$meta_size = $m_len_flag & $meta_head;

		if (! read FLACFILE,$meta_contents,$meta_size) {
			print "ERROR: file $filename problem reading contents\n";
			return -3;
		}

		$thr =  {Last => $meta_last,
		Type => $meta_type,
		Size => $meta_size,
		Contents => $meta_contents};

		$meta_list->[$numheads] = $thr;
		$numheads++;

		if ($meta_type==$STREAMINFO)
		{
			# This is the stream info block
			$fileinfo->{streaminfo} = parse_streaminfo_header($meta_contents);
		}
		elsif ($meta_type==$VORBIS_COMMENT)
		{
			# This is the Vorbis tag
			$fileinfo->{tags} = parse_vorbis_header($meta_contents);
		}

		if ($meta_last)
		{
			last;
		}
	}

	$fileinfo->{start} = tell FLACFILE;

	close FLACFILE;

	$fileinfo->{headers} = $meta_list;

	return $status;
}

sub parse_streaminfo_header
{
	#METADATA_BLOCK_STREAMINFO  
	# <16>  The minimum block size (in samples) used in the stream.  
	# <16>  The maximum block size (in samples) used in the stream. (Minimum blocksize == maximum blocksize) implies a fixed-blocksize stream.  
	# <24>  The minimum frame size (in bytes) used in the stream. May be 0 to imply the value is not known.  
	# <24>  The maximum frame size (in bytes) used in the stream. May be 0 to imply the value is not known.  
	# <20>  Sample rate in Hz. Though 20 bits are available, the maximum sample rate is limited by the structure of frame headers to 655350Hz. Also, a value of 0 is invalid.  
	# <3>  (number of channels)-1. FLAC supports from 1 to 8 channels  
	# <5>  (bits per sample)-1. FLAC supports from 4 to 32 bits per sample. Currently the reference encoder and decoders only support up to 24 bits per sample.  
	# <36>  Total samples in stream. 'Samples' means inter-channel sample, i.e. one second of 44.1Khz audio will have 44100 samples regardless of the number of channels. A value of zero here means the number of total samples is unknown.  
	# <128>  MD5 signature of the unencoded audio data. This allows the decoder to determine if an error exists in the audio data even when the error does not result in an invalid bitstream.  

	my $self = shift;
	my $streaminfostring = shift;
	my $r;
	my ($min_block_size,$max_block_size);
	my ($min_frame_size,$max_frame_size);
	my $sample_rate_hz;
	my $num_channels;
	my $bits_per_sample;
	my ($total_samples, $total_samples1, $total_samples2);
	my $duration_sec;
	my $upper_bits;
	my $taghash = {};

	$r = substr($streaminfostring,0,2);
	# n - A 16 bit value in "network" (big-endian) order
	$min_block_size = unpack "n", $r;
	
	$r = substr($streaminfostring,2,2);
	$max_block_size = unpack "n", $r;

	$r = substr($streaminfostring,4,3);
	$r = "\c@" . $r;
	# N - A 32 bit value in "network" (big-endian) order
	$min_frame_size = unpack "N", $r;

	$r = substr($streaminfostring,7,3);
	$r = "\c@" . $r;
	$max_frame_size = unpack "N", $r;

	$r = substr($streaminfostring,10,3);
	$r = "\c@" . $r;
	# 20-bit field so throw away lowest order four bits
	# N - A 32 bit value in "network" (big-endian) order
	$sample_rate_hz = unpack("N", $r) >> 4;

	$r = substr($streaminfostring,12,1);
	# 3-bit field so throw away highest order four bits and lowest 1 bit
	# add 1 because 8 encodings map to 1 through 8
	# C - An unsigned char value
	$num_channels = ((unpack("C", $r) & 0x0f) >> 1) + 1;

	$r = substr($streaminfostring,12,2);
	# 5-bit field so throw away highest order 3 bits and lowest 4 bits
	# add 1 because 32 encodings map to 1 through 32
	# n - An unsigned short (16-bit) in "network" (big-endian) order
	$bits_per_sample = ((unpack("n", $r) & 0x01f0) >> 4) + 1;

	$r = substr($streaminfostring,14,5);
	# 36-bit field so throw away highest order 4 bits
	# to keep it a 32 bit value.  At 44.1kHz, this will allow
	# for a track that is over 27 hours long.
	# N - A 32 bit value in "network" (big-endian) order
	$total_samples = unpack("N", $r);

	# check that upper 4 bits are zero
	$r = substr($streaminfostring,13,1);
	# C - An unsigned char value
	$upper_bits = unpack("C", $r) & 0x0f;
	if ($upper_bits ne 0) {
		  print "ERROR play duration exceeds 32-bit value\n";
	}
	   
	$duration_sec = $total_samples / $sample_rate_hz;

	$taghash->{$keyInfoDuration} = $duration_sec;
	$taghash->{$keyInfoSampleRate} = $sample_rate_hz;
	$taghash->{$keyInfoChannels} = $num_channels;
	$taghash->{$keyInfoBitsPerSample} = $bits_per_sample;
	$taghash->{$keyInfoTotalSamples} = $total_samples;
	
	return $taghash;
}

sub parse_vorbis_header
{
	# All Vorbis tags are in little-endian format
	# first, vendor length
	my $self = shift;
	my $tagstring;
	$tagstring = shift;
	my ($vendor_length,$vendor_string);
	my ($numtags,$i);
	my ($lnlen,$lnstr);
	my ($tag_key, $tag_val);
	my $taghash = {};
	my @list;

	$vendor_length = substr($tagstring,0,4);
	$vendor_length = unpack "L",$vendor_length;

	$tagstring = substr($tagstring,4);

	$vendor_string = substr($tagstring,0,$vendor_length);

	$taghash->{Vendor} = $vendor_string;

	$tagstring = substr($tagstring,$vendor_length);

	$numtags = substr($tagstring,0,4);
	$numtags = unpack "L",$numtags;

	$tagstring = substr($tagstring,4);

	for ($i=0;$i<$numtags;$i++)
	{
		$lnlen = substr($tagstring,0,4);
		$tagstring = substr($tagstring,4);
		$lnlen = unpack "L",$lnlen;

		$lnstr = substr($tagstring,0,$lnlen);
		$tagstring = substr($tagstring,$lnlen);

		# parse for '='
		if ($lnstr =~ /(.*?)=(.*)/)
		{
			$tag_key = $1;
			$tag_key =~ tr/a-z/A-Z/;
			$tag_val = $2;
			if (!defined $taghash->{$tag_key}) {
			  $taghash->{$tag_key} = $tag_val;
			  #print "$tag_key = $tag_val\n";
			}
			else {
			  $taghash->{$tag_key} = $taghash->{$tag_key} . "\\" . $tag_val;
			}
		}
	}

	return $taghash;
}

# Perl trim function to remove whitespace from the start and end of the string
sub trim($)
{
	my $self = shift;
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

#input legal Windows file name, output properly translated name
sub WinFileFix($)
{
	my $self = shift;
	my $string = shift;
	$string =~ s/;/:/;
	$string =~ s/=/\?/;
	$string =~ s/_/\//;
	return $string;   
}

=head1 AUTHOR

  � 2014 Steve Coward <steve-coward@comcast.net>
 *All rights reserved. No warranty, explicit or implicit, provided.
 In no event shall the author be liable for any claim or damages.

=cut

1;
